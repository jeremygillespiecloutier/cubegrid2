﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class that detects tap and drag events on Android devices
// Ignores events that occur while another event is in progress
public class InputController : MonoBehaviour
{

    int touchID = -1; // ID of the touch event currently being tracked
    const float DRAG_MIN = 15f; // Minimum dragging distance before a touch event is considered a drag gesture
    Vector2 lastPos; // last position of the touch event
    bool dragging=false; // True if the player is currently dragging
    // At each frame, dragged is set to true if a change in drag is detected and tapped is set to true if a tap is detected
    // If there is a change in drag, dragVector contains the direction and magnitude of this change
    // If there is a tap, tapPosition will contain the position of the tap
    public bool dragged { get; private set; }
    public bool tapped { get; private set; }
    public Vector2 dragVector { get; private set; }
    public Vector2 tapPosition { get; private set; }
    public bool tapOverUI { get; private set; } // True if the last tap was over the UI when it occured

    void Update()
    {
        dragged = false;
        tapped = false;
        dragVector = Vector3.zero;
        foreach(Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began && touchID == -1)
            {
                lastPos = touch.position;
                touchID = touch.fingerId;
            }
            if (touch.fingerId == touchID)
            {
                // Since isPointerOverGameObject returns false when touch is in TouchPhase.Ended, we can only
                // detect if it was over the UI one frame before it ended, so we need to do the check here
                if (touch.phase != TouchPhase.Ended)
                    tapOverUI = UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(touchID);

                if (touch.phase == TouchPhase.Moved)
                {
                    // Begin dragging gesture if player dragged far enough from initial touch
                    Vector2 diff = touch.position - lastPos;
                    if (diff.magnitude > DRAG_MIN && !dragging)
                    {
                        dragging = true;
                        lastPos = touch.position;
                    }
                    if (dragging)
                    {
                        // Calculate change in drag
                        dragged = true;
                        dragVector = touch.position - lastPos;
                        lastPos = touch.position;
                    }
                }
                else if (touch.phase == TouchPhase.Ended)
                {
                    // If touch ended without a drag gesture detected, it was a tap
                    if (!dragging)
                    {
                        tapPosition = touch.position;
                        tapped = true;
                        touchID = -1;
                    }
                    dragging = false;
                }
            }
        }
    }
}
