﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

    Text textWin, textFragments, textDialogBox;
    Button buttonSettings, buttonDone, buttonVolume;
    GameObject dialogBox, panelSettings;
    Game game;
    bool volumeOn = false;
    float volume;

    private void Start()
    {
        volume = AudioListener.volume;
        AudioListener.volume = 0;
        game = GameObject.Find("Level").GetComponent<Game>();
        textWin = GameObject.Find("TextWin").GetComponent<Text>();
        textFragments = GameObject.Find("TextFragments").GetComponent<Text>();
        dialogBox = GameObject.Find("DialogBox");
        textDialogBox = GameObject.Find("TextDialogBox").GetComponent<Text>();
        panelSettings = GameObject.Find("PanelSettings");
        buttonSettings = GameObject.Find("ButtonSettings").GetComponent<Button>();
        buttonSettings.onClick.AddListener(() => {
            panelSettings.SetActive(true);
        });
        buttonDone = GameObject.Find("ButtonDone").GetComponent<Button>();
        buttonDone.onClick.AddListener(() => {
            panelSettings.SetActive(false);
        });
        buttonVolume = GameObject.Find("ButtonVolume").GetComponent<Button>();
        buttonVolume.onClick.AddListener(() => {
            volumeOn = !volumeOn;
            AudioListener.volume = volumeOn ? volume : 0;
            buttonVolume.GetComponent<Image>().sprite = volumeOn ? buttonVolume.GetComponent<ToggleButton>().imageOn : buttonVolume.GetComponent<ToggleButton>().imageOff;
        });

        dialogBox.SetActive(false);
        textWin.gameObject.SetActive(false);
        panelSettings.SetActive(false);
    }

    public void finishLevel()
    {
        textWin.gameObject.SetActive(true);
    }

    public void updateFragments()
    {
        textFragments.text = game.fragments+" fragment"+(game.fragments==1?"":"s");
    }

    public void showDialog(string message)
    {
        dialogBox.SetActive(true);
        textDialogBox.text = message;
    }

    public void hideDialog()
    {
        dialogBox.SetActive(false);
    }

}
