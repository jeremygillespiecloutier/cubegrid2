﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Snaps the game object to the defined grid in edit mode
[ExecuteInEditMode]
public class GridSnap : MonoBehaviour {

    public float snapX = 1, snapY = 1, snapZ=1; // The snapping intervals in world units

	void Update () {
        // Ensure the gameobject's position is a multiple of the snap values
        if (!Application.isPlaying)
        {
            Vector3 pos = transform.position;
            transform.position = new Vector3(Mathf.Floor(pos.x / snapX) * snapX, Mathf.Floor(pos.y / snapY) * snapY, Mathf.Floor(pos.z / snapZ) * snapZ);
        }
	}
}
