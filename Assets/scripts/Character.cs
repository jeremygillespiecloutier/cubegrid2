﻿using Assets.scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Player behavior
public class Character : MonoBehaviour {

    public Cell currentCell;
    public List<Link> path=new List<Link>();
    Level level;
    Game game;
    Link currentLink; // Current link being travelled by the character
    float rotation = 0; // rotation progress of the current move
    const float SPEED = 200f; // In degrees per second
    Vector3 startPos; // Start position of current move
    Quaternion initialRotation; // stores character's rotation at start of game
    bool stopped; // True if player is stopped on his current cell
    public bool isPlayer { get { return gameObject == GameObject.Find("Player"); } }


	void Start () {
        level = GameObject.Find("Level").GetComponent<Level>();
        game = GameObject.Find("Level").GetComponent<Game>();
        // Find character's current cell
        currentCell = level.cells[(int)transform.position.x, (int)transform.position.y, (int)transform.position.z];
        initialRotation = transform.rotation;
    }

	void Update () {
        moveCharacter();
	}

    void moveCharacter()
    {
        if (currentLink == null && path.Count > 0)
        {
            if (currentCell.trigger != null)
                currentCell.trigger.exit(stopped, this);
            // Get the next move
            currentLink = path[0];
            path.RemoveAt(0);
            currentCell = currentLink.second;
            rotation = 0;
            startPos = transform.position;
            stopped = path.Count == 0;
        }
        if (currentLink != null)
        {
            // Update character position and rotation
            rotation += SPEED * Time.deltaTime;
            rotation = Mathf.Clamp(rotation, 0, currentLink.angle);
            transform.position = startPos;
            transform.rotation = initialRotation;
            transform.RotateAround(currentLink.pivot, currentLink.axis, rotation);
            if (rotation >= currentLink.angle) // If character reached stop position, move is complete
            {
                currentLink = null;
                transform.rotation = initialRotation; // Reset character rotation
                // If new position contains a trigger, execute it
                if (currentCell.trigger != null)
                    currentCell.trigger.enter(path.Count==0, this);
                reachedCell(path.Count == 0);
            }
        }
    }

    void reachedCell(bool isDestination)
    {
        if (isPlayer)
        {
            // Check if player collected a fragment
            Cell cell = level.cellAtPosition(transform.position);
            if (cell.fragment)
            {
                Destroy(cell.fragment);
                cell.fragment = null;
                game.fragments++;
            }
        }
    }

}
