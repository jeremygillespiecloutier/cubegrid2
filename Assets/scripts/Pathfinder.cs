﻿using Assets.scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Handles pathfinding
public class Pathfinder : MonoBehaviour {

    Level level;

	// Use this for initialization
	void Start () {
        level = GetComponent<Level>();
	}

    // Returns a path from the start cell to the destination cell using A* (empty if no path was found)
    public List<Link> findPath(Cell start, Cell destination)
    {
        List<Link> path = new List<Link>();
        List<Cell> visited = new List<Cell>();
        List<Cell> border = new List<Cell>();
        Cell current = start;
        current.h = Mathf.Abs(current.pos.x - destination.pos.x) + Mathf.Abs(current.pos.y - destination.pos.y) + Mathf.Abs(current.pos.z - destination.pos.z);
        border.Add(current);

        while (border.Count != 0)
        {
            float best = float.MaxValue;
            foreach(Cell cell in border)
            {
                if (cell.f < best)
                {
                    current = cell;
                    best = cell.f;
                }
            }
            border.Remove(current);
            visited.Add(current);
            if (current == destination)
                break;
            foreach(Link link in current.links)
            {
                if (!link.second.isObstacle && !visited.Contains(link.second))
                {
                    float h = Mathf.Abs(destination.x - link.second.x) + Mathf.Abs(destination.y - link.second.y) + Mathf.Abs(destination.z - link.second.z);
                    if (!border.Contains(link.second))
                        border.Add(link.second);
                    else if (link.second.f < h + current.g + 1)
                        continue;
                    link.second.predLink = link;
                    link.second.g = current.g + 1;
                    link.second.h = h;
                }
            }
        }
        // reconstruct the path
        if (current == destination)
        {
            while (current.predLink != null)
            {
                path.Insert(0, current.predLink);
                current = current.predLink.first;
            }
        }
        // clear heuristic and cost values
        foreach(Cell cell in visited)
        {
            cell.g = 0;
            cell.h = 0;
            cell.predLink = null;
        }
        foreach(Cell cell in border)
        {
            cell.g = 0;
            cell.h = 0;
            cell.predLink = null;
        }
        return path;
    }

}
