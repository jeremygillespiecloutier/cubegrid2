﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.scripts
{
    // Contains information about a cell (reachable empty square) in the game grid
    public class Cell
    {
        public List<Vector3> midPoints { get; private set; } // midpoints of each edge of the cell
        public List<Cell> blocks { get; private set; } // adjacent cells that are blocks
        public List<Link> links { get; private set; } // links to cells that are reachable from the current cell
        public int x { get; private set; }
        public int y { get; private set; }
        public int z { get; private set; }
        public Vector3 pos { get; private set; }
        public bool isBlock { get; private set; }
        public Link predLink; // Link from the predecessor cell to this cell (for pathfinding)
        public float g = 0, h = 0; // cost so far  and heuristic values 
        public float f { get { return g + h; } } // total cost function
        public Trigger trigger;
        public bool isObstacle = false;
        public GameObject fragment; // fragment contained by the cell

        public Cell(int x, int y, int z, bool isBlock)
        {
            midPoints = new List<Vector3>();
            blocks = new List<Cell>();
            links = new List<Link>();
            this.x = x;
            this.y = y;
            this.z = z;
            pos = new Vector3(x, y, z);
            this.isBlock = isBlock;
            calculateMidpoints();
        }

        void calculateMidpoints()
        {
            // Calculate the midpoints of the edges of the cell
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    for (int k = -1; k <= 1; k++)
                    {
                        if (!(i == 0 && j == 0 && k == 0) && !(Mathf.Abs(i) == 1 && Mathf.Abs(j) == 1 && Mathf.Abs(k) == 1))
                        {
                            midPoints.Add(new Vector3(x + i * 0.5f, y + j * 0.5f, z + k * 0.5f));
                        }
                    }
                }
            }
        }
    }

    public class Link
    {
        public Vector3 axis;
        public Vector3 pivot; // point around which player rotates to go from first to second cell
        public float angle;
        public Cell first, second;
    }
}
