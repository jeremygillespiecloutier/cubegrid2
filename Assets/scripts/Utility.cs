﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.scripts
{
    // Various utility functions
    class Utility
    {
        // Round the positions of transforms to correct floating point errors;
        public static void roundPosition(params Transform[] transforms)
        {
            foreach(Transform t in transforms)
            {
                Vector3 pos = t.transform.position;
                t.transform.position = new Vector3(Mathf.Round(pos.x), Mathf.Round(pos.y), Mathf.Round(pos.z));
            }
        }
    }
}
