﻿using Assets.scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour {

    Level level;
    Pathfinder pathfinder;
    Character player;
    Menu menu;
    InputController inputController;
    const float CAM_SPEED=120; // Camera rotation speed (degrees per second)
    const float CAM_DISTANCE = 12; // Distance from camera to player
    const float DRAG_SCALE=0.5f; // Scale factor to dampen or increase the value of drags detected on touch screens
    float camHoriz=0, camVert=0; // The camera horizontal and vertical rotation
    Quaternion camRot; // Initial camera rotatio
    // Number of fragments collected by the player
    int numFragments;
    public int fragments { get { return numFragments; } set { numFragments = value; menu.updateFragments(); } }
    bool android; // True if the app is running on Android

	void Start () {
        level = GetComponent<Level>();
        pathfinder = GetComponent<Pathfinder>();
        player = GameObject.Find("Player").GetComponent<Character>();
        menu = GameObject.Find("Canvas").GetComponent<Menu>();
        inputController=GetComponent<InputController>();
        android = Application.platform == RuntimePlatform.Android;
        camRot = Camera.main.transform.rotation;
        Camera.main.transform.position = player.transform.position + new Vector3(0, 0, -CAM_DISTANCE);
        Camera.main.transform.LookAt(player.transform.position);
        Camera.main.GetComponent<Animator>().enabled = false;
    }

	void Update () {
        // Move the camera
        float step = Time.deltaTime * CAM_SPEED;
        if (Input.GetKey(KeyCode.A))
            camHoriz += step;
        if (Input.GetKey(KeyCode.D))
            camHoriz -= step;
        if (Input.GetKey(KeyCode.W))
            camVert += step;
        if (Input.GetKey(KeyCode.S))
            camVert -= step;
        if (inputController.dragged)
        {
            camHoriz += inputController.dragVector.x*DRAG_SCALE;
            camVert += inputController.dragVector.y*DRAG_SCALE;
        }
        camHoriz = camHoriz%360;
        camVert = camVert % 360;

        Camera.main.transform.rotation = camRot;
        Camera.main.transform.position = player.transform.position + new Vector3(0, 0, -CAM_DISTANCE);
        Camera.main.transform.LookAt(player.transform.position);
        Camera.main.transform.RotateAround(player.transform.position, new Vector3(0, 1, 0), camHoriz);
        Camera.main.transform.RotateAround(player.transform.position, Camera.main.transform.right, camVert);

        bool overUI = android ? inputController.tapOverUI : UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(); // Ignore clicks that intersect UI
        // Move the player
        if (((Input.GetMouseButtonDown(0) && !android) || inputController.tapped) && !overUI)
            movePlayer();
    }

    void movePlayer()
    {
        if (Camera.main.GetComponent<Animator>().enabled) // Don't move player while there is an animation
            return;

        Vector3 mousePos = android ? new Vector3(inputController.tapPosition.x, inputController.tapPosition.y, 0) : Input.mousePosition;
        // Find the cell the player wants to move to and calculate the path to get there
        RaycastHit hit;
        if(Physics.Raycast(Camera.main.ScreenPointToRay(mousePos), out hit))
        {
            if (hit.transform.tag == "Block")
            {
                Vector3 pos = hit.transform.position + hit.normal.normalized;
                Cell destination = level.cellAtPosition(pos);
                if (destination != null && destination!=player.currentCell && !destination.isObstacle)
                {
                    List<Link> path = pathfinder.findPath(player.currentCell, destination);
                    player.path = path;
                }
            }
        }
    }

}
