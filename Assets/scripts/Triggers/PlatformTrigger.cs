﻿using Assets.scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Trigger that causes a platform to move
public class PlatformTrigger : Trigger {

    GameObject platform; // The platform
    public List<Vector3> moves; // The list of movements by the platform (destination at end of moves must be a hidden block)
    Vector3 characterStart, platformStart;
    bool moving = false;
    Character character;
    Level level;
    int direction = 1; // sign flipped when platform switches direction
    const float SPEED = 3f;
    float distance, totalDistance; // The distance travelled and the total distance to travel

    private void Start()
    {
        level = GameObject.Find("Level").GetComponent<Level>();
        platform = transform.parent.gameObject;
    }

    private void Update()
    {
        if (moving)
        {
            // Find position of platform relative to start
            distance += Time.deltaTime * SPEED;
            distance = Mathf.Clamp(distance, 0, totalDistance);
            Vector3 movement = getMovement(distance);

            // Move the platform, trigger and character
            platform.transform.position = platformStart + movement;
            character.transform.position = characterStart + movement;
            if (distance >= totalDistance)
            {
                // Update the destination cell by adding this trigger and set new player position
                moving = false;
                distance = 0;
                Cell destination = level.cellAtPosition(character.transform.position);
                destination.trigger = this;
                character.currentCell = destination;
                // Reverse the moves and flip the direction sign
                direction *= -1;
                moves.Reverse();
                Utility.roundPosition(platform.transform, character.transform);
            }
        }
    }

    public override void enter(bool isDestination, Character character)
    {
        if (isDestination && character.isPlayer)
        {
            // Store initial positions and set the trigger of current cell to null (since trigger of platform will be moved to the destination of the platform)
            level.cellAtPosition(character.transform.position).trigger = null;
            moving = true;
            this.character = character;
            characterStart = character.transform.position;
            platformStart = platform.transform.position;
            distance = 0;
            // Calculate total distance of moves
            totalDistance = 0;
            foreach(Vector3 move in moves)
                totalDistance += move.magnitude;
        }
    }

    // Determines the current position of the platform  relative to the start point
    Vector3 getMovement(float distance)
    {
        float remaining = distance;
        Vector3 movement = Vector3.zero;
        foreach(Vector3 move in moves)
        {
            float step = Mathf.Min(remaining, move.magnitude);
            remaining -= step;
            movement += (step/move.magnitude) * move*direction;
        }
        return movement;
    }
}
