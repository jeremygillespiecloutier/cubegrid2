﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Trigger that toggles an on/off state
public class ToggleTrigger : Trigger {

    bool active = false; // Falg indicating if the toggle is active
    Material mat1, mat2; // The materials for the active and inactive states
    public bool isActive { get { return active; } }

    private void Start()
    {
        mat1=(Material)Resources.Load("ToggleMaterial1", typeof(Material)); // Toggle inactive  material
        mat2 = (Material)Resources.Load("ToggleMaterial2", typeof(Material)); // Toggle active material
        gameObject.GetComponent<Renderer>().material = mat1;
    }

    public override void enter(bool isDestination, Character character)
    {
        // Change toggle state and set correct material
        active = !active;
        gameObject.GetComponent<Renderer>().material = active ? mat2 : mat1;
    }
}
