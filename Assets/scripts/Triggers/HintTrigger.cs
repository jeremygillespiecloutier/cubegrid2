﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HintTrigger : Trigger {

    public string message;
    Menu menu;

    private void Start()
    {
        menu = GameObject.Find("Canvas").GetComponent<Menu>();
    }

    public override void enter(bool isDestination, Character character)
    {
        if (isDestination && character.isPlayer)
        {
            menu.showDialog(message);
        }
    }

    public override void exit(bool isDestination, Character character)
    {
        menu.hideDialog();
    }
}
