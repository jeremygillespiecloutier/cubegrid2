﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Base class for triggers, methods are called when player enters and leaves the trigger
public class Trigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // Called when a character enters the trigger. isDestination is true if the move that caused the trigger was the player's destination (false if intermediate step)
    public virtual  void enter(bool isDestination, Character character) { }

    // Called when the character leaves the trigger. isDestination true if player was stopped on the trigger.
    public virtual void exit(bool isDestination, Character character) { }
}
