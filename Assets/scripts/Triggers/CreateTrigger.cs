﻿using Assets.scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class that modifies the level by adding new blocks
public class CreateTrigger : Trigger {

    public List<Vector3> relPositions; // Position of the blocks to create relative to the block that has the trigger
    Level level;
    bool created = false;

	void Start () {
        level = GameObject.Find("Level").GetComponent<Level>();	
	}

    public override void enter(bool isDestination, Character character)
    {
        if (isDestination && !created)
        {
            created = true;
            foreach(Vector3 relPos in relPositions)
            {
                Vector3 pos = transform.parent.position + relPos;
                GameObject block = (GameObject)Instantiate(Resources.Load("Cube"));
                block.tag = "Block";
                block.transform.position = pos;
                level.addBlock(pos);
            }
        }
    }

    public override void exit(bool isDestination, Character character)
    {
        if (isDestination)
        {

        }
    }

}
