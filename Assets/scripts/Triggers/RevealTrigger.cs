﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Trigger that changes the material of some blocks
public class RevealTrigger : Trigger {

    public List<GameObject> children; // The blocks to reveal/hide
    public Material hiddenMaterial, revealMaterial; // Materials to show when the blocks are visible and when they are hidden

    private void Start()
    {
        putMaterial(true);
    }

    public override void enter(bool isDestination, Character character)
    {
        // Put the reveal material on children
        if (isDestination)
            putMaterial(false);
    }

    public override void exit(bool isDestination, Character character)
    {
        // Put the hidden material on children
        if (isDestination)
            putMaterial(true);
    }

    // If hidden true, puts the hidden material on the children, otherwise puts the reveal material
    void putMaterial(bool hidden)
    {
        foreach (GameObject child in children)
            child.GetComponent<Renderer>().material = hidden?hiddenMaterial:revealMaterial;
    }

}
