﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Trigger that causes the player to die
public class TrapTrigger : Trigger {

    public override void enter(bool isDestination, Character character)
    {
        if(character.isPlayer)
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
