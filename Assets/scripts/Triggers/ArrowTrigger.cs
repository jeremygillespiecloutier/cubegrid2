﻿using Assets.scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Trigger that moves a target character in a given direction
public class ArrowTrigger : Trigger {

    public Character target; // Target character to move
    public Vector3 direction; // Direction in which to move the target (normalized)
    Level level;

    private void Start()
    {
        direction.Normalize();
        level = GameObject.Find("Level").GetComponent<Level>();
    }

    public override void enter(bool isDestination, Character character)
    {
        // Set the destination of the target
        if (isDestination)
        {
            Debug.Log("here");
            Cell next = level.cellAtPosition(target.transform.position + direction);
            if (next != null)
            {
                foreach(Link link in target.currentCell.links)
                {
                    if (link.second == next)
                    {
                        target.path.Clear();
                        target.path.Add(link);
                        break;
                    }
                }
            }
        }
    }
}
