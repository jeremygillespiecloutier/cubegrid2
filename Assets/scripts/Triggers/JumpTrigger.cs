﻿using Assets.scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Trigger that causes the player to jump
public class JumpTrigger : Trigger {

    public Vector3 vert; // The vertical direction of the jump (unit length)
    public Vector3 horiz; // The horizontal direction of the jump (unit length)
    public float width; // The width of the jump
    public float height; // The height of the jump
    Vector3 axis; // The axis player rotates around while jumping
    float halfWidth;
    Character character;
    Level level;
    bool executing = false;
    float progress = 0; // Progress of the jump, between 0 and 1
    Vector3 start; // start point of the jump
    Quaternion rotation; // Initial player rotation
    const float SPEED = 1; // Speed of jump, in progress per second

    void Start () {
        character = GameObject.Find("Player").GetComponent<Character>();
        level = GameObject.Find("Level").GetComponent<Level>();
        halfWidth = width / 2;
        vert.Normalize();
        horiz.Normalize();
        axis = Vector3.Cross(vert, horiz);
    }

	void Update () {
        if (executing)
        {
            // Jumps follow the parametric equation of an 2D ellipse with radii width/2 (on the x axis) and height (on the y axis)
            // start is the rightmost point on the x axis (t=0), the stop is the topmost point on the y axis (t=pi/2) and the
            // stop is the leftmost point on the x acis (t=pi)
            progress += Time.deltaTime * SPEED;
            progress = Mathf.Clamp(progress, 0, 1);
            float t = progress * Mathf.PI; // 
            character.transform.position = start + (halfWidth - halfWidth * Mathf.Cos(t)) * horiz + Mathf.Sin(t) * height*vert;
            character.transform.rotation = rotation;
            character.transform.Rotate(axis, progress * 360);
            // Jump finished, updare player position
            if (progress >= 1)
            {
                executing = false;
                character.currentCell = level.cellAtPosition(character.transform.position);
            }
        }
	}

    public override void enter(bool isDestination, Character character)
    {
        if (isDestination)
        {
            // Prepare the jump
            this.character = character;
            executing = true;
            progress = 0;
            start = character.transform.position;
            rotation = character.transform.rotation;
        }
    }
}
