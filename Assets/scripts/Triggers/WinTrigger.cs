﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Trigger that makes the player complete the level
public class WinTrigger : Trigger {

    Menu menu;

    private void Start()
    {
        menu = GameObject.Find("Canvas").GetComponent<Menu>();
    }

    public override void enter(bool isDestination, Character character)
    {
        if (character.isPlayer)
        {
            character.path.Clear();
            menu.finishLevel();
        }
    }

}
