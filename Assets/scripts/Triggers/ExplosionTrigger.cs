﻿using Assets.scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Trigger that destroys the target block
public class ExplosionTrigger : Trigger {

    public GameObject target;
    Level level;
    bool destroyed = false;

    private void Start()
    {
        level = GameObject.Find("Level").GetComponent<Level>();
    }

    public override void enter(bool isDestination, Character character)
    {
        if (!destroyed)
        {
            Cell targetCell = level.cellAtPosition(target.transform.position);
            targetCell.isObstacle = false;
            Destroy(target);
            destroyed = true;
        }
    }
}
