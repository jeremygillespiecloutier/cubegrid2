﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Trigger that starts a camera animation
public class SpectateTrigger : Trigger {

    Animator animator;
    public string animationName; // The name of the camera animation to trigger (defined in animator)
    bool animating = false; // Flag indicating if animation is currently active
    bool previousAnimating = false; // Was the animation active last frame

    private void Start()
    {
        animator = Camera.main.GetComponent<Animator>();
    }

    private void Update()
    {
        if (animating && previousAnimating) // Need to run at least one frame before detecting initial animator state change
        {
            AnimatorStateInfo info = animator.GetCurrentAnimatorStateInfo(0);
            if (!info.IsName(animationName))
            {
                animator.enabled = false;
                animating = false;
            }
        }
        previousAnimating = animating;
    }

    public override void enter(bool isDestination, Character character)
    {
        if (character.isPlayer)
        {
            animator.enabled = true;
            animator.Play(animationName);
            animating = true;
        }
    }

}
