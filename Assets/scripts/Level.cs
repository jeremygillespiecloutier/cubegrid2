﻿using Assets.scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Generates the level
public class Level : MonoBehaviour {

    public Cell[,,] cells;


    void Awake () {
        createGrid();
	}

    void createGrid()
    {
        // The min and max block coordinates in the level
        float minX =float.MaxValue, maxX=float.MinValue, minY=float.MaxValue, maxY=float.MinValue, minZ=float.MaxValue, maxZ=float.MinValue;
        foreach (GameObject block in GameObject.FindGameObjectsWithTag("Block"))
        {
            Vector3 pos = block.transform.position;
            minX = Mathf.Min(minX, pos.x);
            maxX = Mathf.Max(maxX, pos.x);
            minY = Mathf.Min(minY, pos.y);
            maxY = Mathf.Max(maxY, pos.y);
            minZ = Mathf.Min(minZ, pos.z);
            maxZ = Mathf.Max(maxZ, pos.z);
        }

        // Construct the array of cells
        int overflow = 3; // Extra space for cells beyond the min/max in each axis
        transform.position += new Vector3(-(minX - overflow), -(minY - overflow), -(minZ - overflow));
        int xLength = (int)Mathf.Round(maxX - minX) + 2*overflow;
        int yLength = (int)Mathf.Round(maxY - minY) + 2 * overflow;
        int zLength = (int)Mathf.Round(maxZ - minZ) + 2 * overflow;
        cells = new Cell[xLength, yLength, zLength];

        // Create cells for each block
        foreach(GameObject block in GameObject.FindGameObjectsWithTag("Block"))
        {
            Vector3 pos = block.transform.position;
            Cell cell = new Cell((int)pos.x, (int)pos.y, (int)pos.z, true);
            cells[(int)pos.x, (int)pos.y, (int)pos.z] = cell;
            // Hide blocks that have a child with the Hidden tag
            for(int i = 0; i < block.transform.childCount; i++)
            {
                if (block.transform.GetChild(i).tag == "Hidden")
                {
                    foreach (Renderer renderer in block.GetComponentsInChildren<Renderer>())
                        renderer.enabled = false;
                    break;
                }
            }
        }

        // Create empty cells adjacent to blocks
        foreach (GameObject block in GameObject.FindGameObjectsWithTag("Block"))
        {
            Vector3 pos = block.transform.position;
            int x = (int)pos.x, y = (int)pos.y, z = (int)pos.z;
            if (cells[x + 1, y, z] == null)
                cells[x + 1, y, z] = new Cell(x+1,y,z, false);
            if (cells[x - 1, y, z] == null)
                cells[x - 1, y, z] = new Cell(x-1,y,z, false);
            if (cells[x, y+1, z] == null)
                cells[x, y+1, z] = new Cell(x,y+1,z, false);
            if (cells[x, y - 1, z] == null)
                cells[x, y - 1, z] = new Cell(x,y-1,z, false);
            if (cells[x, y, z+1] == null)
                cells[x, y, z+1] = new Cell(x,y,z+1, false);
            if (cells[x, y, z - 1] == null)
                cells[x, y, z - 1] = new Cell(x,y,z-1, false);
        }

        // Mark cells that contain obstacles
        foreach(GameObject obstacle in GameObject.FindGameObjectsWithTag("Obstacle"))
        {
            Vector3 pos = obstacle.transform.position;
            Cell cell = cellAtPosition(pos);
            if(cell!=null)
                cell.isObstacle=true;
        }

        // Detect cube faces that are not reachable
        foreach(GameObject obstacleFace in GameObject.FindGameObjectsWithTag("ObstacleFace"))
        {
            Vector3 pos = obstacleFace.transform.position + 2 * (obstacleFace.transform.position - obstacleFace.transform.parent.position);
            Cell cell = cellAtPosition(pos);
            if (cell != null)
                cell.isObstacle = true;
        }

        // Attach triggers to their corresponding cells
        foreach(Trigger trigger in ((Trigger[])FindObjectsOfType(typeof(Trigger))))
        {
            Vector3 diff = (trigger.transform.position - trigger.transform.parent.position) * 2; // Vector from center of cube that has trigger to adjacent cell
            Vector3 pos = trigger.transform.parent.position + diff; // Position of adjacent cell that initiates the trigger
            Cell cell = cells[(int)pos.x, (int)pos.y, (int)pos.z];
            if(cell!=null)
                cell.trigger = trigger.GetComponent<Trigger>();
        }

        // Associate fragments to their corresponding cells
        foreach(GameObject fragment in GameObject.FindGameObjectsWithTag("Fragment"))
        {
            Vector3 pos = fragment.transform.position;
            Cell cell = cells[(int)pos.x, (int)pos.y, (int)pos.z];
            if (cell != null)
                cell.fragment = fragment;
        }

        foreach(Cell cell in cells)
        {
            if (cell == null)
                continue;

            // Get blocks adjacent to empty cells
            if (!cell.isBlock)
            {
                cell.blocks.Clear();
                Cell other;
                if ((other = cells[cell.x - 1, cell.y, cell.z]) != null && other.isBlock)
                    cell.blocks.Add(other);
                if ((other = cells[cell.x + 1, cell.y, cell.z]) != null && other.isBlock)
                    cell.blocks.Add(other);
                if ((other = cells[cell.x, cell.y - 1, cell.z]) != null && other.isBlock)
                    cell.blocks.Add(other);
                if ((other = cells[cell.x, cell.y + 1, cell.z]) != null && other.isBlock)
                    cell.blocks.Add(other);
                if ((other = cells[cell.x, cell.y, cell.z - 1]) != null && other.isBlock)
                    cell.blocks.Add(other);
                if ((other = cells[cell.x, cell.y, cell.z + 1]) != null && other.isBlock)
                    cell.blocks.Add(other);
            }
        }

        foreach(Cell cell in cells)
        {
            if (cell == null || cell.isBlock)
                continue;
            // Find pairs of adjacent empty cells that allow movement
            foreach(Cell other in adjacentCells(cell, false)){
                if (!other.isBlock)
                {
                    // Add a link to allow movement from the first cell to the second cell
                    createLink(cell, other, false);
                }
            }
        }

    }

    // Return the cells adjacent to the given cell.
    // If direct is true, only returns the adjacent cells that share a face, otherwise those that share an edge
    List<Cell> adjacentCells(Cell cell, bool direct)
    {
        List<Cell> adjacent = new List<Cell>();
        for (int i = -1; i <= 1; i++)
        {
            for (int j = -1; j <= 1; j++)
            {
                for (int k = -1; k <= 1; k++)
                {
                    if ((direct && Mathf.Abs(i)+Mathf.Abs(j)+Mathf.Abs(k)==1) || 
                        (!direct && !(i == 0 && j == 0 && k == 0) && !(Mathf.Abs(i) == 1 && Mathf.Abs(j) == 1 && Mathf.Abs(k) == 1)))
                    {
                        Vector3 pos = cell.pos + new Vector3(i, j, k);
                        if (cellAtPosition(pos) != null)
                            adjacent.Add(cellAtPosition(pos));
                    }
                }
            }
        }
        return adjacent;
    }

    // Adds a new block to the level at the given position (after the level has already been created)
    public void addBlock(Vector3 pos)
    {
        if (cellAtPosition(pos) != null)
        {
            // If block already exists at given position, do nothing
            Cell previous = cellAtPosition(pos);
            if (previous.isBlock)
                return;
            // if empty cell already exists. remove links from neighbors of this cell to this cell
            foreach(Cell cell in adjacentCells(previous, false))
            {
                for(int i = cell.links.Count - 1; i >= 0; i--)
                {
                    Link link = cell.links[i];
                    if (link.second == previous)
                        cell.links.RemoveAt(i);
                }
            }
        }

        // Create the block
        Cell block = new Cell((int)Mathf.Round(pos.x), (int)Mathf.Round(pos.y), (int)Mathf.Round(pos.z), true);
        cells[block.x, block.y, block.z] = block;
        // Create empty cells adjacent to this block if needed and update neighbors block list
        for (int i = -1; i <= 1; i++)
            for (int j = -1; j <= 1; j++)
                for (int k = -1; k <= 1; k++)
                    if (Mathf.Abs(i)+Mathf.Abs(j)+Mathf.Abs(k) == 1)
                    {
                        if (cells[block.x + i, block.y + j, block.z + k] == null)
                            cells[block.x + i, block.y + j, block.z + k] = new Cell(block.x + i, block.y + j, block.z + k, false);
                        Cell cell = cells[block.x + i, block.y + j, block.z + k];
                        if (!cell.isBlock)
                            cell.blocks.Add(block);
                    }

        // Create links between pairs of empty neighbors of this block
        List<Cell> adjacent = adjacentCells(block, false);
        foreach (Cell cell1 in adjacent)
            if(!cell1.isBlock)
                foreach (Cell cell2 in adjacent)
                    if (!cell2.isBlock && cell1 != cell2)
                        createLink(cell1, cell2, true);
    }

    // Creates a movement link from the first cell to the second cell
    // And also from the second cell to the first cell if twofold is set to true
    void createLink(Cell cell1, Cell cell2, bool twofold)
    {
        float epsilon = 0.05f;
        // Find matching edge midpoints
        foreach(Vector3 point1 in cell1.midPoints)
        {
            foreach(Vector3 point2 in cell2.midPoints)
            {
                if ((point1 - point2).sqrMagnitude < epsilon)
                {
                    bool found = false;
                    Cell block1 = null, block2 = null;
                    // Find block adjacent to first cell that also shares this midpoint
                    foreach(Cell cell in cell1.blocks)
                    {
                        foreach(Vector3 point in cell.midPoints)
                        {
                            if ((point - point1).sqrMagnitude < epsilon)
                            {
                                block1 = cell;
                                found = true;
                                break;
                            }
                        }
                    }
                    if (!found)
                        continue;
                    found = false;
                    // Find block adjacent to second cell that also shares this midpoint
                    foreach(Cell cell in cell2.blocks)
                    {
                        foreach(Vector3 point in cell.midPoints)
                        {
                            if ((point - point1).sqrMagnitude < epsilon)
                            {
                                block2 = cell;
                                found = true;
                                break;
                            }
                        }
                    }
                    // If the first and second cell both have a block that share their common edge midpoint
                    // We can use this midpoint as the pivot for the link.
                    if (found)
                    {
                        // Determine the rotation axis and create the link from cell1 to cell2
                        Vector3 center = (block1.pos + block2.pos)/2f;
                        Vector3 diff = cell2.pos - cell1.pos;
                        Vector3 normal = point1 - center;
                        Link link = new Link();
                        link.pivot = point1;
                        link.first = cell1;
                        link.second = cell2;
                        link.axis = Vector3.Cross(normal, diff);
                        link.angle = (block1 == block2) ? 180 : 90;
                        cell1.links.Add(link);

                        if (twofold) //If flag is set, also add link from second to first cell
                        {
                            Link link2 = new Link();
                            link2.pivot = point1;
                            link2.first = cell2;
                            link2.second = cell1;
                            link2.axis = -link.axis;
                            link2.angle = link.angle;
                            cell2.links.Add(link2);
                        }
                        return;
                    }
                }
            }
        }
    }

    // returns the cell associated with the given position
    public Cell cellAtPosition(Vector3 position)
    {
        return cells[(int)Mathf.Round(position.x), (int)Mathf.Round(position.y), (int)Mathf.Round(position.z)];
    }
}
