﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// ArrowTrigger
struct ArrowTrigger_t1089668331;
// Trigger
struct Trigger_t3844394560;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// Level
struct Level_t866853782;
// Character
struct Character_t3726027591;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.Transform
struct Transform_t3275118058;
// Assets.scripts.Cell
struct Cell_t964604196;
// System.Collections.Generic.List`1<Assets.scripts.Link>
struct List_1_t145766874;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// Assets.scripts.Link
struct Link_t776645742;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Collections.Generic.List`1<Assets.scripts.Cell>
struct List_1_t333725328;
// Assets.scripts.Utility
struct Utility_t2627328774;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.Object
struct Object_t1021602117;
// Game
struct Game_t1600141214;
// CreateTrigger
struct CreateTrigger_t2237538136;
// ExplosionTrigger
struct ExplosionTrigger_t4191861867;
// Menu
struct Menu_t4261767481;
// Pathfinder
struct Pathfinder_t1592129589;
// InputController
struct InputController_t3951031284;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Animator
struct Animator_t69676727;
// UnityEngine.Behaviour
struct Behaviour_t955675639;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// GridSnap
struct GridSnap_t338261636;
// HintTrigger
struct HintTrigger_t910047485;
// UnityEngine.Touch[]
struct TouchU5BU5D_t3887265178;
// JumpTrigger
struct JumpTrigger_t1208645988;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t2810717544;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// UnityEngine.Renderer
struct Renderer_t257310565;
// System.Type
struct Type_t;
// UnityEngine.Object[]
struct ObjectU5BU5D_t4217747464;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t2455055323;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// UnityEngine.UI.Image
struct Image_t2042527209;
// ToggleButton
struct ToggleButton_t3926247120;
// UnityEngine.Sprite
struct Sprite_t309593783;
// PlatformTrigger
struct PlatformTrigger_t3477208093;
// RevealTrigger
struct RevealTrigger_t3929941135;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.Material
struct Material_t193706927;
// SpectateTrigger
struct SpectateTrigger_t3335062351;
// ToggleTrigger
struct ToggleTrigger_t2585261492;
// TrapTrigger
struct TrapTrigger_t598219061;
// WinTrigger
struct WinTrigger_t872747498;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// Assets.scripts.Cell[]
struct CellU5BU5D_t1218597325;
// Assets.scripts.Link[]
struct LinkU5BU5D_t1181425179;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2295673753;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t339478082;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Void
struct Void_t1841601450;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.UI.Selectable
struct Selectable_t1490392188;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Reflection.MemberFilter
struct MemberFilter_t3405857066;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t834278767;
// Assets.scripts.Cell[0...,0...,0...]
struct CellU5BU2CU2CU5D_t1218597327;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
struct List_1_t664902677;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t1295781545;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem>
struct List_1_t2835956395;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t2681005625;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t1282925227;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t261436805;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t385374196;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3177091249;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t859513320;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t3244928895;
// UnityEngine.UI.Graphic
struct Graphic_t2426225576;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t2665681875;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t1156185964;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3778758259;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// UnityEngine.UI.FontData
struct FontData_t2614388407;
// UnityEngine.TextGenerator
struct TextGenerator_t647235000;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t3048644023;

extern const RuntimeMethod* GameObject_GetComponent_TisLevel_t866853782_m3318339701_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1594932300;
extern const uint32_t ArrowTrigger_Start_m454924842_MetadataUsageId;
extern RuntimeClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_GetEnumerator_m3554822035_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m1037497887_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m1988879339_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m257155336_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m218825171_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m2728416437_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1407338552;
extern const uint32_t ArrowTrigger_enter_m3090982584_MetadataUsageId;
extern RuntimeClass* List_1_t1612828712_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t333725328_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t145766874_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m347461442_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m2909086300_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m1908755428_RuntimeMethod_var;
extern const uint32_t Cell__ctor_m2554603460_MetadataUsageId;
extern RuntimeClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_Add_m2338641291_RuntimeMethod_var;
extern const uint32_t Cell_calculateMidpoints_m4202269553_MetadataUsageId;
extern const uint32_t Utility_roundPosition_m3988846968_MetadataUsageId;
extern const uint32_t Character__ctor_m1610743918_MetadataUsageId;
extern RuntimeClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1875862075;
extern const uint32_t Character_get_isPlayer_m2500959794_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisGame_t1600141214_m3908916829_RuntimeMethod_var;
extern const uint32_t Character_Start_m2430444898_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Count_m3786137094_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m914633333_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAt_m2271034946_RuntimeMethod_var;
extern const uint32_t Character_moveCharacter_m4256772666_MetadataUsageId;
extern const uint32_t Character_reachedCell_m2549832251_MetadataUsageId;
extern const uint32_t CreateTrigger_Start_m1083303839_MetadataUsageId;
extern RuntimeClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_GetEnumerator_m940839541_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m857008049_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m2007266881_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m3215924523_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3665939501;
extern Il2CppCodeGenString* _stringLiteral3745862197;
extern const uint32_t CreateTrigger_enter_m956692605_MetadataUsageId;
extern const uint32_t ExplosionTrigger_Start_m1958706572_MetadataUsageId;
extern const uint32_t ExplosionTrigger_enter_m4019835266_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisLevel_t866853782_m4130163129_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisPathfinder_t1592129589_m1369771324_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisCharacter_t3726027591_m2667239018_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisMenu_t4261767481_m3170984502_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisInputController_t3951031284_m1126083993_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisAnimator_t69676727_m475627522_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1047780110;
extern const uint32_t Game_Start_m3679318685_MetadataUsageId;
extern RuntimeClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern RuntimeClass* EventSystem_t3466835263_il2cpp_TypeInfo_var;
extern const uint32_t Game_Update_m3455215516_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Game_movePlayer_m4079979315_MetadataUsageId;
extern const uint32_t GridSnap_Update_m175371526_MetadataUsageId;
extern const uint32_t HintTrigger_Start_m599260802_MetadataUsageId;
extern RuntimeClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t InputController_Update_m3485699262_MetadataUsageId;
extern const uint32_t JumpTrigger_Start_m2965133417_MetadataUsageId;
extern const uint32_t JumpTrigger_Update_m717189614_MetadataUsageId;
extern const RuntimeType* Trigger_t3844394560_0_0_0_var;
extern RuntimeClass* CellU5BU2CU2CU5D_t1218597327_il2cpp_TypeInfo_var;
extern RuntimeClass* Cell_t964604196_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* TriggerU5BU5D_t3145789633_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponentsInChildren_TisRenderer_t257310565_m2883698982_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisTrigger_t3844394560_m3031482907_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m3715491197_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1728370848_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m174512497_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m4246516629_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m1435586957_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m3632502891_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2025730826;
extern Il2CppCodeGenString* _stringLiteral2124864605;
extern Il2CppCodeGenString* _stringLiteral1093546118;
extern Il2CppCodeGenString* _stringLiteral2596549766;
extern const uint32_t Level_createGrid_m485254149_MetadataUsageId;
extern const uint32_t Level_adjacentCells_m69322768_MetadataUsageId;
extern const uint32_t Level_addBlock_m1332986222_MetadataUsageId;
extern RuntimeClass* Link_t776645742_il2cpp_TypeInfo_var;
extern const uint32_t Level_createLink_m2520085500_MetadataUsageId;
extern const uint32_t Level_cellAtPosition_m891825944_MetadataUsageId;
extern RuntimeClass* UnityAction_t4025899511_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TisText_t356221433_m1217399699_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisButton_t2872111280_m1008560876_RuntimeMethod_var;
extern const RuntimeMethod* Menu_U3CStartU3Em__0_m959812099_RuntimeMethod_var;
extern const RuntimeMethod* Menu_U3CStartU3Em__1_m959812004_RuntimeMethod_var;
extern const RuntimeMethod* Menu_U3CStartU3Em__2_m959812033_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2995787169;
extern Il2CppCodeGenString* _stringLiteral2386992142;
extern Il2CppCodeGenString* _stringLiteral3343504797;
extern Il2CppCodeGenString* _stringLiteral2686302870;
extern Il2CppCodeGenString* _stringLiteral618982595;
extern Il2CppCodeGenString* _stringLiteral1761466451;
extern Il2CppCodeGenString* _stringLiteral2998934070;
extern Il2CppCodeGenString* _stringLiteral1659439092;
extern const uint32_t Menu_Start_m3632225306_MetadataUsageId;
extern RuntimeClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1993429030;
extern Il2CppCodeGenString* _stringLiteral372029391;
extern const uint32_t Menu_updateFragments_m871188344_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisImage_t2042527209_m2189462422_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisToggleButton_t3926247120_m2729835643_RuntimeMethod_var;
extern const uint32_t Menu_U3CStartU3Em__2_m959812033_MetadataUsageId;
extern const uint32_t Pathfinder_Start_m3564303874_MetadataUsageId;
extern const RuntimeMethod* List_1_Remove_m1927217927_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Contains_m2414020574_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m349349022_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Insert_m2431457653_RuntimeMethod_var;
extern const uint32_t Pathfinder_findPath_m1726079702_MetadataUsageId;
extern const uint32_t PlatformTrigger_Start_m2311334308_MetadataUsageId;
extern RuntimeClass* TransformU5BU5D_t3764228911_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_Reverse_m723397084_RuntimeMethod_var;
extern const uint32_t PlatformTrigger_Update_m3597706753_MetadataUsageId;
extern const uint32_t PlatformTrigger_enter_m1061072086_MetadataUsageId;
extern const uint32_t PlatformTrigger_getMovement_m3962613840_MetadataUsageId;
extern const RuntimeMethod* List_1_GetEnumerator_m970439620_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2389888842_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisRenderer_t257310565_m1312615893_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m3635305532_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m1242097970_RuntimeMethod_var;
extern const uint32_t RevealTrigger_putMaterial_m3028200551_MetadataUsageId;
extern const uint32_t SpectateTrigger_Start_m1580828672_MetadataUsageId;
extern const RuntimeType* Material_t193706927_0_0_0_var;
extern RuntimeClass* Material_t193706927_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral572223054;
extern Il2CppCodeGenString* _stringLiteral572223055;
extern const uint32_t ToggleTrigger_Start_m3095179001_MetadataUsageId;
extern const uint32_t ToggleTrigger_enter_m1707427839_MetadataUsageId;
extern const uint32_t WinTrigger_Start_m2621660491_MetadataUsageId;
extern const uint32_t WinTrigger_enter_m3251762553_MetadataUsageId;

struct TransformU5BU5D_t3764228911;
struct CellU5BU2CU2CU5D_t1218597327;
struct TouchU5BU5D_t3887265178;
struct GameObjectU5BU5D_t3057952154;
struct RendererU5BU5D_t2810717544;
struct TriggerU5BU5D_t3145789633;
struct ObjectU5BU5D_t4217747464;


#ifndef U3CMODULEU3E_T3783534231_H
#define U3CMODULEU3E_T3783534231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534231 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534231_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef LIST_1_T1612828712_H
#define LIST_1_T1612828712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_t1612828712  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_t1172311765* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1612828712, ____items_1)); }
	inline Vector3U5BU5D_t1172311765* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_t1172311765** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_t1172311765* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1612828712, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1612828712, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1612828712_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector3U5BU5D_t1172311765* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1612828712_StaticFields, ___EmptyArray_4)); }
	inline Vector3U5BU5D_t1172311765* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector3U5BU5D_t1172311765** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector3U5BU5D_t1172311765* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1612828712_H
#ifndef LIST_1_T333725328_H
#define LIST_1_T333725328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Assets.scripts.Cell>
struct  List_1_t333725328  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	CellU5BU5D_t1218597325* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t333725328, ____items_1)); }
	inline CellU5BU5D_t1218597325* get__items_1() const { return ____items_1; }
	inline CellU5BU5D_t1218597325** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(CellU5BU5D_t1218597325* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t333725328, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t333725328, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t333725328_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	CellU5BU5D_t1218597325* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t333725328_StaticFields, ___EmptyArray_4)); }
	inline CellU5BU5D_t1218597325* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline CellU5BU5D_t1218597325** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(CellU5BU5D_t1218597325* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T333725328_H
#ifndef LIST_1_T1125654279_H
#define LIST_1_T1125654279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct  List_1_t1125654279  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GameObjectU5BU5D_t3057952154* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1125654279, ____items_1)); }
	inline GameObjectU5BU5D_t3057952154* get__items_1() const { return ____items_1; }
	inline GameObjectU5BU5D_t3057952154** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GameObjectU5BU5D_t3057952154* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1125654279, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1125654279, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1125654279_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	GameObjectU5BU5D_t3057952154* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1125654279_StaticFields, ___EmptyArray_4)); }
	inline GameObjectU5BU5D_t3057952154* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(GameObjectU5BU5D_t3057952154* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1125654279_H
#ifndef LIST_1_T145766874_H
#define LIST_1_T145766874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Assets.scripts.Link>
struct  List_1_t145766874  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	LinkU5BU5D_t1181425179* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t145766874, ____items_1)); }
	inline LinkU5BU5D_t1181425179* get__items_1() const { return ____items_1; }
	inline LinkU5BU5D_t1181425179** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(LinkU5BU5D_t1181425179* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t145766874, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t145766874, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t145766874_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	LinkU5BU5D_t1181425179* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t145766874_StaticFields, ___EmptyArray_4)); }
	inline LinkU5BU5D_t1181425179* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline LinkU5BU5D_t1181425179** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(LinkU5BU5D_t1181425179* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T145766874_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef UTILITY_T2627328774_H
#define UTILITY_T2627328774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets.scripts.Utility
struct  Utility_t2627328774  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILITY_T2627328774_H
#ifndef UNITYEVENTBASE_T828812576_H
#define UNITYEVENTBASE_T828812576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t828812576  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2295673753 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t339478082 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_Calls_0)); }
	inline InvokableCallList_t2295673753 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2295673753 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2295673753 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t339478082 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t339478082 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t339478082 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T828812576_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t1328083999* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t1328083999* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t1328083999** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t1328083999* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef UNITYEVENT_T408735097_H
#define UNITYEVENT_T408735097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t408735097  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t408735097, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T408735097_H
#ifndef QUATERNION_T4030073918_H
#define QUATERNION_T4030073918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t4030073918 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t4030073918_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t4030073918  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t4030073918  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t4030073918 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t4030073918  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T4030073918_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef SCENE_T1684909666_H
#define SCENE_T1684909666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t1684909666 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t1684909666, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T1684909666_H
#ifndef ANIMATORSTATEINFO_T2577870592_H
#define ANIMATORSTATEINFO_T2577870592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorStateInfo
struct  AnimatorStateInfo_t2577870592 
{
public:
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Name
	int32_t ___m_Name_0;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Path
	int32_t ___m_Path_1;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_FullPath
	int32_t ___m_FullPath_2;
	// System.Single UnityEngine.AnimatorStateInfo::m_NormalizedTime
	float ___m_NormalizedTime_3;
	// System.Single UnityEngine.AnimatorStateInfo::m_Length
	float ___m_Length_4;
	// System.Single UnityEngine.AnimatorStateInfo::m_Speed
	float ___m_Speed_5;
	// System.Single UnityEngine.AnimatorStateInfo::m_SpeedMultiplier
	float ___m_SpeedMultiplier_6;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Tag
	int32_t ___m_Tag_7;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Loop
	int32_t ___m_Loop_8;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t2577870592, ___m_Name_0)); }
	inline int32_t get_m_Name_0() const { return ___m_Name_0; }
	inline int32_t* get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(int32_t value)
	{
		___m_Name_0 = value;
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t2577870592, ___m_Path_1)); }
	inline int32_t get_m_Path_1() const { return ___m_Path_1; }
	inline int32_t* get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(int32_t value)
	{
		___m_Path_1 = value;
	}

	inline static int32_t get_offset_of_m_FullPath_2() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t2577870592, ___m_FullPath_2)); }
	inline int32_t get_m_FullPath_2() const { return ___m_FullPath_2; }
	inline int32_t* get_address_of_m_FullPath_2() { return &___m_FullPath_2; }
	inline void set_m_FullPath_2(int32_t value)
	{
		___m_FullPath_2 = value;
	}

	inline static int32_t get_offset_of_m_NormalizedTime_3() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t2577870592, ___m_NormalizedTime_3)); }
	inline float get_m_NormalizedTime_3() const { return ___m_NormalizedTime_3; }
	inline float* get_address_of_m_NormalizedTime_3() { return &___m_NormalizedTime_3; }
	inline void set_m_NormalizedTime_3(float value)
	{
		___m_NormalizedTime_3 = value;
	}

	inline static int32_t get_offset_of_m_Length_4() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t2577870592, ___m_Length_4)); }
	inline float get_m_Length_4() const { return ___m_Length_4; }
	inline float* get_address_of_m_Length_4() { return &___m_Length_4; }
	inline void set_m_Length_4(float value)
	{
		___m_Length_4 = value;
	}

	inline static int32_t get_offset_of_m_Speed_5() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t2577870592, ___m_Speed_5)); }
	inline float get_m_Speed_5() const { return ___m_Speed_5; }
	inline float* get_address_of_m_Speed_5() { return &___m_Speed_5; }
	inline void set_m_Speed_5(float value)
	{
		___m_Speed_5 = value;
	}

	inline static int32_t get_offset_of_m_SpeedMultiplier_6() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t2577870592, ___m_SpeedMultiplier_6)); }
	inline float get_m_SpeedMultiplier_6() const { return ___m_SpeedMultiplier_6; }
	inline float* get_address_of_m_SpeedMultiplier_6() { return &___m_SpeedMultiplier_6; }
	inline void set_m_SpeedMultiplier_6(float value)
	{
		___m_SpeedMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_m_Tag_7() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t2577870592, ___m_Tag_7)); }
	inline int32_t get_m_Tag_7() const { return ___m_Tag_7; }
	inline int32_t* get_address_of_m_Tag_7() { return &___m_Tag_7; }
	inline void set_m_Tag_7(int32_t value)
	{
		___m_Tag_7 = value;
	}

	inline static int32_t get_offset_of_m_Loop_8() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t2577870592, ___m_Loop_8)); }
	inline int32_t get_m_Loop_8() const { return ___m_Loop_8; }
	inline int32_t* get_address_of_m_Loop_8() { return &___m_Loop_8; }
	inline void set_m_Loop_8(int32_t value)
	{
		___m_Loop_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORSTATEINFO_T2577870592_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef ENUMERATOR_T660383953_H
#define ENUMERATOR_T660383953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>
struct  Enumerator_t660383953 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1125654279 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	GameObject_t1756533147 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t660383953, ___l_0)); }
	inline List_1_t1125654279 * get_l_0() const { return ___l_0; }
	inline List_1_t1125654279 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1125654279 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t660383953, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t660383953, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t660383953, ___current_3)); }
	inline GameObject_t1756533147 * get_current_3() const { return ___current_3; }
	inline GameObject_t1756533147 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(GameObject_t1756533147 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T660383953_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef ENUMERATOR_T4163422298_H
#define ENUMERATOR_T4163422298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<Assets.scripts.Cell>
struct  Enumerator_t4163422298 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t333725328 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Cell_t964604196 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t4163422298, ___l_0)); }
	inline List_1_t333725328 * get_l_0() const { return ___l_0; }
	inline List_1_t333725328 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t333725328 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t4163422298, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t4163422298, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t4163422298, ___current_3)); }
	inline Cell_t964604196 * get_current_3() const { return ___current_3; }
	inline Cell_t964604196 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Cell_t964604196 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T4163422298_H
#ifndef ENUMERATOR_T3975463844_H
#define ENUMERATOR_T3975463844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<Assets.scripts.Link>
struct  Enumerator_t3975463844 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t145766874 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Link_t776645742 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t3975463844, ___l_0)); }
	inline List_1_t145766874 * get_l_0() const { return ___l_0; }
	inline List_1_t145766874 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t145766874 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3975463844, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t3975463844, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3975463844, ___current_3)); }
	inline Link_t776645742 * get_current_3() const { return ___current_3; }
	inline Link_t776645742 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Link_t776645742 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3975463844_H
#ifndef ENUMERATOR_T1593300101_H
#define ENUMERATOR_T1593300101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t1593300101 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t2058570427 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t1593300101, ___l_0)); }
	inline List_1_t2058570427 * get_l_0() const { return ___l_0; }
	inline List_1_t2058570427 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t2058570427 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1593300101, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t1593300101, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1593300101, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1593300101_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef SPRITESTATE_T1353336012_H
#define SPRITESTATE_T1353336012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1353336012 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t309593783 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t309593783 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t309593783 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1353336012, ___m_HighlightedSprite_0)); }
	inline Sprite_t309593783 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t309593783 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t309593783 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1353336012, ___m_PressedSprite_1)); }
	inline Sprite_t309593783 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t309593783 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t309593783 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1353336012, ___m_DisabledSprite_2)); }
	inline Sprite_t309593783 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t309593783 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t309593783 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1353336012_marshaled_pinvoke
{
	Sprite_t309593783 * ___m_HighlightedSprite_0;
	Sprite_t309593783 * ___m_PressedSprite_1;
	Sprite_t309593783 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1353336012_marshaled_com
{
	Sprite_t309593783 * ___m_HighlightedSprite_0;
	Sprite_t309593783 * ___m_PressedSprite_1;
	Sprite_t309593783 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1353336012_H
#ifndef TOUCHPHASE_T2458120420_H
#define TOUCHPHASE_T2458120420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t2458120420 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t2458120420, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T2458120420_H
#ifndef RUNTIMETYPEHANDLE_T2330101084_H
#define RUNTIMETYPEHANDLE_T2330101084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t2330101084 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t2330101084, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T2330101084_H
#ifndef BUTTONCLICKEDEVENT_T2455055323_H
#define BUTTONCLICKEDEVENT_T2455055323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Button/ButtonClickedEvent
struct  ButtonClickedEvent_t2455055323  : public UnityEvent_t408735097
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONCLICKEDEVENT_T2455055323_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef FILLMETHOD_T1640962579_H
#define FILLMETHOD_T1640962579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t1640962579 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillMethod_t1640962579, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T1640962579_H
#ifndef COLORBLOCK_T2652774230_H
#define COLORBLOCK_T2652774230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2652774230 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2020392075  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2020392075  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2020392075  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2020392075  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_NormalColor_0)); }
	inline Color_t2020392075  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2020392075 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2020392075  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_HighlightedColor_1)); }
	inline Color_t2020392075  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2020392075 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2020392075  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_PressedColor_2)); }
	inline Color_t2020392075  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2020392075 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2020392075  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_DisabledColor_3)); }
	inline Color_t2020392075  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2020392075 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2020392075  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2652774230_H
#ifndef TRANSITION_T605142169_H
#define TRANSITION_T605142169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t605142169 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t605142169, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T605142169_H
#ifndef TYPE_T3352948571_H
#define TYPE_T3352948571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t3352948571 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t3352948571, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T3352948571_H
#ifndef TOUCHTYPE_T2732027771_H
#define TOUCHTYPE_T2732027771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t2732027771 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t2732027771, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T2732027771_H
#ifndef MODE_T1081683921_H
#define MODE_T1081683921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1081683921 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1081683921, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1081683921_H
#ifndef SELECTIONSTATE_T3187567897_H
#define SELECTIONSTATE_T3187567897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t3187567897 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t3187567897, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T3187567897_H
#ifndef BINDINGFLAGS_T1082350898_H
#define BINDINGFLAGS_T1082350898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t1082350898 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t1082350898, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T1082350898_H
#ifndef RAYCASTHIT_T87180320_H
#define RAYCASTHIT_T87180320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t87180320 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t2243707580  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t2243707580  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2243707579  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t3497673348 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Point_0)); }
	inline Vector3_t2243707580  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t2243707580 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t2243707580  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Normal_1)); }
	inline Vector3_t2243707580  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t2243707580 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t2243707580  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_UV_4)); }
	inline Vector2_t2243707579  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2243707579 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2243707579  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Collider_5)); }
	inline Collider_t3497673348 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t3497673348 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t3497673348 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t87180320_marshaled_pinvoke
{
	Vector3_t2243707580  ___m_Point_0;
	Vector3_t2243707580  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2243707579  ___m_UV_4;
	Collider_t3497673348 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t87180320_marshaled_com
{
	Vector3_t2243707580  ___m_Point_0;
	Vector3_t2243707580  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2243707579  ___m_UV_4;
	Collider_t3497673348 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T87180320_H
#ifndef LINK_T776645742_H
#define LINK_T776645742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets.scripts.Link
struct  Link_t776645742  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Assets.scripts.Link::axis
	Vector3_t2243707580  ___axis_0;
	// UnityEngine.Vector3 Assets.scripts.Link::pivot
	Vector3_t2243707580  ___pivot_1;
	// System.Single Assets.scripts.Link::angle
	float ___angle_2;
	// Assets.scripts.Cell Assets.scripts.Link::first
	Cell_t964604196 * ___first_3;
	// Assets.scripts.Cell Assets.scripts.Link::second
	Cell_t964604196 * ___second_4;

public:
	inline static int32_t get_offset_of_axis_0() { return static_cast<int32_t>(offsetof(Link_t776645742, ___axis_0)); }
	inline Vector3_t2243707580  get_axis_0() const { return ___axis_0; }
	inline Vector3_t2243707580 * get_address_of_axis_0() { return &___axis_0; }
	inline void set_axis_0(Vector3_t2243707580  value)
	{
		___axis_0 = value;
	}

	inline static int32_t get_offset_of_pivot_1() { return static_cast<int32_t>(offsetof(Link_t776645742, ___pivot_1)); }
	inline Vector3_t2243707580  get_pivot_1() const { return ___pivot_1; }
	inline Vector3_t2243707580 * get_address_of_pivot_1() { return &___pivot_1; }
	inline void set_pivot_1(Vector3_t2243707580  value)
	{
		___pivot_1 = value;
	}

	inline static int32_t get_offset_of_angle_2() { return static_cast<int32_t>(offsetof(Link_t776645742, ___angle_2)); }
	inline float get_angle_2() const { return ___angle_2; }
	inline float* get_address_of_angle_2() { return &___angle_2; }
	inline void set_angle_2(float value)
	{
		___angle_2 = value;
	}

	inline static int32_t get_offset_of_first_3() { return static_cast<int32_t>(offsetof(Link_t776645742, ___first_3)); }
	inline Cell_t964604196 * get_first_3() const { return ___first_3; }
	inline Cell_t964604196 ** get_address_of_first_3() { return &___first_3; }
	inline void set_first_3(Cell_t964604196 * value)
	{
		___first_3 = value;
		Il2CppCodeGenWriteBarrier((&___first_3), value);
	}

	inline static int32_t get_offset_of_second_4() { return static_cast<int32_t>(offsetof(Link_t776645742, ___second_4)); }
	inline Cell_t964604196 * get_second_4() const { return ___second_4; }
	inline Cell_t964604196 ** get_address_of_second_4() { return &___second_4; }
	inline void set_second_4(Cell_t964604196 * value)
	{
		___second_4 = value;
		Il2CppCodeGenWriteBarrier((&___second_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINK_T776645742_H
#ifndef KEYCODE_T2283395152_H
#define KEYCODE_T2283395152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2283395152 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2283395152, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2283395152_H
#ifndef ENUMERATOR_T1147558386_H
#define ENUMERATOR_T1147558386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>
struct  Enumerator_t1147558386 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1612828712 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Vector3_t2243707580  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t1147558386, ___l_0)); }
	inline List_1_t1612828712 * get_l_0() const { return ___l_0; }
	inline List_1_t1612828712 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1612828712 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1147558386, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t1147558386, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1147558386, ___current_3)); }
	inline Vector3_t2243707580  get_current_3() const { return ___current_3; }
	inline Vector3_t2243707580 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Vector3_t2243707580  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1147558386_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef CELL_T964604196_H
#define CELL_T964604196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets.scripts.Cell
struct  Cell_t964604196  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Assets.scripts.Cell::<midPoints>k__BackingField
	List_1_t1612828712 * ___U3CmidPointsU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<Assets.scripts.Cell> Assets.scripts.Cell::<blocks>k__BackingField
	List_1_t333725328 * ___U3CblocksU3Ek__BackingField_1;
	// System.Collections.Generic.List`1<Assets.scripts.Link> Assets.scripts.Cell::<links>k__BackingField
	List_1_t145766874 * ___U3ClinksU3Ek__BackingField_2;
	// System.Int32 Assets.scripts.Cell::<x>k__BackingField
	int32_t ___U3CxU3Ek__BackingField_3;
	// System.Int32 Assets.scripts.Cell::<y>k__BackingField
	int32_t ___U3CyU3Ek__BackingField_4;
	// System.Int32 Assets.scripts.Cell::<z>k__BackingField
	int32_t ___U3CzU3Ek__BackingField_5;
	// UnityEngine.Vector3 Assets.scripts.Cell::<pos>k__BackingField
	Vector3_t2243707580  ___U3CposU3Ek__BackingField_6;
	// System.Boolean Assets.scripts.Cell::<isBlock>k__BackingField
	bool ___U3CisBlockU3Ek__BackingField_7;
	// Assets.scripts.Link Assets.scripts.Cell::predLink
	Link_t776645742 * ___predLink_8;
	// System.Single Assets.scripts.Cell::g
	float ___g_9;
	// System.Single Assets.scripts.Cell::h
	float ___h_10;
	// Trigger Assets.scripts.Cell::trigger
	Trigger_t3844394560 * ___trigger_11;
	// System.Boolean Assets.scripts.Cell::isObstacle
	bool ___isObstacle_12;
	// UnityEngine.GameObject Assets.scripts.Cell::fragment
	GameObject_t1756533147 * ___fragment_13;

public:
	inline static int32_t get_offset_of_U3CmidPointsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Cell_t964604196, ___U3CmidPointsU3Ek__BackingField_0)); }
	inline List_1_t1612828712 * get_U3CmidPointsU3Ek__BackingField_0() const { return ___U3CmidPointsU3Ek__BackingField_0; }
	inline List_1_t1612828712 ** get_address_of_U3CmidPointsU3Ek__BackingField_0() { return &___U3CmidPointsU3Ek__BackingField_0; }
	inline void set_U3CmidPointsU3Ek__BackingField_0(List_1_t1612828712 * value)
	{
		___U3CmidPointsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmidPointsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CblocksU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Cell_t964604196, ___U3CblocksU3Ek__BackingField_1)); }
	inline List_1_t333725328 * get_U3CblocksU3Ek__BackingField_1() const { return ___U3CblocksU3Ek__BackingField_1; }
	inline List_1_t333725328 ** get_address_of_U3CblocksU3Ek__BackingField_1() { return &___U3CblocksU3Ek__BackingField_1; }
	inline void set_U3CblocksU3Ek__BackingField_1(List_1_t333725328 * value)
	{
		___U3CblocksU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CblocksU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ClinksU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Cell_t964604196, ___U3ClinksU3Ek__BackingField_2)); }
	inline List_1_t145766874 * get_U3ClinksU3Ek__BackingField_2() const { return ___U3ClinksU3Ek__BackingField_2; }
	inline List_1_t145766874 ** get_address_of_U3ClinksU3Ek__BackingField_2() { return &___U3ClinksU3Ek__BackingField_2; }
	inline void set_U3ClinksU3Ek__BackingField_2(List_1_t145766874 * value)
	{
		___U3ClinksU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClinksU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CxU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Cell_t964604196, ___U3CxU3Ek__BackingField_3)); }
	inline int32_t get_U3CxU3Ek__BackingField_3() const { return ___U3CxU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CxU3Ek__BackingField_3() { return &___U3CxU3Ek__BackingField_3; }
	inline void set_U3CxU3Ek__BackingField_3(int32_t value)
	{
		___U3CxU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CyU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Cell_t964604196, ___U3CyU3Ek__BackingField_4)); }
	inline int32_t get_U3CyU3Ek__BackingField_4() const { return ___U3CyU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CyU3Ek__BackingField_4() { return &___U3CyU3Ek__BackingField_4; }
	inline void set_U3CyU3Ek__BackingField_4(int32_t value)
	{
		___U3CyU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CzU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Cell_t964604196, ___U3CzU3Ek__BackingField_5)); }
	inline int32_t get_U3CzU3Ek__BackingField_5() const { return ___U3CzU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CzU3Ek__BackingField_5() { return &___U3CzU3Ek__BackingField_5; }
	inline void set_U3CzU3Ek__BackingField_5(int32_t value)
	{
		___U3CzU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CposU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Cell_t964604196, ___U3CposU3Ek__BackingField_6)); }
	inline Vector3_t2243707580  get_U3CposU3Ek__BackingField_6() const { return ___U3CposU3Ek__BackingField_6; }
	inline Vector3_t2243707580 * get_address_of_U3CposU3Ek__BackingField_6() { return &___U3CposU3Ek__BackingField_6; }
	inline void set_U3CposU3Ek__BackingField_6(Vector3_t2243707580  value)
	{
		___U3CposU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CisBlockU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Cell_t964604196, ___U3CisBlockU3Ek__BackingField_7)); }
	inline bool get_U3CisBlockU3Ek__BackingField_7() const { return ___U3CisBlockU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CisBlockU3Ek__BackingField_7() { return &___U3CisBlockU3Ek__BackingField_7; }
	inline void set_U3CisBlockU3Ek__BackingField_7(bool value)
	{
		___U3CisBlockU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_predLink_8() { return static_cast<int32_t>(offsetof(Cell_t964604196, ___predLink_8)); }
	inline Link_t776645742 * get_predLink_8() const { return ___predLink_8; }
	inline Link_t776645742 ** get_address_of_predLink_8() { return &___predLink_8; }
	inline void set_predLink_8(Link_t776645742 * value)
	{
		___predLink_8 = value;
		Il2CppCodeGenWriteBarrier((&___predLink_8), value);
	}

	inline static int32_t get_offset_of_g_9() { return static_cast<int32_t>(offsetof(Cell_t964604196, ___g_9)); }
	inline float get_g_9() const { return ___g_9; }
	inline float* get_address_of_g_9() { return &___g_9; }
	inline void set_g_9(float value)
	{
		___g_9 = value;
	}

	inline static int32_t get_offset_of_h_10() { return static_cast<int32_t>(offsetof(Cell_t964604196, ___h_10)); }
	inline float get_h_10() const { return ___h_10; }
	inline float* get_address_of_h_10() { return &___h_10; }
	inline void set_h_10(float value)
	{
		___h_10 = value;
	}

	inline static int32_t get_offset_of_trigger_11() { return static_cast<int32_t>(offsetof(Cell_t964604196, ___trigger_11)); }
	inline Trigger_t3844394560 * get_trigger_11() const { return ___trigger_11; }
	inline Trigger_t3844394560 ** get_address_of_trigger_11() { return &___trigger_11; }
	inline void set_trigger_11(Trigger_t3844394560 * value)
	{
		___trigger_11 = value;
		Il2CppCodeGenWriteBarrier((&___trigger_11), value);
	}

	inline static int32_t get_offset_of_isObstacle_12() { return static_cast<int32_t>(offsetof(Cell_t964604196, ___isObstacle_12)); }
	inline bool get_isObstacle_12() const { return ___isObstacle_12; }
	inline bool* get_address_of_isObstacle_12() { return &___isObstacle_12; }
	inline void set_isObstacle_12(bool value)
	{
		___isObstacle_12 = value;
	}

	inline static int32_t get_offset_of_fragment_13() { return static_cast<int32_t>(offsetof(Cell_t964604196, ___fragment_13)); }
	inline GameObject_t1756533147 * get_fragment_13() const { return ___fragment_13; }
	inline GameObject_t1756533147 ** get_address_of_fragment_13() { return &___fragment_13; }
	inline void set_fragment_13(GameObject_t1756533147 * value)
	{
		___fragment_13 = value;
		Il2CppCodeGenWriteBarrier((&___fragment_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CELL_T964604196_H
#ifndef RUNTIMEPLATFORM_T1869584967_H
#define RUNTIMEPLATFORM_T1869584967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_t1869584967 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RuntimePlatform_t1869584967, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPLATFORM_T1869584967_H
#ifndef RAY_T2469606224_H
#define RAY_T2469606224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t2469606224 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t2243707580  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t2243707580  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t2469606224, ___m_Origin_0)); }
	inline Vector3_t2243707580  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t2243707580 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t2243707580  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t2469606224, ___m_Direction_1)); }
	inline Vector3_t2243707580  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t2243707580 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t2243707580  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T2469606224_H
#ifndef TOUCH_T407273883_H
#define TOUCH_T407273883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t407273883 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t2243707579  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t2243707579  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t2243707579  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_Position_1)); }
	inline Vector2_t2243707579  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t2243707579 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t2243707579  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_RawPosition_2)); }
	inline Vector2_t2243707579  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t2243707579 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t2243707579  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_PositionDelta_3)); }
	inline Vector2_t2243707579  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t2243707579 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t2243707579  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t407273883, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T407273883_H
#ifndef MATERIAL_T193706927_H
#define MATERIAL_T193706927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t193706927  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T193706927_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef NAVIGATION_T1571958496_H
#define NAVIGATION_T1571958496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t1571958496 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t1490392188 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t1490392188 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t1490392188 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t1490392188 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnUp_1)); }
	inline Selectable_t1490392188 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t1490392188 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnDown_2)); }
	inline Selectable_t1490392188 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t1490392188 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnLeft_3)); }
	inline Selectable_t1490392188 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t1490392188 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnRight_4)); }
	inline Selectable_t1490392188 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t1490392188 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1571958496_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t1490392188 * ___m_SelectOnUp_1;
	Selectable_t1490392188 * ___m_SelectOnDown_2;
	Selectable_t1490392188 * ___m_SelectOnLeft_3;
	Selectable_t1490392188 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1571958496_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t1490392188 * ___m_SelectOnUp_1;
	Selectable_t1490392188 * ___m_SelectOnDown_2;
	Selectable_t1490392188 * ___m_SelectOnLeft_3;
	Selectable_t1490392188 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T1571958496_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef SPRITE_T309593783_H
#define SPRITE_T309593783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_t309593783  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T309593783_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t2330101084  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t2330101084  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t2330101084 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t2330101084  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t1664964607* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t3405857066 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t3405857066 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t3405857066 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t1664964607* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t1664964607** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t1664964607* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t3405857066 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t3405857066 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t3405857066 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t3405857066 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t3405857066 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t3405857066 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t3405857066 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t3405857066 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t3405857066 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef GAMEOBJECT_T1756533147_H
#define GAMEOBJECT_T1756533147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1756533147  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1756533147_H
#ifndef TRANSFORM_T3275118058_H
#define TRANSFORM_T3275118058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3275118058  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3275118058_H
#ifndef UNITYACTION_T4025899511_H
#define UNITYACTION_T4025899511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction
struct  UnityAction_t4025899511  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_T4025899511_H
#ifndef RENDERER_T257310565_H
#define RENDERER_T257310565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t257310565  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T257310565_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef CAMERA_T189460977_H
#define CAMERA_T189460977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t189460977  : public Behaviour_t955675639
{
public:

public:
};

struct Camera_t189460977_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t834278767 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t834278767 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t834278767 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t834278767 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t834278767 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t834278767 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t834278767 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t834278767 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t834278767 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t834278767 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t834278767 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t834278767 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T189460977_H
#ifndef ANIMATOR_T69676727_H
#define ANIMATOR_T69676727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animator
struct  Animator_t69676727  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATOR_T69676727_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef LEVEL_T866853782_H
#define LEVEL_T866853782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Level
struct  Level_t866853782  : public MonoBehaviour_t1158329972
{
public:
	// Assets.scripts.Cell[0...,0...,0...] Level::cells
	CellU5BU2CU2CU5D_t1218597327* ___cells_2;

public:
	inline static int32_t get_offset_of_cells_2() { return static_cast<int32_t>(offsetof(Level_t866853782, ___cells_2)); }
	inline CellU5BU2CU2CU5D_t1218597327* get_cells_2() const { return ___cells_2; }
	inline CellU5BU2CU2CU5D_t1218597327** get_address_of_cells_2() { return &___cells_2; }
	inline void set_cells_2(CellU5BU2CU2CU5D_t1218597327* value)
	{
		___cells_2 = value;
		Il2CppCodeGenWriteBarrier((&___cells_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVEL_T866853782_H
#ifndef GAME_T1600141214_H
#define GAME_T1600141214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Game
struct  Game_t1600141214  : public MonoBehaviour_t1158329972
{
public:
	// Level Game::level
	Level_t866853782 * ___level_2;
	// Pathfinder Game::pathfinder
	Pathfinder_t1592129589 * ___pathfinder_3;
	// Character Game::player
	Character_t3726027591 * ___player_4;
	// Menu Game::menu
	Menu_t4261767481 * ___menu_5;
	// InputController Game::inputController
	InputController_t3951031284 * ___inputController_6;
	// System.Single Game::camHoriz
	float ___camHoriz_10;
	// System.Single Game::camVert
	float ___camVert_11;
	// UnityEngine.Quaternion Game::camRot
	Quaternion_t4030073918  ___camRot_12;
	// System.Int32 Game::numFragments
	int32_t ___numFragments_13;
	// System.Boolean Game::android
	bool ___android_14;

public:
	inline static int32_t get_offset_of_level_2() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___level_2)); }
	inline Level_t866853782 * get_level_2() const { return ___level_2; }
	inline Level_t866853782 ** get_address_of_level_2() { return &___level_2; }
	inline void set_level_2(Level_t866853782 * value)
	{
		___level_2 = value;
		Il2CppCodeGenWriteBarrier((&___level_2), value);
	}

	inline static int32_t get_offset_of_pathfinder_3() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___pathfinder_3)); }
	inline Pathfinder_t1592129589 * get_pathfinder_3() const { return ___pathfinder_3; }
	inline Pathfinder_t1592129589 ** get_address_of_pathfinder_3() { return &___pathfinder_3; }
	inline void set_pathfinder_3(Pathfinder_t1592129589 * value)
	{
		___pathfinder_3 = value;
		Il2CppCodeGenWriteBarrier((&___pathfinder_3), value);
	}

	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___player_4)); }
	inline Character_t3726027591 * get_player_4() const { return ___player_4; }
	inline Character_t3726027591 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(Character_t3726027591 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_menu_5() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___menu_5)); }
	inline Menu_t4261767481 * get_menu_5() const { return ___menu_5; }
	inline Menu_t4261767481 ** get_address_of_menu_5() { return &___menu_5; }
	inline void set_menu_5(Menu_t4261767481 * value)
	{
		___menu_5 = value;
		Il2CppCodeGenWriteBarrier((&___menu_5), value);
	}

	inline static int32_t get_offset_of_inputController_6() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___inputController_6)); }
	inline InputController_t3951031284 * get_inputController_6() const { return ___inputController_6; }
	inline InputController_t3951031284 ** get_address_of_inputController_6() { return &___inputController_6; }
	inline void set_inputController_6(InputController_t3951031284 * value)
	{
		___inputController_6 = value;
		Il2CppCodeGenWriteBarrier((&___inputController_6), value);
	}

	inline static int32_t get_offset_of_camHoriz_10() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___camHoriz_10)); }
	inline float get_camHoriz_10() const { return ___camHoriz_10; }
	inline float* get_address_of_camHoriz_10() { return &___camHoriz_10; }
	inline void set_camHoriz_10(float value)
	{
		___camHoriz_10 = value;
	}

	inline static int32_t get_offset_of_camVert_11() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___camVert_11)); }
	inline float get_camVert_11() const { return ___camVert_11; }
	inline float* get_address_of_camVert_11() { return &___camVert_11; }
	inline void set_camVert_11(float value)
	{
		___camVert_11 = value;
	}

	inline static int32_t get_offset_of_camRot_12() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___camRot_12)); }
	inline Quaternion_t4030073918  get_camRot_12() const { return ___camRot_12; }
	inline Quaternion_t4030073918 * get_address_of_camRot_12() { return &___camRot_12; }
	inline void set_camRot_12(Quaternion_t4030073918  value)
	{
		___camRot_12 = value;
	}

	inline static int32_t get_offset_of_numFragments_13() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___numFragments_13)); }
	inline int32_t get_numFragments_13() const { return ___numFragments_13; }
	inline int32_t* get_address_of_numFragments_13() { return &___numFragments_13; }
	inline void set_numFragments_13(int32_t value)
	{
		___numFragments_13 = value;
	}

	inline static int32_t get_offset_of_android_14() { return static_cast<int32_t>(offsetof(Game_t1600141214, ___android_14)); }
	inline bool get_android_14() const { return ___android_14; }
	inline bool* get_address_of_android_14() { return &___android_14; }
	inline void set_android_14(bool value)
	{
		___android_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAME_T1600141214_H
#ifndef TRIGGER_T3844394560_H
#define TRIGGER_T3844394560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Trigger
struct  Trigger_t3844394560  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T3844394560_H
#ifndef CHARACTER_T3726027591_H
#define CHARACTER_T3726027591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Character
struct  Character_t3726027591  : public MonoBehaviour_t1158329972
{
public:
	// Assets.scripts.Cell Character::currentCell
	Cell_t964604196 * ___currentCell_2;
	// System.Collections.Generic.List`1<Assets.scripts.Link> Character::path
	List_1_t145766874 * ___path_3;
	// Level Character::level
	Level_t866853782 * ___level_4;
	// Game Character::game
	Game_t1600141214 * ___game_5;
	// Assets.scripts.Link Character::currentLink
	Link_t776645742 * ___currentLink_6;
	// System.Single Character::rotation
	float ___rotation_7;
	// UnityEngine.Vector3 Character::startPos
	Vector3_t2243707580  ___startPos_9;
	// UnityEngine.Quaternion Character::initialRotation
	Quaternion_t4030073918  ___initialRotation_10;
	// System.Boolean Character::stopped
	bool ___stopped_11;

public:
	inline static int32_t get_offset_of_currentCell_2() { return static_cast<int32_t>(offsetof(Character_t3726027591, ___currentCell_2)); }
	inline Cell_t964604196 * get_currentCell_2() const { return ___currentCell_2; }
	inline Cell_t964604196 ** get_address_of_currentCell_2() { return &___currentCell_2; }
	inline void set_currentCell_2(Cell_t964604196 * value)
	{
		___currentCell_2 = value;
		Il2CppCodeGenWriteBarrier((&___currentCell_2), value);
	}

	inline static int32_t get_offset_of_path_3() { return static_cast<int32_t>(offsetof(Character_t3726027591, ___path_3)); }
	inline List_1_t145766874 * get_path_3() const { return ___path_3; }
	inline List_1_t145766874 ** get_address_of_path_3() { return &___path_3; }
	inline void set_path_3(List_1_t145766874 * value)
	{
		___path_3 = value;
		Il2CppCodeGenWriteBarrier((&___path_3), value);
	}

	inline static int32_t get_offset_of_level_4() { return static_cast<int32_t>(offsetof(Character_t3726027591, ___level_4)); }
	inline Level_t866853782 * get_level_4() const { return ___level_4; }
	inline Level_t866853782 ** get_address_of_level_4() { return &___level_4; }
	inline void set_level_4(Level_t866853782 * value)
	{
		___level_4 = value;
		Il2CppCodeGenWriteBarrier((&___level_4), value);
	}

	inline static int32_t get_offset_of_game_5() { return static_cast<int32_t>(offsetof(Character_t3726027591, ___game_5)); }
	inline Game_t1600141214 * get_game_5() const { return ___game_5; }
	inline Game_t1600141214 ** get_address_of_game_5() { return &___game_5; }
	inline void set_game_5(Game_t1600141214 * value)
	{
		___game_5 = value;
		Il2CppCodeGenWriteBarrier((&___game_5), value);
	}

	inline static int32_t get_offset_of_currentLink_6() { return static_cast<int32_t>(offsetof(Character_t3726027591, ___currentLink_6)); }
	inline Link_t776645742 * get_currentLink_6() const { return ___currentLink_6; }
	inline Link_t776645742 ** get_address_of_currentLink_6() { return &___currentLink_6; }
	inline void set_currentLink_6(Link_t776645742 * value)
	{
		___currentLink_6 = value;
		Il2CppCodeGenWriteBarrier((&___currentLink_6), value);
	}

	inline static int32_t get_offset_of_rotation_7() { return static_cast<int32_t>(offsetof(Character_t3726027591, ___rotation_7)); }
	inline float get_rotation_7() const { return ___rotation_7; }
	inline float* get_address_of_rotation_7() { return &___rotation_7; }
	inline void set_rotation_7(float value)
	{
		___rotation_7 = value;
	}

	inline static int32_t get_offset_of_startPos_9() { return static_cast<int32_t>(offsetof(Character_t3726027591, ___startPos_9)); }
	inline Vector3_t2243707580  get_startPos_9() const { return ___startPos_9; }
	inline Vector3_t2243707580 * get_address_of_startPos_9() { return &___startPos_9; }
	inline void set_startPos_9(Vector3_t2243707580  value)
	{
		___startPos_9 = value;
	}

	inline static int32_t get_offset_of_initialRotation_10() { return static_cast<int32_t>(offsetof(Character_t3726027591, ___initialRotation_10)); }
	inline Quaternion_t4030073918  get_initialRotation_10() const { return ___initialRotation_10; }
	inline Quaternion_t4030073918 * get_address_of_initialRotation_10() { return &___initialRotation_10; }
	inline void set_initialRotation_10(Quaternion_t4030073918  value)
	{
		___initialRotation_10 = value;
	}

	inline static int32_t get_offset_of_stopped_11() { return static_cast<int32_t>(offsetof(Character_t3726027591, ___stopped_11)); }
	inline bool get_stopped_11() const { return ___stopped_11; }
	inline bool* get_address_of_stopped_11() { return &___stopped_11; }
	inline void set_stopped_11(bool value)
	{
		___stopped_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTER_T3726027591_H
#ifndef MENU_T4261767481_H
#define MENU_T4261767481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Menu
struct  Menu_t4261767481  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text Menu::textWin
	Text_t356221433 * ___textWin_2;
	// UnityEngine.UI.Text Menu::textFragments
	Text_t356221433 * ___textFragments_3;
	// UnityEngine.UI.Text Menu::textDialogBox
	Text_t356221433 * ___textDialogBox_4;
	// UnityEngine.UI.Button Menu::buttonSettings
	Button_t2872111280 * ___buttonSettings_5;
	// UnityEngine.UI.Button Menu::buttonDone
	Button_t2872111280 * ___buttonDone_6;
	// UnityEngine.UI.Button Menu::buttonVolume
	Button_t2872111280 * ___buttonVolume_7;
	// UnityEngine.GameObject Menu::dialogBox
	GameObject_t1756533147 * ___dialogBox_8;
	// UnityEngine.GameObject Menu::panelSettings
	GameObject_t1756533147 * ___panelSettings_9;
	// Game Menu::game
	Game_t1600141214 * ___game_10;
	// System.Boolean Menu::volumeOn
	bool ___volumeOn_11;
	// System.Single Menu::volume
	float ___volume_12;

public:
	inline static int32_t get_offset_of_textWin_2() { return static_cast<int32_t>(offsetof(Menu_t4261767481, ___textWin_2)); }
	inline Text_t356221433 * get_textWin_2() const { return ___textWin_2; }
	inline Text_t356221433 ** get_address_of_textWin_2() { return &___textWin_2; }
	inline void set_textWin_2(Text_t356221433 * value)
	{
		___textWin_2 = value;
		Il2CppCodeGenWriteBarrier((&___textWin_2), value);
	}

	inline static int32_t get_offset_of_textFragments_3() { return static_cast<int32_t>(offsetof(Menu_t4261767481, ___textFragments_3)); }
	inline Text_t356221433 * get_textFragments_3() const { return ___textFragments_3; }
	inline Text_t356221433 ** get_address_of_textFragments_3() { return &___textFragments_3; }
	inline void set_textFragments_3(Text_t356221433 * value)
	{
		___textFragments_3 = value;
		Il2CppCodeGenWriteBarrier((&___textFragments_3), value);
	}

	inline static int32_t get_offset_of_textDialogBox_4() { return static_cast<int32_t>(offsetof(Menu_t4261767481, ___textDialogBox_4)); }
	inline Text_t356221433 * get_textDialogBox_4() const { return ___textDialogBox_4; }
	inline Text_t356221433 ** get_address_of_textDialogBox_4() { return &___textDialogBox_4; }
	inline void set_textDialogBox_4(Text_t356221433 * value)
	{
		___textDialogBox_4 = value;
		Il2CppCodeGenWriteBarrier((&___textDialogBox_4), value);
	}

	inline static int32_t get_offset_of_buttonSettings_5() { return static_cast<int32_t>(offsetof(Menu_t4261767481, ___buttonSettings_5)); }
	inline Button_t2872111280 * get_buttonSettings_5() const { return ___buttonSettings_5; }
	inline Button_t2872111280 ** get_address_of_buttonSettings_5() { return &___buttonSettings_5; }
	inline void set_buttonSettings_5(Button_t2872111280 * value)
	{
		___buttonSettings_5 = value;
		Il2CppCodeGenWriteBarrier((&___buttonSettings_5), value);
	}

	inline static int32_t get_offset_of_buttonDone_6() { return static_cast<int32_t>(offsetof(Menu_t4261767481, ___buttonDone_6)); }
	inline Button_t2872111280 * get_buttonDone_6() const { return ___buttonDone_6; }
	inline Button_t2872111280 ** get_address_of_buttonDone_6() { return &___buttonDone_6; }
	inline void set_buttonDone_6(Button_t2872111280 * value)
	{
		___buttonDone_6 = value;
		Il2CppCodeGenWriteBarrier((&___buttonDone_6), value);
	}

	inline static int32_t get_offset_of_buttonVolume_7() { return static_cast<int32_t>(offsetof(Menu_t4261767481, ___buttonVolume_7)); }
	inline Button_t2872111280 * get_buttonVolume_7() const { return ___buttonVolume_7; }
	inline Button_t2872111280 ** get_address_of_buttonVolume_7() { return &___buttonVolume_7; }
	inline void set_buttonVolume_7(Button_t2872111280 * value)
	{
		___buttonVolume_7 = value;
		Il2CppCodeGenWriteBarrier((&___buttonVolume_7), value);
	}

	inline static int32_t get_offset_of_dialogBox_8() { return static_cast<int32_t>(offsetof(Menu_t4261767481, ___dialogBox_8)); }
	inline GameObject_t1756533147 * get_dialogBox_8() const { return ___dialogBox_8; }
	inline GameObject_t1756533147 ** get_address_of_dialogBox_8() { return &___dialogBox_8; }
	inline void set_dialogBox_8(GameObject_t1756533147 * value)
	{
		___dialogBox_8 = value;
		Il2CppCodeGenWriteBarrier((&___dialogBox_8), value);
	}

	inline static int32_t get_offset_of_panelSettings_9() { return static_cast<int32_t>(offsetof(Menu_t4261767481, ___panelSettings_9)); }
	inline GameObject_t1756533147 * get_panelSettings_9() const { return ___panelSettings_9; }
	inline GameObject_t1756533147 ** get_address_of_panelSettings_9() { return &___panelSettings_9; }
	inline void set_panelSettings_9(GameObject_t1756533147 * value)
	{
		___panelSettings_9 = value;
		Il2CppCodeGenWriteBarrier((&___panelSettings_9), value);
	}

	inline static int32_t get_offset_of_game_10() { return static_cast<int32_t>(offsetof(Menu_t4261767481, ___game_10)); }
	inline Game_t1600141214 * get_game_10() const { return ___game_10; }
	inline Game_t1600141214 ** get_address_of_game_10() { return &___game_10; }
	inline void set_game_10(Game_t1600141214 * value)
	{
		___game_10 = value;
		Il2CppCodeGenWriteBarrier((&___game_10), value);
	}

	inline static int32_t get_offset_of_volumeOn_11() { return static_cast<int32_t>(offsetof(Menu_t4261767481, ___volumeOn_11)); }
	inline bool get_volumeOn_11() const { return ___volumeOn_11; }
	inline bool* get_address_of_volumeOn_11() { return &___volumeOn_11; }
	inline void set_volumeOn_11(bool value)
	{
		___volumeOn_11 = value;
	}

	inline static int32_t get_offset_of_volume_12() { return static_cast<int32_t>(offsetof(Menu_t4261767481, ___volume_12)); }
	inline float get_volume_12() const { return ___volume_12; }
	inline float* get_address_of_volume_12() { return &___volume_12; }
	inline void set_volume_12(float value)
	{
		___volume_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENU_T4261767481_H
#ifndef GRIDSNAP_T338261636_H
#define GRIDSNAP_T338261636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GridSnap
struct  GridSnap_t338261636  : public MonoBehaviour_t1158329972
{
public:
	// System.Single GridSnap::snapX
	float ___snapX_2;
	// System.Single GridSnap::snapY
	float ___snapY_3;
	// System.Single GridSnap::snapZ
	float ___snapZ_4;

public:
	inline static int32_t get_offset_of_snapX_2() { return static_cast<int32_t>(offsetof(GridSnap_t338261636, ___snapX_2)); }
	inline float get_snapX_2() const { return ___snapX_2; }
	inline float* get_address_of_snapX_2() { return &___snapX_2; }
	inline void set_snapX_2(float value)
	{
		___snapX_2 = value;
	}

	inline static int32_t get_offset_of_snapY_3() { return static_cast<int32_t>(offsetof(GridSnap_t338261636, ___snapY_3)); }
	inline float get_snapY_3() const { return ___snapY_3; }
	inline float* get_address_of_snapY_3() { return &___snapY_3; }
	inline void set_snapY_3(float value)
	{
		___snapY_3 = value;
	}

	inline static int32_t get_offset_of_snapZ_4() { return static_cast<int32_t>(offsetof(GridSnap_t338261636, ___snapZ_4)); }
	inline float get_snapZ_4() const { return ___snapZ_4; }
	inline float* get_address_of_snapZ_4() { return &___snapZ_4; }
	inline void set_snapZ_4(float value)
	{
		___snapZ_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRIDSNAP_T338261636_H
#ifndef PATHFINDER_T1592129589_H
#define PATHFINDER_T1592129589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinder
struct  Pathfinder_t1592129589  : public MonoBehaviour_t1158329972
{
public:
	// Level Pathfinder::level
	Level_t866853782 * ___level_2;

public:
	inline static int32_t get_offset_of_level_2() { return static_cast<int32_t>(offsetof(Pathfinder_t1592129589, ___level_2)); }
	inline Level_t866853782 * get_level_2() const { return ___level_2; }
	inline Level_t866853782 ** get_address_of_level_2() { return &___level_2; }
	inline void set_level_2(Level_t866853782 * value)
	{
		___level_2 = value;
		Il2CppCodeGenWriteBarrier((&___level_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHFINDER_T1592129589_H
#ifndef TOGGLEBUTTON_T3926247120_H
#define TOGGLEBUTTON_T3926247120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ToggleButton
struct  ToggleButton_t3926247120  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Sprite ToggleButton::imageOn
	Sprite_t309593783 * ___imageOn_2;
	// UnityEngine.Sprite ToggleButton::imageOff
	Sprite_t309593783 * ___imageOff_3;

public:
	inline static int32_t get_offset_of_imageOn_2() { return static_cast<int32_t>(offsetof(ToggleButton_t3926247120, ___imageOn_2)); }
	inline Sprite_t309593783 * get_imageOn_2() const { return ___imageOn_2; }
	inline Sprite_t309593783 ** get_address_of_imageOn_2() { return &___imageOn_2; }
	inline void set_imageOn_2(Sprite_t309593783 * value)
	{
		___imageOn_2 = value;
		Il2CppCodeGenWriteBarrier((&___imageOn_2), value);
	}

	inline static int32_t get_offset_of_imageOff_3() { return static_cast<int32_t>(offsetof(ToggleButton_t3926247120, ___imageOff_3)); }
	inline Sprite_t309593783 * get_imageOff_3() const { return ___imageOff_3; }
	inline Sprite_t309593783 ** get_address_of_imageOff_3() { return &___imageOff_3; }
	inline void set_imageOff_3(Sprite_t309593783 * value)
	{
		___imageOff_3 = value;
		Il2CppCodeGenWriteBarrier((&___imageOff_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEBUTTON_T3926247120_H
#ifndef INPUTCONTROLLER_T3951031284_H
#define INPUTCONTROLLER_T3951031284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InputController
struct  InputController_t3951031284  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 InputController::touchID
	int32_t ___touchID_2;
	// UnityEngine.Vector2 InputController::lastPos
	Vector2_t2243707579  ___lastPos_4;
	// System.Boolean InputController::dragging
	bool ___dragging_5;
	// System.Boolean InputController::<dragged>k__BackingField
	bool ___U3CdraggedU3Ek__BackingField_6;
	// System.Boolean InputController::<tapped>k__BackingField
	bool ___U3CtappedU3Ek__BackingField_7;
	// UnityEngine.Vector2 InputController::<dragVector>k__BackingField
	Vector2_t2243707579  ___U3CdragVectorU3Ek__BackingField_8;
	// UnityEngine.Vector2 InputController::<tapPosition>k__BackingField
	Vector2_t2243707579  ___U3CtapPositionU3Ek__BackingField_9;
	// System.Boolean InputController::<tapOverUI>k__BackingField
	bool ___U3CtapOverUIU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_touchID_2() { return static_cast<int32_t>(offsetof(InputController_t3951031284, ___touchID_2)); }
	inline int32_t get_touchID_2() const { return ___touchID_2; }
	inline int32_t* get_address_of_touchID_2() { return &___touchID_2; }
	inline void set_touchID_2(int32_t value)
	{
		___touchID_2 = value;
	}

	inline static int32_t get_offset_of_lastPos_4() { return static_cast<int32_t>(offsetof(InputController_t3951031284, ___lastPos_4)); }
	inline Vector2_t2243707579  get_lastPos_4() const { return ___lastPos_4; }
	inline Vector2_t2243707579 * get_address_of_lastPos_4() { return &___lastPos_4; }
	inline void set_lastPos_4(Vector2_t2243707579  value)
	{
		___lastPos_4 = value;
	}

	inline static int32_t get_offset_of_dragging_5() { return static_cast<int32_t>(offsetof(InputController_t3951031284, ___dragging_5)); }
	inline bool get_dragging_5() const { return ___dragging_5; }
	inline bool* get_address_of_dragging_5() { return &___dragging_5; }
	inline void set_dragging_5(bool value)
	{
		___dragging_5 = value;
	}

	inline static int32_t get_offset_of_U3CdraggedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(InputController_t3951031284, ___U3CdraggedU3Ek__BackingField_6)); }
	inline bool get_U3CdraggedU3Ek__BackingField_6() const { return ___U3CdraggedU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CdraggedU3Ek__BackingField_6() { return &___U3CdraggedU3Ek__BackingField_6; }
	inline void set_U3CdraggedU3Ek__BackingField_6(bool value)
	{
		___U3CdraggedU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CtappedU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(InputController_t3951031284, ___U3CtappedU3Ek__BackingField_7)); }
	inline bool get_U3CtappedU3Ek__BackingField_7() const { return ___U3CtappedU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CtappedU3Ek__BackingField_7() { return &___U3CtappedU3Ek__BackingField_7; }
	inline void set_U3CtappedU3Ek__BackingField_7(bool value)
	{
		___U3CtappedU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CdragVectorU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(InputController_t3951031284, ___U3CdragVectorU3Ek__BackingField_8)); }
	inline Vector2_t2243707579  get_U3CdragVectorU3Ek__BackingField_8() const { return ___U3CdragVectorU3Ek__BackingField_8; }
	inline Vector2_t2243707579 * get_address_of_U3CdragVectorU3Ek__BackingField_8() { return &___U3CdragVectorU3Ek__BackingField_8; }
	inline void set_U3CdragVectorU3Ek__BackingField_8(Vector2_t2243707579  value)
	{
		___U3CdragVectorU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CtapPositionU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(InputController_t3951031284, ___U3CtapPositionU3Ek__BackingField_9)); }
	inline Vector2_t2243707579  get_U3CtapPositionU3Ek__BackingField_9() const { return ___U3CtapPositionU3Ek__BackingField_9; }
	inline Vector2_t2243707579 * get_address_of_U3CtapPositionU3Ek__BackingField_9() { return &___U3CtapPositionU3Ek__BackingField_9; }
	inline void set_U3CtapPositionU3Ek__BackingField_9(Vector2_t2243707579  value)
	{
		___U3CtapPositionU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CtapOverUIU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(InputController_t3951031284, ___U3CtapOverUIU3Ek__BackingField_10)); }
	inline bool get_U3CtapOverUIU3Ek__BackingField_10() const { return ___U3CtapOverUIU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CtapOverUIU3Ek__BackingField_10() { return &___U3CtapOverUIU3Ek__BackingField_10; }
	inline void set_U3CtapOverUIU3Ek__BackingField_10(bool value)
	{
		___U3CtapOverUIU3Ek__BackingField_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTCONTROLLER_T3951031284_H
#ifndef ARROWTRIGGER_T1089668331_H
#define ARROWTRIGGER_T1089668331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArrowTrigger
struct  ArrowTrigger_t1089668331  : public Trigger_t3844394560
{
public:
	// Character ArrowTrigger::target
	Character_t3726027591 * ___target_2;
	// UnityEngine.Vector3 ArrowTrigger::direction
	Vector3_t2243707580  ___direction_3;
	// Level ArrowTrigger::level
	Level_t866853782 * ___level_4;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(ArrowTrigger_t1089668331, ___target_2)); }
	inline Character_t3726027591 * get_target_2() const { return ___target_2; }
	inline Character_t3726027591 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Character_t3726027591 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_direction_3() { return static_cast<int32_t>(offsetof(ArrowTrigger_t1089668331, ___direction_3)); }
	inline Vector3_t2243707580  get_direction_3() const { return ___direction_3; }
	inline Vector3_t2243707580 * get_address_of_direction_3() { return &___direction_3; }
	inline void set_direction_3(Vector3_t2243707580  value)
	{
		___direction_3 = value;
	}

	inline static int32_t get_offset_of_level_4() { return static_cast<int32_t>(offsetof(ArrowTrigger_t1089668331, ___level_4)); }
	inline Level_t866853782 * get_level_4() const { return ___level_4; }
	inline Level_t866853782 ** get_address_of_level_4() { return &___level_4; }
	inline void set_level_4(Level_t866853782 * value)
	{
		___level_4 = value;
		Il2CppCodeGenWriteBarrier((&___level_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARROWTRIGGER_T1089668331_H
#ifndef REVEALTRIGGER_T3929941135_H
#define REVEALTRIGGER_T3929941135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RevealTrigger
struct  RevealTrigger_t3929941135  : public Trigger_t3844394560
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> RevealTrigger::children
	List_1_t1125654279 * ___children_2;
	// UnityEngine.Material RevealTrigger::hiddenMaterial
	Material_t193706927 * ___hiddenMaterial_3;
	// UnityEngine.Material RevealTrigger::revealMaterial
	Material_t193706927 * ___revealMaterial_4;

public:
	inline static int32_t get_offset_of_children_2() { return static_cast<int32_t>(offsetof(RevealTrigger_t3929941135, ___children_2)); }
	inline List_1_t1125654279 * get_children_2() const { return ___children_2; }
	inline List_1_t1125654279 ** get_address_of_children_2() { return &___children_2; }
	inline void set_children_2(List_1_t1125654279 * value)
	{
		___children_2 = value;
		Il2CppCodeGenWriteBarrier((&___children_2), value);
	}

	inline static int32_t get_offset_of_hiddenMaterial_3() { return static_cast<int32_t>(offsetof(RevealTrigger_t3929941135, ___hiddenMaterial_3)); }
	inline Material_t193706927 * get_hiddenMaterial_3() const { return ___hiddenMaterial_3; }
	inline Material_t193706927 ** get_address_of_hiddenMaterial_3() { return &___hiddenMaterial_3; }
	inline void set_hiddenMaterial_3(Material_t193706927 * value)
	{
		___hiddenMaterial_3 = value;
		Il2CppCodeGenWriteBarrier((&___hiddenMaterial_3), value);
	}

	inline static int32_t get_offset_of_revealMaterial_4() { return static_cast<int32_t>(offsetof(RevealTrigger_t3929941135, ___revealMaterial_4)); }
	inline Material_t193706927 * get_revealMaterial_4() const { return ___revealMaterial_4; }
	inline Material_t193706927 ** get_address_of_revealMaterial_4() { return &___revealMaterial_4; }
	inline void set_revealMaterial_4(Material_t193706927 * value)
	{
		___revealMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___revealMaterial_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REVEALTRIGGER_T3929941135_H
#ifndef PLATFORMTRIGGER_T3477208093_H
#define PLATFORMTRIGGER_T3477208093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlatformTrigger
struct  PlatformTrigger_t3477208093  : public Trigger_t3844394560
{
public:
	// UnityEngine.GameObject PlatformTrigger::platform
	GameObject_t1756533147 * ___platform_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> PlatformTrigger::moves
	List_1_t1612828712 * ___moves_3;
	// UnityEngine.Vector3 PlatformTrigger::characterStart
	Vector3_t2243707580  ___characterStart_4;
	// UnityEngine.Vector3 PlatformTrigger::platformStart
	Vector3_t2243707580  ___platformStart_5;
	// System.Boolean PlatformTrigger::moving
	bool ___moving_6;
	// Character PlatformTrigger::character
	Character_t3726027591 * ___character_7;
	// Level PlatformTrigger::level
	Level_t866853782 * ___level_8;
	// System.Int32 PlatformTrigger::direction
	int32_t ___direction_9;
	// System.Single PlatformTrigger::distance
	float ___distance_11;
	// System.Single PlatformTrigger::totalDistance
	float ___totalDistance_12;

public:
	inline static int32_t get_offset_of_platform_2() { return static_cast<int32_t>(offsetof(PlatformTrigger_t3477208093, ___platform_2)); }
	inline GameObject_t1756533147 * get_platform_2() const { return ___platform_2; }
	inline GameObject_t1756533147 ** get_address_of_platform_2() { return &___platform_2; }
	inline void set_platform_2(GameObject_t1756533147 * value)
	{
		___platform_2 = value;
		Il2CppCodeGenWriteBarrier((&___platform_2), value);
	}

	inline static int32_t get_offset_of_moves_3() { return static_cast<int32_t>(offsetof(PlatformTrigger_t3477208093, ___moves_3)); }
	inline List_1_t1612828712 * get_moves_3() const { return ___moves_3; }
	inline List_1_t1612828712 ** get_address_of_moves_3() { return &___moves_3; }
	inline void set_moves_3(List_1_t1612828712 * value)
	{
		___moves_3 = value;
		Il2CppCodeGenWriteBarrier((&___moves_3), value);
	}

	inline static int32_t get_offset_of_characterStart_4() { return static_cast<int32_t>(offsetof(PlatformTrigger_t3477208093, ___characterStart_4)); }
	inline Vector3_t2243707580  get_characterStart_4() const { return ___characterStart_4; }
	inline Vector3_t2243707580 * get_address_of_characterStart_4() { return &___characterStart_4; }
	inline void set_characterStart_4(Vector3_t2243707580  value)
	{
		___characterStart_4 = value;
	}

	inline static int32_t get_offset_of_platformStart_5() { return static_cast<int32_t>(offsetof(PlatformTrigger_t3477208093, ___platformStart_5)); }
	inline Vector3_t2243707580  get_platformStart_5() const { return ___platformStart_5; }
	inline Vector3_t2243707580 * get_address_of_platformStart_5() { return &___platformStart_5; }
	inline void set_platformStart_5(Vector3_t2243707580  value)
	{
		___platformStart_5 = value;
	}

	inline static int32_t get_offset_of_moving_6() { return static_cast<int32_t>(offsetof(PlatformTrigger_t3477208093, ___moving_6)); }
	inline bool get_moving_6() const { return ___moving_6; }
	inline bool* get_address_of_moving_6() { return &___moving_6; }
	inline void set_moving_6(bool value)
	{
		___moving_6 = value;
	}

	inline static int32_t get_offset_of_character_7() { return static_cast<int32_t>(offsetof(PlatformTrigger_t3477208093, ___character_7)); }
	inline Character_t3726027591 * get_character_7() const { return ___character_7; }
	inline Character_t3726027591 ** get_address_of_character_7() { return &___character_7; }
	inline void set_character_7(Character_t3726027591 * value)
	{
		___character_7 = value;
		Il2CppCodeGenWriteBarrier((&___character_7), value);
	}

	inline static int32_t get_offset_of_level_8() { return static_cast<int32_t>(offsetof(PlatformTrigger_t3477208093, ___level_8)); }
	inline Level_t866853782 * get_level_8() const { return ___level_8; }
	inline Level_t866853782 ** get_address_of_level_8() { return &___level_8; }
	inline void set_level_8(Level_t866853782 * value)
	{
		___level_8 = value;
		Il2CppCodeGenWriteBarrier((&___level_8), value);
	}

	inline static int32_t get_offset_of_direction_9() { return static_cast<int32_t>(offsetof(PlatformTrigger_t3477208093, ___direction_9)); }
	inline int32_t get_direction_9() const { return ___direction_9; }
	inline int32_t* get_address_of_direction_9() { return &___direction_9; }
	inline void set_direction_9(int32_t value)
	{
		___direction_9 = value;
	}

	inline static int32_t get_offset_of_distance_11() { return static_cast<int32_t>(offsetof(PlatformTrigger_t3477208093, ___distance_11)); }
	inline float get_distance_11() const { return ___distance_11; }
	inline float* get_address_of_distance_11() { return &___distance_11; }
	inline void set_distance_11(float value)
	{
		___distance_11 = value;
	}

	inline static int32_t get_offset_of_totalDistance_12() { return static_cast<int32_t>(offsetof(PlatformTrigger_t3477208093, ___totalDistance_12)); }
	inline float get_totalDistance_12() const { return ___totalDistance_12; }
	inline float* get_address_of_totalDistance_12() { return &___totalDistance_12; }
	inline void set_totalDistance_12(float value)
	{
		___totalDistance_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMTRIGGER_T3477208093_H
#ifndef EVENTSYSTEM_T3466835263_H
#define EVENTSYSTEM_T3466835263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventSystem
struct  EventSystem_t3466835263  : public UIBehaviour_t3960014691
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule> UnityEngine.EventSystems.EventSystem::m_SystemInputModules
	List_1_t664902677 * ___m_SystemInputModules_2;
	// UnityEngine.EventSystems.BaseInputModule UnityEngine.EventSystems.EventSystem::m_CurrentInputModule
	BaseInputModule_t1295781545 * ___m_CurrentInputModule_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_FirstSelected
	GameObject_t1756533147 * ___m_FirstSelected_5;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_sendNavigationEvents
	bool ___m_sendNavigationEvents_6;
	// System.Int32 UnityEngine.EventSystems.EventSystem::m_DragThreshold
	int32_t ___m_DragThreshold_7;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_CurrentSelected
	GameObject_t1756533147 * ___m_CurrentSelected_8;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_HasFocus
	bool ___m_HasFocus_9;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_SelectionGuard
	bool ___m_SelectionGuard_10;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.EventSystem::m_DummyData
	BaseEventData_t2681005625 * ___m_DummyData_11;

public:
	inline static int32_t get_offset_of_m_SystemInputModules_2() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263, ___m_SystemInputModules_2)); }
	inline List_1_t664902677 * get_m_SystemInputModules_2() const { return ___m_SystemInputModules_2; }
	inline List_1_t664902677 ** get_address_of_m_SystemInputModules_2() { return &___m_SystemInputModules_2; }
	inline void set_m_SystemInputModules_2(List_1_t664902677 * value)
	{
		___m_SystemInputModules_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SystemInputModules_2), value);
	}

	inline static int32_t get_offset_of_m_CurrentInputModule_3() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263, ___m_CurrentInputModule_3)); }
	inline BaseInputModule_t1295781545 * get_m_CurrentInputModule_3() const { return ___m_CurrentInputModule_3; }
	inline BaseInputModule_t1295781545 ** get_address_of_m_CurrentInputModule_3() { return &___m_CurrentInputModule_3; }
	inline void set_m_CurrentInputModule_3(BaseInputModule_t1295781545 * value)
	{
		___m_CurrentInputModule_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentInputModule_3), value);
	}

	inline static int32_t get_offset_of_m_FirstSelected_5() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263, ___m_FirstSelected_5)); }
	inline GameObject_t1756533147 * get_m_FirstSelected_5() const { return ___m_FirstSelected_5; }
	inline GameObject_t1756533147 ** get_address_of_m_FirstSelected_5() { return &___m_FirstSelected_5; }
	inline void set_m_FirstSelected_5(GameObject_t1756533147 * value)
	{
		___m_FirstSelected_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_FirstSelected_5), value);
	}

	inline static int32_t get_offset_of_m_sendNavigationEvents_6() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263, ___m_sendNavigationEvents_6)); }
	inline bool get_m_sendNavigationEvents_6() const { return ___m_sendNavigationEvents_6; }
	inline bool* get_address_of_m_sendNavigationEvents_6() { return &___m_sendNavigationEvents_6; }
	inline void set_m_sendNavigationEvents_6(bool value)
	{
		___m_sendNavigationEvents_6 = value;
	}

	inline static int32_t get_offset_of_m_DragThreshold_7() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263, ___m_DragThreshold_7)); }
	inline int32_t get_m_DragThreshold_7() const { return ___m_DragThreshold_7; }
	inline int32_t* get_address_of_m_DragThreshold_7() { return &___m_DragThreshold_7; }
	inline void set_m_DragThreshold_7(int32_t value)
	{
		___m_DragThreshold_7 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelected_8() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263, ___m_CurrentSelected_8)); }
	inline GameObject_t1756533147 * get_m_CurrentSelected_8() const { return ___m_CurrentSelected_8; }
	inline GameObject_t1756533147 ** get_address_of_m_CurrentSelected_8() { return &___m_CurrentSelected_8; }
	inline void set_m_CurrentSelected_8(GameObject_t1756533147 * value)
	{
		___m_CurrentSelected_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentSelected_8), value);
	}

	inline static int32_t get_offset_of_m_HasFocus_9() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263, ___m_HasFocus_9)); }
	inline bool get_m_HasFocus_9() const { return ___m_HasFocus_9; }
	inline bool* get_address_of_m_HasFocus_9() { return &___m_HasFocus_9; }
	inline void set_m_HasFocus_9(bool value)
	{
		___m_HasFocus_9 = value;
	}

	inline static int32_t get_offset_of_m_SelectionGuard_10() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263, ___m_SelectionGuard_10)); }
	inline bool get_m_SelectionGuard_10() const { return ___m_SelectionGuard_10; }
	inline bool* get_address_of_m_SelectionGuard_10() { return &___m_SelectionGuard_10; }
	inline void set_m_SelectionGuard_10(bool value)
	{
		___m_SelectionGuard_10 = value;
	}

	inline static int32_t get_offset_of_m_DummyData_11() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263, ___m_DummyData_11)); }
	inline BaseEventData_t2681005625 * get_m_DummyData_11() const { return ___m_DummyData_11; }
	inline BaseEventData_t2681005625 ** get_address_of_m_DummyData_11() { return &___m_DummyData_11; }
	inline void set_m_DummyData_11(BaseEventData_t2681005625 * value)
	{
		___m_DummyData_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_DummyData_11), value);
	}
};

struct EventSystem_t3466835263_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem> UnityEngine.EventSystems.EventSystem::m_EventSystems
	List_1_t2835956395 * ___m_EventSystems_4;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::s_RaycastComparer
	Comparison_1_t1282925227 * ___s_RaycastComparer_12;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::<>f__mg$cache0
	Comparison_1_t1282925227 * ___U3CU3Ef__mgU24cache0_13;

public:
	inline static int32_t get_offset_of_m_EventSystems_4() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263_StaticFields, ___m_EventSystems_4)); }
	inline List_1_t2835956395 * get_m_EventSystems_4() const { return ___m_EventSystems_4; }
	inline List_1_t2835956395 ** get_address_of_m_EventSystems_4() { return &___m_EventSystems_4; }
	inline void set_m_EventSystems_4(List_1_t2835956395 * value)
	{
		___m_EventSystems_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystems_4), value);
	}

	inline static int32_t get_offset_of_s_RaycastComparer_12() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263_StaticFields, ___s_RaycastComparer_12)); }
	inline Comparison_1_t1282925227 * get_s_RaycastComparer_12() const { return ___s_RaycastComparer_12; }
	inline Comparison_1_t1282925227 ** get_address_of_s_RaycastComparer_12() { return &___s_RaycastComparer_12; }
	inline void set_s_RaycastComparer_12(Comparison_1_t1282925227 * value)
	{
		___s_RaycastComparer_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_RaycastComparer_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_13() { return static_cast<int32_t>(offsetof(EventSystem_t3466835263_StaticFields, ___U3CU3Ef__mgU24cache0_13)); }
	inline Comparison_1_t1282925227 * get_U3CU3Ef__mgU24cache0_13() const { return ___U3CU3Ef__mgU24cache0_13; }
	inline Comparison_1_t1282925227 ** get_address_of_U3CU3Ef__mgU24cache0_13() { return &___U3CU3Ef__mgU24cache0_13; }
	inline void set_U3CU3Ef__mgU24cache0_13(Comparison_1_t1282925227 * value)
	{
		___U3CU3Ef__mgU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSYSTEM_T3466835263_H
#ifndef JUMPTRIGGER_T1208645988_H
#define JUMPTRIGGER_T1208645988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JumpTrigger
struct  JumpTrigger_t1208645988  : public Trigger_t3844394560
{
public:
	// UnityEngine.Vector3 JumpTrigger::vert
	Vector3_t2243707580  ___vert_2;
	// UnityEngine.Vector3 JumpTrigger::horiz
	Vector3_t2243707580  ___horiz_3;
	// System.Single JumpTrigger::width
	float ___width_4;
	// System.Single JumpTrigger::height
	float ___height_5;
	// UnityEngine.Vector3 JumpTrigger::axis
	Vector3_t2243707580  ___axis_6;
	// System.Single JumpTrigger::halfWidth
	float ___halfWidth_7;
	// Character JumpTrigger::character
	Character_t3726027591 * ___character_8;
	// Level JumpTrigger::level
	Level_t866853782 * ___level_9;
	// System.Boolean JumpTrigger::executing
	bool ___executing_10;
	// System.Single JumpTrigger::progress
	float ___progress_11;
	// UnityEngine.Vector3 JumpTrigger::start
	Vector3_t2243707580  ___start_12;
	// UnityEngine.Quaternion JumpTrigger::rotation
	Quaternion_t4030073918  ___rotation_13;

public:
	inline static int32_t get_offset_of_vert_2() { return static_cast<int32_t>(offsetof(JumpTrigger_t1208645988, ___vert_2)); }
	inline Vector3_t2243707580  get_vert_2() const { return ___vert_2; }
	inline Vector3_t2243707580 * get_address_of_vert_2() { return &___vert_2; }
	inline void set_vert_2(Vector3_t2243707580  value)
	{
		___vert_2 = value;
	}

	inline static int32_t get_offset_of_horiz_3() { return static_cast<int32_t>(offsetof(JumpTrigger_t1208645988, ___horiz_3)); }
	inline Vector3_t2243707580  get_horiz_3() const { return ___horiz_3; }
	inline Vector3_t2243707580 * get_address_of_horiz_3() { return &___horiz_3; }
	inline void set_horiz_3(Vector3_t2243707580  value)
	{
		___horiz_3 = value;
	}

	inline static int32_t get_offset_of_width_4() { return static_cast<int32_t>(offsetof(JumpTrigger_t1208645988, ___width_4)); }
	inline float get_width_4() const { return ___width_4; }
	inline float* get_address_of_width_4() { return &___width_4; }
	inline void set_width_4(float value)
	{
		___width_4 = value;
	}

	inline static int32_t get_offset_of_height_5() { return static_cast<int32_t>(offsetof(JumpTrigger_t1208645988, ___height_5)); }
	inline float get_height_5() const { return ___height_5; }
	inline float* get_address_of_height_5() { return &___height_5; }
	inline void set_height_5(float value)
	{
		___height_5 = value;
	}

	inline static int32_t get_offset_of_axis_6() { return static_cast<int32_t>(offsetof(JumpTrigger_t1208645988, ___axis_6)); }
	inline Vector3_t2243707580  get_axis_6() const { return ___axis_6; }
	inline Vector3_t2243707580 * get_address_of_axis_6() { return &___axis_6; }
	inline void set_axis_6(Vector3_t2243707580  value)
	{
		___axis_6 = value;
	}

	inline static int32_t get_offset_of_halfWidth_7() { return static_cast<int32_t>(offsetof(JumpTrigger_t1208645988, ___halfWidth_7)); }
	inline float get_halfWidth_7() const { return ___halfWidth_7; }
	inline float* get_address_of_halfWidth_7() { return &___halfWidth_7; }
	inline void set_halfWidth_7(float value)
	{
		___halfWidth_7 = value;
	}

	inline static int32_t get_offset_of_character_8() { return static_cast<int32_t>(offsetof(JumpTrigger_t1208645988, ___character_8)); }
	inline Character_t3726027591 * get_character_8() const { return ___character_8; }
	inline Character_t3726027591 ** get_address_of_character_8() { return &___character_8; }
	inline void set_character_8(Character_t3726027591 * value)
	{
		___character_8 = value;
		Il2CppCodeGenWriteBarrier((&___character_8), value);
	}

	inline static int32_t get_offset_of_level_9() { return static_cast<int32_t>(offsetof(JumpTrigger_t1208645988, ___level_9)); }
	inline Level_t866853782 * get_level_9() const { return ___level_9; }
	inline Level_t866853782 ** get_address_of_level_9() { return &___level_9; }
	inline void set_level_9(Level_t866853782 * value)
	{
		___level_9 = value;
		Il2CppCodeGenWriteBarrier((&___level_9), value);
	}

	inline static int32_t get_offset_of_executing_10() { return static_cast<int32_t>(offsetof(JumpTrigger_t1208645988, ___executing_10)); }
	inline bool get_executing_10() const { return ___executing_10; }
	inline bool* get_address_of_executing_10() { return &___executing_10; }
	inline void set_executing_10(bool value)
	{
		___executing_10 = value;
	}

	inline static int32_t get_offset_of_progress_11() { return static_cast<int32_t>(offsetof(JumpTrigger_t1208645988, ___progress_11)); }
	inline float get_progress_11() const { return ___progress_11; }
	inline float* get_address_of_progress_11() { return &___progress_11; }
	inline void set_progress_11(float value)
	{
		___progress_11 = value;
	}

	inline static int32_t get_offset_of_start_12() { return static_cast<int32_t>(offsetof(JumpTrigger_t1208645988, ___start_12)); }
	inline Vector3_t2243707580  get_start_12() const { return ___start_12; }
	inline Vector3_t2243707580 * get_address_of_start_12() { return &___start_12; }
	inline void set_start_12(Vector3_t2243707580  value)
	{
		___start_12 = value;
	}

	inline static int32_t get_offset_of_rotation_13() { return static_cast<int32_t>(offsetof(JumpTrigger_t1208645988, ___rotation_13)); }
	inline Quaternion_t4030073918  get_rotation_13() const { return ___rotation_13; }
	inline Quaternion_t4030073918 * get_address_of_rotation_13() { return &___rotation_13; }
	inline void set_rotation_13(Quaternion_t4030073918  value)
	{
		___rotation_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JUMPTRIGGER_T1208645988_H
#ifndef GRAPHIC_T2426225576_H
#define GRAPHIC_T2426225576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t2426225576  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t193706927 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2020392075  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3349966182 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t261436805 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t209405766 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t4025899511 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t4025899511 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t4025899511 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3177091249 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Material_4)); }
	inline Material_t193706927 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t193706927 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t193706927 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Color_5)); }
	inline Color_t2020392075  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2020392075 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2020392075  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RectTransform_7)); }
	inline RectTransform_t3349966182 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3349966182 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3349966182 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t261436805 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t261436805 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t261436805 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Canvas_9)); }
	inline Canvas_t209405766 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t209405766 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t209405766 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3177091249 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3177091249 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3177091249 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t2426225576_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t193706927 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3542995729 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t1356156583 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t385374196 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t193706927 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t193706927 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t193706927 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3542995729 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3542995729 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3542995729 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t1356156583 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t1356156583 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t1356156583 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t385374196 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t385374196 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t385374196 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T2426225576_H
#ifndef HINTTRIGGER_T910047485_H
#define HINTTRIGGER_T910047485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HintTrigger
struct  HintTrigger_t910047485  : public Trigger_t3844394560
{
public:
	// System.String HintTrigger::message
	String_t* ___message_2;
	// Menu HintTrigger::menu
	Menu_t4261767481 * ___menu_3;

public:
	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(HintTrigger_t910047485, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_menu_3() { return static_cast<int32_t>(offsetof(HintTrigger_t910047485, ___menu_3)); }
	inline Menu_t4261767481 * get_menu_3() const { return ___menu_3; }
	inline Menu_t4261767481 ** get_address_of_menu_3() { return &___menu_3; }
	inline void set_menu_3(Menu_t4261767481 * value)
	{
		___menu_3 = value;
		Il2CppCodeGenWriteBarrier((&___menu_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HINTTRIGGER_T910047485_H
#ifndef CREATETRIGGER_T2237538136_H
#define CREATETRIGGER_T2237538136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateTrigger
struct  CreateTrigger_t2237538136  : public Trigger_t3844394560
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> CreateTrigger::relPositions
	List_1_t1612828712 * ___relPositions_2;
	// Level CreateTrigger::level
	Level_t866853782 * ___level_3;
	// System.Boolean CreateTrigger::created
	bool ___created_4;

public:
	inline static int32_t get_offset_of_relPositions_2() { return static_cast<int32_t>(offsetof(CreateTrigger_t2237538136, ___relPositions_2)); }
	inline List_1_t1612828712 * get_relPositions_2() const { return ___relPositions_2; }
	inline List_1_t1612828712 ** get_address_of_relPositions_2() { return &___relPositions_2; }
	inline void set_relPositions_2(List_1_t1612828712 * value)
	{
		___relPositions_2 = value;
		Il2CppCodeGenWriteBarrier((&___relPositions_2), value);
	}

	inline static int32_t get_offset_of_level_3() { return static_cast<int32_t>(offsetof(CreateTrigger_t2237538136, ___level_3)); }
	inline Level_t866853782 * get_level_3() const { return ___level_3; }
	inline Level_t866853782 ** get_address_of_level_3() { return &___level_3; }
	inline void set_level_3(Level_t866853782 * value)
	{
		___level_3 = value;
		Il2CppCodeGenWriteBarrier((&___level_3), value);
	}

	inline static int32_t get_offset_of_created_4() { return static_cast<int32_t>(offsetof(CreateTrigger_t2237538136, ___created_4)); }
	inline bool get_created_4() const { return ___created_4; }
	inline bool* get_address_of_created_4() { return &___created_4; }
	inline void set_created_4(bool value)
	{
		___created_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATETRIGGER_T2237538136_H
#ifndef EXPLOSIONTRIGGER_T4191861867_H
#define EXPLOSIONTRIGGER_T4191861867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExplosionTrigger
struct  ExplosionTrigger_t4191861867  : public Trigger_t3844394560
{
public:
	// UnityEngine.GameObject ExplosionTrigger::target
	GameObject_t1756533147 * ___target_2;
	// Level ExplosionTrigger::level
	Level_t866853782 * ___level_3;
	// System.Boolean ExplosionTrigger::destroyed
	bool ___destroyed_4;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(ExplosionTrigger_t4191861867, ___target_2)); }
	inline GameObject_t1756533147 * get_target_2() const { return ___target_2; }
	inline GameObject_t1756533147 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(GameObject_t1756533147 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_level_3() { return static_cast<int32_t>(offsetof(ExplosionTrigger_t4191861867, ___level_3)); }
	inline Level_t866853782 * get_level_3() const { return ___level_3; }
	inline Level_t866853782 ** get_address_of_level_3() { return &___level_3; }
	inline void set_level_3(Level_t866853782 * value)
	{
		___level_3 = value;
		Il2CppCodeGenWriteBarrier((&___level_3), value);
	}

	inline static int32_t get_offset_of_destroyed_4() { return static_cast<int32_t>(offsetof(ExplosionTrigger_t4191861867, ___destroyed_4)); }
	inline bool get_destroyed_4() const { return ___destroyed_4; }
	inline bool* get_address_of_destroyed_4() { return &___destroyed_4; }
	inline void set_destroyed_4(bool value)
	{
		___destroyed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPLOSIONTRIGGER_T4191861867_H
#ifndef TRAPTRIGGER_T598219061_H
#define TRAPTRIGGER_T598219061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrapTrigger
struct  TrapTrigger_t598219061  : public Trigger_t3844394560
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAPTRIGGER_T598219061_H
#ifndef TOGGLETRIGGER_T2585261492_H
#define TOGGLETRIGGER_T2585261492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ToggleTrigger
struct  ToggleTrigger_t2585261492  : public Trigger_t3844394560
{
public:
	// System.Boolean ToggleTrigger::active
	bool ___active_2;
	// UnityEngine.Material ToggleTrigger::mat1
	Material_t193706927 * ___mat1_3;
	// UnityEngine.Material ToggleTrigger::mat2
	Material_t193706927 * ___mat2_4;

public:
	inline static int32_t get_offset_of_active_2() { return static_cast<int32_t>(offsetof(ToggleTrigger_t2585261492, ___active_2)); }
	inline bool get_active_2() const { return ___active_2; }
	inline bool* get_address_of_active_2() { return &___active_2; }
	inline void set_active_2(bool value)
	{
		___active_2 = value;
	}

	inline static int32_t get_offset_of_mat1_3() { return static_cast<int32_t>(offsetof(ToggleTrigger_t2585261492, ___mat1_3)); }
	inline Material_t193706927 * get_mat1_3() const { return ___mat1_3; }
	inline Material_t193706927 ** get_address_of_mat1_3() { return &___mat1_3; }
	inline void set_mat1_3(Material_t193706927 * value)
	{
		___mat1_3 = value;
		Il2CppCodeGenWriteBarrier((&___mat1_3), value);
	}

	inline static int32_t get_offset_of_mat2_4() { return static_cast<int32_t>(offsetof(ToggleTrigger_t2585261492, ___mat2_4)); }
	inline Material_t193706927 * get_mat2_4() const { return ___mat2_4; }
	inline Material_t193706927 ** get_address_of_mat2_4() { return &___mat2_4; }
	inline void set_mat2_4(Material_t193706927 * value)
	{
		___mat2_4 = value;
		Il2CppCodeGenWriteBarrier((&___mat2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLETRIGGER_T2585261492_H
#ifndef SELECTABLE_T1490392188_H
#define SELECTABLE_T1490392188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t1490392188  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1571958496  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2652774230  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1353336012  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t3244928895 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t2426225576 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t2665681875 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Navigation_3)); }
	inline Navigation_t1571958496  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t1571958496 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t1571958496  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Colors_5)); }
	inline ColorBlock_t2652774230  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t2652774230 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t2652774230  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_SpriteState_6)); }
	inline SpriteState_t1353336012  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t1353336012 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t1353336012  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t3244928895 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t3244928895 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t3244928895 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_TargetGraphic_9)); }
	inline Graphic_t2426225576 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t2426225576 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t2426225576 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_CanvasGroupCache_15)); }
	inline List_1_t2665681875 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t2665681875 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t2665681875 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t1490392188_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t859513320 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t1490392188_StaticFields, ___s_List_2)); }
	inline List_1_t859513320 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t859513320 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t859513320 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T1490392188_H
#ifndef SPECTATETRIGGER_T3335062351_H
#define SPECTATETRIGGER_T3335062351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpectateTrigger
struct  SpectateTrigger_t3335062351  : public Trigger_t3844394560
{
public:
	// UnityEngine.Animator SpectateTrigger::animator
	Animator_t69676727 * ___animator_2;
	// System.String SpectateTrigger::animationName
	String_t* ___animationName_3;
	// System.Boolean SpectateTrigger::animating
	bool ___animating_4;
	// System.Boolean SpectateTrigger::previousAnimating
	bool ___previousAnimating_5;

public:
	inline static int32_t get_offset_of_animator_2() { return static_cast<int32_t>(offsetof(SpectateTrigger_t3335062351, ___animator_2)); }
	inline Animator_t69676727 * get_animator_2() const { return ___animator_2; }
	inline Animator_t69676727 ** get_address_of_animator_2() { return &___animator_2; }
	inline void set_animator_2(Animator_t69676727 * value)
	{
		___animator_2 = value;
		Il2CppCodeGenWriteBarrier((&___animator_2), value);
	}

	inline static int32_t get_offset_of_animationName_3() { return static_cast<int32_t>(offsetof(SpectateTrigger_t3335062351, ___animationName_3)); }
	inline String_t* get_animationName_3() const { return ___animationName_3; }
	inline String_t** get_address_of_animationName_3() { return &___animationName_3; }
	inline void set_animationName_3(String_t* value)
	{
		___animationName_3 = value;
		Il2CppCodeGenWriteBarrier((&___animationName_3), value);
	}

	inline static int32_t get_offset_of_animating_4() { return static_cast<int32_t>(offsetof(SpectateTrigger_t3335062351, ___animating_4)); }
	inline bool get_animating_4() const { return ___animating_4; }
	inline bool* get_address_of_animating_4() { return &___animating_4; }
	inline void set_animating_4(bool value)
	{
		___animating_4 = value;
	}

	inline static int32_t get_offset_of_previousAnimating_5() { return static_cast<int32_t>(offsetof(SpectateTrigger_t3335062351, ___previousAnimating_5)); }
	inline bool get_previousAnimating_5() const { return ___previousAnimating_5; }
	inline bool* get_address_of_previousAnimating_5() { return &___previousAnimating_5; }
	inline void set_previousAnimating_5(bool value)
	{
		___previousAnimating_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECTATETRIGGER_T3335062351_H
#ifndef WINTRIGGER_T872747498_H
#define WINTRIGGER_T872747498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WinTrigger
struct  WinTrigger_t872747498  : public Trigger_t3844394560
{
public:
	// Menu WinTrigger::menu
	Menu_t4261767481 * ___menu_2;

public:
	inline static int32_t get_offset_of_menu_2() { return static_cast<int32_t>(offsetof(WinTrigger_t872747498, ___menu_2)); }
	inline Menu_t4261767481 * get_menu_2() const { return ___menu_2; }
	inline Menu_t4261767481 ** get_address_of_menu_2() { return &___menu_2; }
	inline void set_menu_2(Menu_t4261767481 * value)
	{
		___menu_2 = value;
		Il2CppCodeGenWriteBarrier((&___menu_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINTRIGGER_T872747498_H
#ifndef BUTTON_T2872111280_H
#define BUTTON_T2872111280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Button
struct  Button_t2872111280  : public Selectable_t1490392188
{
public:
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_t2455055323 * ___m_OnClick_16;

public:
	inline static int32_t get_offset_of_m_OnClick_16() { return static_cast<int32_t>(offsetof(Button_t2872111280, ___m_OnClick_16)); }
	inline ButtonClickedEvent_t2455055323 * get_m_OnClick_16() const { return ___m_OnClick_16; }
	inline ButtonClickedEvent_t2455055323 ** get_address_of_m_OnClick_16() { return &___m_OnClick_16; }
	inline void set_m_OnClick_16(ButtonClickedEvent_t2455055323 * value)
	{
		___m_OnClick_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnClick_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T2872111280_H
#ifndef MASKABLEGRAPHIC_T540192618_H
#define MASKABLEGRAPHIC_T540192618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t540192618  : public Graphic_t2426225576
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t193706927 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t1156185964 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3778758259 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1172311765* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_MaskMaterial_20)); }
	inline Material_t193706927 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t193706927 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t193706927 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ParentMask_21)); }
	inline RectMask2D_t1156185964 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t1156185964 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t1156185964 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3778758259 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3778758259 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3778758259 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1172311765* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1172311765* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T540192618_H
#ifndef IMAGE_T2042527209_H
#define IMAGE_T2042527209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t2042527209  : public MaskableGraphic_t540192618
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t309593783 * ___m_Sprite_29;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t309593783 * ___m_OverrideSprite_30;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_31;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_32;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_33;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_34;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_35;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_36;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_37;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_38;

public:
	inline static int32_t get_offset_of_m_Sprite_29() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_Sprite_29)); }
	inline Sprite_t309593783 * get_m_Sprite_29() const { return ___m_Sprite_29; }
	inline Sprite_t309593783 ** get_address_of_m_Sprite_29() { return &___m_Sprite_29; }
	inline void set_m_Sprite_29(Sprite_t309593783 * value)
	{
		___m_Sprite_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_29), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_30() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_OverrideSprite_30)); }
	inline Sprite_t309593783 * get_m_OverrideSprite_30() const { return ___m_OverrideSprite_30; }
	inline Sprite_t309593783 ** get_address_of_m_OverrideSprite_30() { return &___m_OverrideSprite_30; }
	inline void set_m_OverrideSprite_30(Sprite_t309593783 * value)
	{
		___m_OverrideSprite_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_30), value);
	}

	inline static int32_t get_offset_of_m_Type_31() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_Type_31)); }
	inline int32_t get_m_Type_31() const { return ___m_Type_31; }
	inline int32_t* get_address_of_m_Type_31() { return &___m_Type_31; }
	inline void set_m_Type_31(int32_t value)
	{
		___m_Type_31 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_32() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_PreserveAspect_32)); }
	inline bool get_m_PreserveAspect_32() const { return ___m_PreserveAspect_32; }
	inline bool* get_address_of_m_PreserveAspect_32() { return &___m_PreserveAspect_32; }
	inline void set_m_PreserveAspect_32(bool value)
	{
		___m_PreserveAspect_32 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_33() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_FillCenter_33)); }
	inline bool get_m_FillCenter_33() const { return ___m_FillCenter_33; }
	inline bool* get_address_of_m_FillCenter_33() { return &___m_FillCenter_33; }
	inline void set_m_FillCenter_33(bool value)
	{
		___m_FillCenter_33 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_34() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_FillMethod_34)); }
	inline int32_t get_m_FillMethod_34() const { return ___m_FillMethod_34; }
	inline int32_t* get_address_of_m_FillMethod_34() { return &___m_FillMethod_34; }
	inline void set_m_FillMethod_34(int32_t value)
	{
		___m_FillMethod_34 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_35() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_FillAmount_35)); }
	inline float get_m_FillAmount_35() const { return ___m_FillAmount_35; }
	inline float* get_address_of_m_FillAmount_35() { return &___m_FillAmount_35; }
	inline void set_m_FillAmount_35(float value)
	{
		___m_FillAmount_35 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_36() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_FillClockwise_36)); }
	inline bool get_m_FillClockwise_36() const { return ___m_FillClockwise_36; }
	inline bool* get_address_of_m_FillClockwise_36() { return &___m_FillClockwise_36; }
	inline void set_m_FillClockwise_36(bool value)
	{
		___m_FillClockwise_36 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_37() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_FillOrigin_37)); }
	inline int32_t get_m_FillOrigin_37() const { return ___m_FillOrigin_37; }
	inline int32_t* get_address_of_m_FillOrigin_37() { return &___m_FillOrigin_37; }
	inline void set_m_FillOrigin_37(int32_t value)
	{
		___m_FillOrigin_37 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_38() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_AlphaHitTestMinimumThreshold_38)); }
	inline float get_m_AlphaHitTestMinimumThreshold_38() const { return ___m_AlphaHitTestMinimumThreshold_38; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_38() { return &___m_AlphaHitTestMinimumThreshold_38; }
	inline void set_m_AlphaHitTestMinimumThreshold_38(float value)
	{
		___m_AlphaHitTestMinimumThreshold_38 = value;
	}
};

struct Image_t2042527209_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t193706927 * ___s_ETC1DefaultUI_28;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_t686124026* ___s_VertScratch_39;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_t686124026* ___s_UVScratch_40;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t1172311765* ___s_Xy_41;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t1172311765* ___s_Uv_42;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_28() { return static_cast<int32_t>(offsetof(Image_t2042527209_StaticFields, ___s_ETC1DefaultUI_28)); }
	inline Material_t193706927 * get_s_ETC1DefaultUI_28() const { return ___s_ETC1DefaultUI_28; }
	inline Material_t193706927 ** get_address_of_s_ETC1DefaultUI_28() { return &___s_ETC1DefaultUI_28; }
	inline void set_s_ETC1DefaultUI_28(Material_t193706927 * value)
	{
		___s_ETC1DefaultUI_28 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_28), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_39() { return static_cast<int32_t>(offsetof(Image_t2042527209_StaticFields, ___s_VertScratch_39)); }
	inline Vector2U5BU5D_t686124026* get_s_VertScratch_39() const { return ___s_VertScratch_39; }
	inline Vector2U5BU5D_t686124026** get_address_of_s_VertScratch_39() { return &___s_VertScratch_39; }
	inline void set_s_VertScratch_39(Vector2U5BU5D_t686124026* value)
	{
		___s_VertScratch_39 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_39), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_40() { return static_cast<int32_t>(offsetof(Image_t2042527209_StaticFields, ___s_UVScratch_40)); }
	inline Vector2U5BU5D_t686124026* get_s_UVScratch_40() const { return ___s_UVScratch_40; }
	inline Vector2U5BU5D_t686124026** get_address_of_s_UVScratch_40() { return &___s_UVScratch_40; }
	inline void set_s_UVScratch_40(Vector2U5BU5D_t686124026* value)
	{
		___s_UVScratch_40 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_40), value);
	}

	inline static int32_t get_offset_of_s_Xy_41() { return static_cast<int32_t>(offsetof(Image_t2042527209_StaticFields, ___s_Xy_41)); }
	inline Vector3U5BU5D_t1172311765* get_s_Xy_41() const { return ___s_Xy_41; }
	inline Vector3U5BU5D_t1172311765** get_address_of_s_Xy_41() { return &___s_Xy_41; }
	inline void set_s_Xy_41(Vector3U5BU5D_t1172311765* value)
	{
		___s_Xy_41 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_41), value);
	}

	inline static int32_t get_offset_of_s_Uv_42() { return static_cast<int32_t>(offsetof(Image_t2042527209_StaticFields, ___s_Uv_42)); }
	inline Vector3U5BU5D_t1172311765* get_s_Uv_42() const { return ___s_Uv_42; }
	inline Vector3U5BU5D_t1172311765** get_address_of_s_Uv_42() { return &___s_Uv_42; }
	inline void set_s_Uv_42(Vector3U5BU5D_t1172311765* value)
	{
		___s_Uv_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_42), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T2042527209_H
#ifndef TEXT_T356221433_H
#define TEXT_T356221433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t356221433  : public MaskableGraphic_t540192618
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t2614388407 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t647235000 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t647235000 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t3048644023* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_FontData_28)); }
	inline FontData_t2614388407 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t2614388407 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t2614388407 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_TextCache_30)); }
	inline TextGenerator_t647235000 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t647235000 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t647235000 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t647235000 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t647235000 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t647235000 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t3048644023* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t3048644023** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t3048644023* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t356221433_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t193706927 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t356221433_StaticFields, ___s_DefaultText_32)); }
	inline Material_t193706927 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t193706927 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t193706927 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T356221433_H
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Transform_t3275118058 * m_Items[1];

public:
	inline Transform_t3275118058 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Transform_t3275118058 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Transform_t3275118058 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Transform_t3275118058 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Transform_t3275118058 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Transform_t3275118058 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Assets.scripts.Cell[,,]
struct CellU5BU2CU2CU5D_t1218597327  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Cell_t964604196 * m_Items[1];

public:
	inline Cell_t964604196 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Cell_t964604196 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Cell_t964604196 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Cell_t964604196 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Cell_t964604196 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Cell_t964604196 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Cell_t964604196 * GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items[index];
	}
	inline Cell_t964604196 ** GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k, Cell_t964604196 * value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Cell_t964604196 * GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k) const
	{
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items[index];
	}
	inline Cell_t964604196 ** GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k)
	{
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k, Cell_t964604196 * value)
	{
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Touch[]
struct TouchU5BU5D_t3887265178  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Touch_t407273883  m_Items[1];

public:
	inline Touch_t407273883  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Touch_t407273883 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Touch_t407273883  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Touch_t407273883  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Touch_t407273883 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Touch_t407273883  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_t1756533147 * m_Items[1];

public:
	inline GameObject_t1756533147 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t1756533147 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t1756533147 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_t1756533147 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t1756533147 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t1756533147 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Renderer[]
struct RendererU5BU5D_t2810717544  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Renderer_t257310565 * m_Items[1];

public:
	inline Renderer_t257310565 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Renderer_t257310565 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Renderer_t257310565 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Renderer_t257310565 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Renderer_t257310565 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Renderer_t257310565 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Trigger[]
struct TriggerU5BU5D_t3145789633  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Trigger_t3844394560 * m_Items[1];

public:
	inline Trigger_t3844394560 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Trigger_t3844394560 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Trigger_t3844394560 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Trigger_t3844394560 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Trigger_t3844394560 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Trigger_t3844394560 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Object[]
struct ObjectU5BU5D_t4217747464  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Object_t1021602117 * m_Items[1];

public:
	inline Object_t1021602117 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Object_t1021602117 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Object_t1021602117 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Object_t1021602117 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Object_t1021602117 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Object_t1021602117 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared (GameObject_t1756533147 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t1593300101  List_1_GetEnumerator_m2837081829_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m2577424081_gshared (Enumerator_t1593300101 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m4254626809_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m4157722533_gshared (List_1_t2058570427 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m44995089_gshared (Enumerator_t1593300101 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3736175406_gshared (Enumerator_t1593300101 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
extern "C"  void List_1__ctor_m347461442_gshared (List_1_t1612828712 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m310736118_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
extern "C"  void List_1_Add_m2338641291_gshared (List_1_t1612828712 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2375293942_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m2062981835_gshared (List_1_t2058570427 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3615096820_gshared (List_1_t2058570427 * __this, int32_t p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.Vector3>::GetEnumerator()
extern "C"  Enumerator_t1147558386  List_1_GetEnumerator_m940839541_gshared (List_1_t1612828712 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::get_Current()
extern "C"  Vector3_t2243707580  Enumerator_get_Current_m857008049_gshared (Enumerator_t1147558386 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2007266881_gshared (Enumerator_t1147558386 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::Dispose()
extern "C"  void Enumerator_Dispose_m3215924523_gshared (Enumerator_t1147558386 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2724124387_gshared (Component_t3819376471 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* GameObject_GetComponentsInChildren_TisRuntimeObject_m1467294482_gshared (GameObject_t1756533147 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C"  bool List_1_Remove_m3164383811_gshared (List_1_t2058570427 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(!0)
extern "C"  bool List_1_Contains_m1658838094_gshared (List_1_t2058570427 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,!0)
extern "C"  void List_1_Insert_m4101600027_gshared (List_1_t2058570427 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Reverse()
extern "C"  void List_1_Reverse_m723397084_gshared (List_1_t1612828712 * __this, const RuntimeMethod* method);

// System.Void Trigger::.ctor()
extern "C"  void Trigger__ctor_m1356284031 (Trigger_t3844394560 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::Normalize()
extern "C"  void Vector3_Normalize_m2606725557 (Vector3_t2243707580 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t1756533147 * GameObject_Find_m279709565 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<Level>()
#define GameObject_GetComponent_TisLevel_t866853782_m3318339701(__this, method) ((  Level_t866853782 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m2923680153 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m3374354972 (Component_t3819376471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m2304215762 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Addition_m394909128 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Assets.scripts.Cell Level::cellAtPosition(UnityEngine.Vector3)
extern "C"  Cell_t964604196 * Level_cellAtPosition_m891825944 (Level_t866853782 * __this, Vector3_t2243707580  ___position0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Assets.scripts.Link> Assets.scripts.Cell::get_links()
extern "C"  List_1_t145766874 * Cell_get_links_m2033144484 (Cell_t964604196 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Assets.scripts.Link>::GetEnumerator()
#define List_1_GetEnumerator_m3554822035(__this, method) ((  Enumerator_t3975463844  (*) (List_1_t145766874 *, const RuntimeMethod*))List_1_GetEnumerator_m2837081829_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<Assets.scripts.Link>::get_Current()
#define Enumerator_get_Current_m1037497887(__this, method) ((  Link_t776645742 * (*) (Enumerator_t3975463844 *, const RuntimeMethod*))Enumerator_get_Current_m2577424081_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Assets.scripts.Link>::Clear()
#define List_1_Clear_m1988879339(__this, method) ((  void (*) (List_1_t145766874 *, const RuntimeMethod*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Assets.scripts.Link>::Add(!0)
#define List_1_Add_m257155336(__this, p0, method) ((  void (*) (List_1_t145766874 *, Link_t776645742 *, const RuntimeMethod*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Assets.scripts.Link>::MoveNext()
#define Enumerator_MoveNext_m218825171(__this, method) ((  bool (*) (Enumerator_t3975463844 *, const RuntimeMethod*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Assets.scripts.Link>::Dispose()
#define Enumerator_Dispose_m2728416437(__this, method) ((  void (*) (Enumerator_t3975463844 *, const RuntimeMethod*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
#define List_1__ctor_m347461442(__this, method) ((  void (*) (List_1_t1612828712 *, const RuntimeMethod*))List_1__ctor_m347461442_gshared)(__this, method)
// System.Void Assets.scripts.Cell::set_midPoints(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void Cell_set_midPoints_m1417671087 (Cell_t964604196 * __this, List_1_t1612828712 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Assets.scripts.Cell>::.ctor()
#define List_1__ctor_m2909086300(__this, method) ((  void (*) (List_1_t333725328 *, const RuntimeMethod*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void Assets.scripts.Cell::set_blocks(System.Collections.Generic.List`1<Assets.scripts.Cell>)
extern "C"  void Cell_set_blocks_m1582678640 (Cell_t964604196 * __this, List_1_t333725328 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Assets.scripts.Link>::.ctor()
#define List_1__ctor_m1908755428(__this, method) ((  void (*) (List_1_t145766874 *, const RuntimeMethod*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void Assets.scripts.Cell::set_links(System.Collections.Generic.List`1<Assets.scripts.Link>)
extern "C"  void Cell_set_links_m2795728617 (Cell_t964604196 * __this, List_1_t145766874 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Assets.scripts.Cell::set_x(System.Int32)
extern "C"  void Cell_set_x_m2919876108 (Cell_t964604196 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Assets.scripts.Cell::set_y(System.Int32)
extern "C"  void Cell_set_y_m1423634631 (Cell_t964604196 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Assets.scripts.Cell::set_z(System.Int32)
extern "C"  void Cell_set_z_m883112214 (Cell_t964604196 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m1555724485 (Vector3_t2243707580 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Assets.scripts.Cell::set_pos(UnityEngine.Vector3)
extern "C"  void Cell_set_pos_m1164249580 (Cell_t964604196 * __this, Vector3_t2243707580  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Assets.scripts.Cell::set_isBlock(System.Boolean)
extern "C"  void Cell_set_isBlock_m2660126527 (Cell_t964604196 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Assets.scripts.Cell::calculateMidpoints()
extern "C"  void Cell_calculateMidpoints_m4202269553 (Cell_t964604196 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::Abs(System.Int32)
extern "C"  int32_t Mathf_Abs_m722236531 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Vector3> Assets.scripts.Cell::get_midPoints()
extern "C"  List_1_t1612828712 * Cell_get_midPoints_m2313462662 (Cell_t964604196 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Assets.scripts.Cell::get_x()
extern "C"  int32_t Cell_get_x_m2406521465 (Cell_t964604196 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Assets.scripts.Cell::get_y()
extern "C"  int32_t Cell_get_y_m2406521560 (Cell_t964604196 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Assets.scripts.Cell::get_z()
extern "C"  int32_t Cell_get_z_m2406521399 (Cell_t964604196 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
#define List_1_Add_m2338641291(__this, p0, method) ((  void (*) (List_1_t1612828712 *, Vector3_t2243707580 , const RuntimeMethod*))List_1_Add_m2338641291_gshared)(__this, p0, method)
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m2942701431 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1825328214 (MonoBehaviour_t1158329972 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m2159020946 (Component_t3819376471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m2516226135 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<Game>()
#define GameObject_GetComponent_TisGame_t1600141214_m3908916829(__this, method) ((  Game_t1600141214 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t4030073918  Transform_get_rotation_m2617026815 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Character::moveCharacter()
extern "C"  void Character_moveCharacter_m4256772666 (Character_t3726027591 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<Assets.scripts.Link>::get_Count()
#define List_1_get_Count_m3786137094(__this, method) ((  int32_t (*) (List_1_t145766874 *, const RuntimeMethod*))List_1_get_Count_m2375293942_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m3768854296 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<Assets.scripts.Link>::get_Item(System.Int32)
#define List_1_get_Item_m914633333(__this, p0, method) ((  Link_t776645742 * (*) (List_1_t145766874 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2062981835_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<Assets.scripts.Link>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m2271034946(__this, p0, method) ((  void (*) (List_1_t145766874 *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m3615096820_gshared)(__this, p0, method)
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m3925508629 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m1779415170 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m2824446320 (Transform_t3275118058 * __this, Quaternion_t4030073918  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::RotateAround(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  void Transform_RotateAround_m132750417 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, Vector3_t2243707580  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Character::reachedCell(System.Boolean)
extern "C"  void Character_reachedCell_m2549832251 (Character_t3726027591 * __this, bool ___isDestination0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Character::get_isPlayer()
extern "C"  bool Character_get_isPlayer_m2500959794 (Character_t3726027591 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m1757773010 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m3959286051 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Game::get_fragments()
extern "C"  int32_t Game_get_fragments_m2335769477 (Game_t1600141214 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Game::set_fragments(System.Int32)
extern "C"  void Game_set_fragments_m4122492370 (Game_t1600141214 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.Vector3>::GetEnumerator()
#define List_1_GetEnumerator_m940839541(__this, method) ((  Enumerator_t1147558386  (*) (List_1_t1612828712 *, const RuntimeMethod*))List_1_GetEnumerator_m940839541_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::get_Current()
#define Enumerator_get_Current_m857008049(__this, method) ((  Vector3_t2243707580  (*) (Enumerator_t1147558386 *, const RuntimeMethod*))Enumerator_get_Current_m857008049_gshared)(__this, method)
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3275118058 * Transform_get_parent_m2752514051 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern "C"  Object_t1021602117 * Resources_Load_m3480536692 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object)
extern "C"  Object_t1021602117 * Object_Instantiate_m698636442 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::set_tag(System.String)
extern "C"  void GameObject_set_tag_m2565837656 (GameObject_t1756533147 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3275118058 * GameObject_get_transform_m3490276752 (GameObject_t1756533147 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Level::addBlock(UnityEngine.Vector3)
extern "C"  void Level_addBlock_m1332986222 (Level_t866853782 * __this, Vector3_t2243707580  ___pos0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::MoveNext()
#define Enumerator_MoveNext_m2007266881(__this, method) ((  bool (*) (Enumerator_t1147558386 *, const RuntimeMethod*))Enumerator_MoveNext_m2007266881_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::Dispose()
#define Enumerator_Dispose_m3215924523(__this, method) ((  void (*) (Enumerator_t1147558386 *, const RuntimeMethod*))Enumerator_Dispose_m3215924523_gshared)(__this, method)
// System.Void Menu::updateFragments()
extern "C"  void Menu_updateFragments_m871188344 (Menu_t4261767481 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<Level>()
#define Component_GetComponent_TisLevel_t866853782_m4130163129(__this, method) ((  Level_t866853782 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Pathfinder>()
#define Component_GetComponent_TisPathfinder_t1592129589_m1369771324(__this, method) ((  Pathfinder_t1592129589 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Character>()
#define GameObject_GetComponent_TisCharacter_t3726027591_m2667239018(__this, method) ((  Character_t3726027591 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Menu>()
#define GameObject_GetComponent_TisMenu_t4261767481_m3170984502(__this, method) ((  Menu_t4261767481 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<InputController>()
#define Component_GetComponent_TisInputController_t3951031284_m1126083993(__this, method) ((  InputController_t3951031284 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C"  int32_t Application_get_platform_m1727035749 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t189460977 * Camera_get_main_m881971336 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3)
extern "C"  void Transform_LookAt_m1970949065 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t69676727_m475627522(__this, method) ((  Animator_t69676727 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m602406666 (Behaviour_t955675639 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKey(UnityEngine.KeyCode)
extern "C"  bool Input_GetKey_m4207472870 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputController::get_dragged()
extern "C"  bool InputController_get_dragged_m1166786620 (InputController_t3951031284 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 InputController::get_dragVector()
extern "C"  Vector2_t2243707579  InputController_get_dragVector_m2113092058 (InputController_t3951031284 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_right()
extern "C"  Vector3_t2243707580  Transform_get_right_m1162814389 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputController::get_tapOverUI()
extern "C"  bool InputController_get_tapOverUI_m3908338243 (InputController_t3951031284 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.EventSystem::get_current()
extern "C"  EventSystem_t3466835263 * EventSystem_get_current_m319019811 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.EventSystem::IsPointerOverGameObject()
extern "C"  bool EventSystem_IsPointerOverGameObject_m1128779390 (EventSystem_t3466835263 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C"  bool Input_GetMouseButtonDown_m2313448302 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputController::get_tapped()
extern "C"  bool InputController_get_tapped_m2392099722 (InputController_t3951031284 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Game::movePlayer()
extern "C"  void Game_movePlayer_m4079979315 (Game_t1600141214 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C"  bool Behaviour_get_enabled_m646668963 (Behaviour_t955675639 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 InputController::get_tapPosition()
extern "C"  Vector2_t2243707579  InputController_get_tapPosition_m2446245635 (InputController_t3951031284 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t2243707580  Input_get_mousePosition_m2069200279 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C"  Ray_t2469606224  Camera_ScreenPointToRay_m3033403101 (Camera_t189460977 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&)
extern "C"  bool Physics_Raycast_m44003754 (RuntimeObject * __this /* static, unused */, Ray_t2469606224  p0, RaycastHit_t87180320 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
extern "C"  Transform_t3275118058 * RaycastHit_get_transform_m2333455049 (RaycastHit_t87180320 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Component::get_tag()
extern "C"  String_t* Component_get_tag_m124558427 (Component_t3819376471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1790663636 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C"  Vector3_t2243707580  RaycastHit_get_normal_m391626824 (RaycastHit_t87180320 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t2243707580  Vector3_get_normalized_m1057036856 (Vector3_t2243707580 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Assets.scripts.Link> Pathfinder::findPath(Assets.scripts.Cell,Assets.scripts.Cell)
extern "C"  List_1_t145766874 * Pathfinder_findPath_m1726079702 (Pathfinder_t1592129589 * __this, Cell_t964604196 * ___start0, Cell_t964604196 * ___destination1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Application::get_isPlaying()
extern "C"  bool Application_get_isPlaying_m3003895139 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Menu::showDialog(System.String)
extern "C"  void Menu_showDialog_m540151737 (Menu_t4261767481 * __this, String_t* ___message0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Menu::hideDialog()
extern "C"  void Menu_hideDialog_m3443374652 (Menu_t4261767481 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void InputController::set_dragged(System.Boolean)
extern "C"  void InputController_set_dragged_m1073616225 (InputController_t3951031284 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void InputController::set_tapped(System.Boolean)
extern "C"  void InputController_set_tapped_m706833503 (InputController_t3951031284 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t2243707580  Vector3_get_zero_m3202043185 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t2243707579  Vector2_op_Implicit_m385881926 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void InputController::set_dragVector(UnityEngine.Vector2)
extern "C"  void InputController_set_dragVector_m2671669613 (InputController_t3951031284 * __this, Vector2_t2243707579  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Touch[] UnityEngine.Input::get_touches()
extern "C"  TouchU5BU5D_t3887265178* Input_get_touches_m793401621 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C"  int32_t Touch_get_phase_m972231807 (Touch_t407273883 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C"  Vector2_t2243707579  Touch_get_position_m261108426 (Touch_t407273883 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Touch::get_fingerId()
extern "C"  int32_t Touch_get_fingerId_m3527760158 (Touch_t407273883 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.EventSystem::IsPointerOverGameObject(System.Int32)
extern "C"  bool EventSystem_IsPointerOverGameObject_m2415000115 (EventSystem_t3466835263 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void InputController::set_tapOverUI(System.Boolean)
extern "C"  void InputController_set_tapOverUI_m3011029208 (InputController_t3951031284 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  Vector2_op_Subtraction_m1667221528 (RuntimeObject * __this /* static, unused */, Vector2_t2243707579  p0, Vector2_t2243707579  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_magnitude()
extern "C"  float Vector2_get_magnitude_m915202186 (Vector2_t2243707579 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void InputController::set_tapPosition(UnityEngine.Vector2)
extern "C"  void InputController_set_tapPosition_m483651298 (InputController_t3951031284 * __this, Vector2_t2243707579  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Cross(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_Cross_m693157500 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Multiply_m1869676276 (RuntimeObject * __this /* static, unused */, float p0, Vector3_t2243707580  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,System.Single)
extern "C"  void Transform_Rotate_m1159264663 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Level::createGrid()
extern "C"  void Level_createGrid_m485254149 (Level_t866853782 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
extern "C"  GameObjectU5BU5D_t3057952154* GameObject_FindGameObjectsWithTag_m3720927271 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C"  float Mathf_Min_m1552415280 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m2043094730 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Assets.scripts.Cell::.ctor(System.Int32,System.Int32,System.Int32,System.Boolean)
extern "C"  void Cell__ctor_m2554603460 (Cell_t964604196 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, bool ___isBlock3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t3275118058 * Transform_GetChild_m3136145191 (Transform_t3275118058 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Renderer>()
#define GameObject_GetComponentsInChildren_TisRenderer_t257310565_m2883698982(__this, method) ((  RendererU5BU5D_t2810717544* (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponentsInChildren_TisRuntimeObject_m1467294482_gshared)(__this, method)
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C"  void Renderer_set_enabled_m383916764 (Renderer_t257310565 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m196843760 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Subtraction_m4046047256 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m432505302 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t2330101084  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t4217747464* Object_FindObjectsOfType_m3335374525 (RuntimeObject * __this /* static, unused */, Type_t * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_op_Multiply_m2498445460 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<Trigger>()
#define Component_GetComponent_TisTrigger_t3844394560_m3031482907(__this, method) ((  Trigger_t3844394560 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// System.Int32 System.Array::GetLength(System.Int32)
extern "C"  int32_t Array_GetLength_m2083296647 (RuntimeArray * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Assets.scripts.Cell::get_isBlock()
extern "C"  bool Cell_get_isBlock_m3829309610 (Cell_t964604196 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Assets.scripts.Cell> Assets.scripts.Cell::get_blocks()
extern "C"  List_1_t333725328 * Cell_get_blocks_m153328201 (Cell_t964604196 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Assets.scripts.Cell>::Clear()
#define List_1_Clear_m3715491197(__this, method) ((  void (*) (List_1_t333725328 *, const RuntimeMethod*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Assets.scripts.Cell>::Add(!0)
#define List_1_Add_m1728370848(__this, p0, method) ((  void (*) (List_1_t333725328 *, Cell_t964604196 *, const RuntimeMethod*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Collections.Generic.List`1<Assets.scripts.Cell> Level::adjacentCells(Assets.scripts.Cell,System.Boolean)
extern "C"  List_1_t333725328 * Level_adjacentCells_m69322768 (Level_t866853782 * __this, Cell_t964604196 * ___cell0, bool ___direct1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Assets.scripts.Cell>::GetEnumerator()
#define List_1_GetEnumerator_m174512497(__this, method) ((  Enumerator_t4163422298  (*) (List_1_t333725328 *, const RuntimeMethod*))List_1_GetEnumerator_m2837081829_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<Assets.scripts.Cell>::get_Current()
#define Enumerator_get_Current_m4246516629(__this, method) ((  Cell_t964604196 * (*) (Enumerator_t4163422298 *, const RuntimeMethod*))Enumerator_get_Current_m2577424081_gshared)(__this, method)
// System.Void Level::createLink(Assets.scripts.Cell,Assets.scripts.Cell,System.Boolean)
extern "C"  void Level_createLink_m2520085500 (Level_t866853782 * __this, Cell_t964604196 * ___cell10, Cell_t964604196 * ___cell21, bool ___twofold2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<Assets.scripts.Cell>::MoveNext()
#define Enumerator_MoveNext_m1435586957(__this, method) ((  bool (*) (Enumerator_t4163422298 *, const RuntimeMethod*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Assets.scripts.Cell>::Dispose()
#define Enumerator_Dispose_m3632502891(__this, method) ((  void (*) (Enumerator_t4163422298 *, const RuntimeMethod*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// UnityEngine.Vector3 Assets.scripts.Cell::get_pos()
extern "C"  Vector3_t2243707580  Cell_get_pos_m113374293 (Cell_t964604196 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
extern "C"  float Vector3_get_sqrMagnitude_m1445517847 (Vector3_t2243707580 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_op_Division_m3670947479 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Assets.scripts.Link::.ctor()
extern "C"  void Link__ctor_m2788241046 (Link_t776645742 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_UnaryNegation_m2337886919 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioListener::get_volume()
extern "C"  float AudioListener_get_volume_m1738024862 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioListener::set_volume(System.Single)
extern "C"  void AudioListener_set_volume_m4197446127 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t356221433_m1217399699(__this, method) ((  Text_t356221433 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Button>()
#define GameObject_GetComponent_TisButton_t2872111280_m1008560876(__this, method) ((  Button_t2872111280 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::get_onClick()
extern "C"  ButtonClickedEvent_t2455055323 * Button_get_onClick_m1595880935 (Button_t2872111280 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction__ctor_m3825983746 (UnityAction_t4025899511 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
extern "C"  void UnityEvent_AddListener_m3787170584 (UnityEvent_t408735097 * __this, UnityAction_t4025899511 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m2693135142 (GameObject_t1756533147 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C"  String_t* String_Concat_m2000667605 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t2042527209_m2189462422(__this, method) ((  Image_t2042527209 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<ToggleButton>()
#define Component_GetComponent_TisToggleButton_t3926247120_m2729835643(__this, method) ((  ToggleButton_t3926247120 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
extern "C"  void Image_set_sprite_m1800056820 (Image_t2042527209 * __this, Sprite_t309593783 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Assets.scripts.Cell::get_f()
extern "C"  float Cell_get_f_m753931623 (Cell_t964604196 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1<Assets.scripts.Cell>::Remove(!0)
#define List_1_Remove_m1927217927(__this, p0, method) ((  bool (*) (List_1_t333725328 *, Cell_t964604196 *, const RuntimeMethod*))List_1_Remove_m3164383811_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1<Assets.scripts.Cell>::Contains(!0)
#define List_1_Contains_m2414020574(__this, p0, method) ((  bool (*) (List_1_t333725328 *, Cell_t964604196 *, const RuntimeMethod*))List_1_Contains_m1658838094_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<Assets.scripts.Cell>::get_Count()
#define List_1_get_Count_m349349022(__this, method) ((  int32_t (*) (List_1_t333725328 *, const RuntimeMethod*))List_1_get_Count_m2375293942_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Assets.scripts.Link>::Insert(System.Int32,!0)
#define List_1_Insert_m2431457653(__this, p0, p1, method) ((  void (*) (List_1_t145766874 *, int32_t, Link_t776645742 *, const RuntimeMethod*))List_1_Insert_m4101600027_gshared)(__this, p0, p1, method)
// UnityEngine.Vector3 PlatformTrigger::getMovement(System.Single)
extern "C"  Vector3_t2243707580  PlatformTrigger_getMovement_m3962613840 (PlatformTrigger_t3477208093 * __this, float ___distance0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Reverse()
#define List_1_Reverse_m723397084(__this, method) ((  void (*) (List_1_t1612828712 *, const RuntimeMethod*))List_1_Reverse_m723397084_gshared)(__this, method)
// System.Void Assets.scripts.Utility::roundPosition(UnityEngine.Transform[])
extern "C"  void Utility_roundPosition_m3988846968 (RuntimeObject * __this /* static, unused */, TransformU5BU5D_t3764228911* ___transforms0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::get_magnitude()
extern "C"  float Vector3_get_magnitude_m4021906415 (Vector3_t2243707580 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RevealTrigger::putMaterial(System.Boolean)
extern "C"  void RevealTrigger_putMaterial_m3028200551 (RevealTrigger_t3929941135 * __this, bool ___hidden0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.GameObject>::GetEnumerator()
#define List_1_GetEnumerator_m970439620(__this, method) ((  Enumerator_t660383953  (*) (List_1_t1125654279 *, const RuntimeMethod*))List_1_GetEnumerator_m2837081829_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::get_Current()
#define Enumerator_get_Current_m2389888842(__this, method) ((  GameObject_t1756533147 * (*) (Enumerator_t660383953 *, const RuntimeMethod*))Enumerator_get_Current_m2577424081_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t257310565_m1312615893(__this, method) ((  Renderer_t257310565 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
extern "C"  void Renderer_set_material_m3158565337 (Renderer_t257310565 * __this, Material_t193706927 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::MoveNext()
#define Enumerator_MoveNext_m3635305532(__this, method) ((  bool (*) (Enumerator_t660383953 *, const RuntimeMethod*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::Dispose()
#define Enumerator_Dispose_m1242097970(__this, method) ((  void (*) (Enumerator_t660383953 *, const RuntimeMethod*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)
extern "C"  AnimatorStateInfo_t2577870592  Animator_GetCurrentAnimatorStateInfo_m2354582050 (Animator_t69676727 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnimatorStateInfo::IsName(System.String)
extern "C"  bool AnimatorStateInfo_IsName_m1591389294 (AnimatorStateInfo_t2577870592 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::Play(System.String)
extern "C"  void Animator_Play_m577397764 (Animator_t69676727 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C"  Object_t1021602117 * Resources_Load_m4276439101 (RuntimeObject * __this /* static, unused */, String_t* p0, Type_t * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C"  Scene_t1684909666  SceneManager_GetActiveScene_m1722284185 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SceneManagement.Scene::get_name()
extern "C"  String_t* Scene_get_name_m3371331170 (Scene_t1684909666 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C"  void SceneManager_LoadScene_m2357316016 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Menu::finishLevel()
extern "C"  void Menu_finishLevel_m1671178345 (Menu_t4261767481 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ArrowTrigger::.ctor()
extern "C"  void ArrowTrigger__ctor_m3555100598 (ArrowTrigger_t1089668331 * __this, const RuntimeMethod* method)
{
	{
		Trigger__ctor_m1356284031(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ArrowTrigger::Start()
extern "C"  void ArrowTrigger_Start_m454924842 (ArrowTrigger_t1089668331 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrowTrigger_Start_m454924842_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t2243707580 * L_0 = __this->get_address_of_direction_3();
		Vector3_Normalize_m2606725557(L_0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1594932300, /*hidden argument*/NULL);
		Level_t866853782 * L_2 = GameObject_GetComponent_TisLevel_t866853782_m3318339701(L_1, /*hidden argument*/GameObject_GetComponent_TisLevel_t866853782_m3318339701_RuntimeMethod_var);
		__this->set_level_4(L_2);
		return;
	}
}
// System.Void ArrowTrigger::enter(System.Boolean,Character)
extern "C"  void ArrowTrigger_enter_m3090982584 (ArrowTrigger_t1089668331 * __this, bool ___isDestination0, Character_t3726027591 * ___character1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrowTrigger_enter_m3090982584_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Cell_t964604196 * V_0 = NULL;
	Link_t776645742 * V_1 = NULL;
	Enumerator_t3975463844  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = ___isDestination0;
		if (!L_0)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m2923680153(NULL /*static, unused*/, _stringLiteral1407338552, /*hidden argument*/NULL);
		Level_t866853782 * L_1 = __this->get_level_4();
		Character_t3726027591 * L_2 = __this->get_target_2();
		Transform_t3275118058 * L_3 = Component_get_transform_m3374354972(L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Transform_get_position_m2304215762(L_3, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = __this->get_direction_3();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_6 = Vector3_op_Addition_m394909128(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Cell_t964604196 * L_7 = Level_cellAtPosition_m891825944(L_1, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Cell_t964604196 * L_8 = V_0;
		if (!L_8)
		{
			goto IL_00b1;
		}
	}
	{
		Character_t3726027591 * L_9 = __this->get_target_2();
		Cell_t964604196 * L_10 = L_9->get_currentCell_2();
		List_1_t145766874 * L_11 = Cell_get_links_m2033144484(L_10, /*hidden argument*/NULL);
		Enumerator_t3975463844  L_12 = List_1_GetEnumerator_m3554822035(L_11, /*hidden argument*/List_1_GetEnumerator_m3554822035_RuntimeMethod_var);
		V_2 = L_12;
	}

IL_0053:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0092;
		}

IL_0058:
		{
			Link_t776645742 * L_13 = Enumerator_get_Current_m1037497887((&V_2), /*hidden argument*/Enumerator_get_Current_m1037497887_RuntimeMethod_var);
			V_1 = L_13;
			Link_t776645742 * L_14 = V_1;
			Cell_t964604196 * L_15 = L_14->get_second_4();
			Cell_t964604196 * L_16 = V_0;
			if ((!(((RuntimeObject*)(Cell_t964604196 *)L_15) == ((RuntimeObject*)(Cell_t964604196 *)L_16))))
			{
				goto IL_0092;
			}
		}

IL_006c:
		{
			Character_t3726027591 * L_17 = __this->get_target_2();
			List_1_t145766874 * L_18 = L_17->get_path_3();
			List_1_Clear_m1988879339(L_18, /*hidden argument*/List_1_Clear_m1988879339_RuntimeMethod_var);
			Character_t3726027591 * L_19 = __this->get_target_2();
			List_1_t145766874 * L_20 = L_19->get_path_3();
			Link_t776645742 * L_21 = V_1;
			List_1_Add_m257155336(L_20, L_21, /*hidden argument*/List_1_Add_m257155336_RuntimeMethod_var);
			goto IL_009e;
		}

IL_0092:
		{
			bool L_22 = Enumerator_MoveNext_m218825171((&V_2), /*hidden argument*/Enumerator_MoveNext_m218825171_RuntimeMethod_var);
			if (L_22)
			{
				goto IL_0058;
			}
		}

IL_009e:
		{
			IL2CPP_LEAVE(0xB1, FINALLY_00a3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00a3;
	}

FINALLY_00a3:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2728416437((&V_2), /*hidden argument*/Enumerator_Dispose_m2728416437_RuntimeMethod_var);
		IL2CPP_END_FINALLY(163)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(163)
	{
		IL2CPP_JUMP_TBL(0xB1, IL_00b1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b1:
	{
		return;
	}
}
// System.Void Assets.scripts.Cell::.ctor(System.Int32,System.Int32,System.Int32,System.Boolean)
extern "C"  void Cell__ctor_m2554603460 (Cell_t964604196 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, bool ___isBlock3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cell__ctor_m2554603460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		List_1_t1612828712 * L_0 = (List_1_t1612828712 *)il2cpp_codegen_object_new(List_1_t1612828712_il2cpp_TypeInfo_var);
		List_1__ctor_m347461442(L_0, /*hidden argument*/List_1__ctor_m347461442_RuntimeMethod_var);
		Cell_set_midPoints_m1417671087(__this, L_0, /*hidden argument*/NULL);
		List_1_t333725328 * L_1 = (List_1_t333725328 *)il2cpp_codegen_object_new(List_1_t333725328_il2cpp_TypeInfo_var);
		List_1__ctor_m2909086300(L_1, /*hidden argument*/List_1__ctor_m2909086300_RuntimeMethod_var);
		Cell_set_blocks_m1582678640(__this, L_1, /*hidden argument*/NULL);
		List_1_t145766874 * L_2 = (List_1_t145766874 *)il2cpp_codegen_object_new(List_1_t145766874_il2cpp_TypeInfo_var);
		List_1__ctor_m1908755428(L_2, /*hidden argument*/List_1__ctor_m1908755428_RuntimeMethod_var);
		Cell_set_links_m2795728617(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___x0;
		Cell_set_x_m2919876108(__this, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___y1;
		Cell_set_y_m1423634631(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = ___z2;
		Cell_set_z_m883112214(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = ___x0;
		int32_t L_7 = ___y1;
		int32_t L_8 = ___z2;
		Vector3_t2243707580  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m1555724485((&L_9), (((float)((float)L_6))), (((float)((float)L_7))), (((float)((float)L_8))), /*hidden argument*/NULL);
		Cell_set_pos_m1164249580(__this, L_9, /*hidden argument*/NULL);
		bool L_10 = ___isBlock3;
		Cell_set_isBlock_m2660126527(__this, L_10, /*hidden argument*/NULL);
		Cell_calculateMidpoints_m4202269553(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.Vector3> Assets.scripts.Cell::get_midPoints()
extern "C"  List_1_t1612828712 * Cell_get_midPoints_m2313462662 (Cell_t964604196 * __this, const RuntimeMethod* method)
{
	{
		List_1_t1612828712 * L_0 = __this->get_U3CmidPointsU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void Assets.scripts.Cell::set_midPoints(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void Cell_set_midPoints_m1417671087 (Cell_t964604196 * __this, List_1_t1612828712 * ___value0, const RuntimeMethod* method)
{
	{
		List_1_t1612828712 * L_0 = ___value0;
		__this->set_U3CmidPointsU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<Assets.scripts.Cell> Assets.scripts.Cell::get_blocks()
extern "C"  List_1_t333725328 * Cell_get_blocks_m153328201 (Cell_t964604196 * __this, const RuntimeMethod* method)
{
	{
		List_1_t333725328 * L_0 = __this->get_U3CblocksU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void Assets.scripts.Cell::set_blocks(System.Collections.Generic.List`1<Assets.scripts.Cell>)
extern "C"  void Cell_set_blocks_m1582678640 (Cell_t964604196 * __this, List_1_t333725328 * ___value0, const RuntimeMethod* method)
{
	{
		List_1_t333725328 * L_0 = ___value0;
		__this->set_U3CblocksU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<Assets.scripts.Link> Assets.scripts.Cell::get_links()
extern "C"  List_1_t145766874 * Cell_get_links_m2033144484 (Cell_t964604196 * __this, const RuntimeMethod* method)
{
	{
		List_1_t145766874 * L_0 = __this->get_U3ClinksU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void Assets.scripts.Cell::set_links(System.Collections.Generic.List`1<Assets.scripts.Link>)
extern "C"  void Cell_set_links_m2795728617 (Cell_t964604196 * __this, List_1_t145766874 * ___value0, const RuntimeMethod* method)
{
	{
		List_1_t145766874 * L_0 = ___value0;
		__this->set_U3ClinksU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Int32 Assets.scripts.Cell::get_x()
extern "C"  int32_t Cell_get_x_m2406521465 (Cell_t964604196 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CxU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void Assets.scripts.Cell::set_x(System.Int32)
extern "C"  void Cell_set_x_m2919876108 (Cell_t964604196 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CxU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Int32 Assets.scripts.Cell::get_y()
extern "C"  int32_t Cell_get_y_m2406521560 (Cell_t964604196 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CyU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void Assets.scripts.Cell::set_y(System.Int32)
extern "C"  void Cell_set_y_m1423634631 (Cell_t964604196 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CyU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Int32 Assets.scripts.Cell::get_z()
extern "C"  int32_t Cell_get_z_m2406521399 (Cell_t964604196 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CzU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void Assets.scripts.Cell::set_z(System.Int32)
extern "C"  void Cell_set_z_m883112214 (Cell_t964604196 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CzU3Ek__BackingField_5(L_0);
		return;
	}
}
// UnityEngine.Vector3 Assets.scripts.Cell::get_pos()
extern "C"  Vector3_t2243707580  Cell_get_pos_m113374293 (Cell_t964604196 * __this, const RuntimeMethod* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_U3CposU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void Assets.scripts.Cell::set_pos(UnityEngine.Vector3)
extern "C"  void Cell_set_pos_m1164249580 (Cell_t964604196 * __this, Vector3_t2243707580  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_t2243707580  L_0 = ___value0;
		__this->set_U3CposU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Boolean Assets.scripts.Cell::get_isBlock()
extern "C"  bool Cell_get_isBlock_m3829309610 (Cell_t964604196 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CisBlockU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void Assets.scripts.Cell::set_isBlock(System.Boolean)
extern "C"  void Cell_set_isBlock_m2660126527 (Cell_t964604196 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CisBlockU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Single Assets.scripts.Cell::get_f()
extern "C"  float Cell_get_f_m753931623 (Cell_t964604196 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_g_9();
		float L_1 = __this->get_h_10();
		return ((float)((float)L_0+(float)L_1));
	}
}
// System.Void Assets.scripts.Cell::calculateMidpoints()
extern "C"  void Cell_calculateMidpoints_m4202269553 (Cell_t964604196 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Cell_calculateMidpoints_m4202269553_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = (-1);
		goto IL_00a5;
	}

IL_0007:
	{
		V_1 = (-1);
		goto IL_009a;
	}

IL_000e:
	{
		V_2 = (-1);
		goto IL_008f;
	}

IL_0015:
	{
		int32_t L_0 = V_0;
		if (L_0)
		{
			goto IL_0027;
		}
	}
	{
		int32_t L_1 = V_1;
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		int32_t L_2 = V_2;
		if (!L_2)
		{
			goto IL_008b;
		}
	}

IL_0027:
	{
		int32_t L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_Abs_m722236531(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_004b;
		}
	}
	{
		int32_t L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_6 = Mathf_Abs_m722236531(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_004b;
		}
	}
	{
		int32_t L_7 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_8 = Mathf_Abs_m722236531(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)1)))
		{
			goto IL_008b;
		}
	}

IL_004b:
	{
		List_1_t1612828712 * L_9 = Cell_get_midPoints_m2313462662(__this, /*hidden argument*/NULL);
		int32_t L_10 = Cell_get_x_m2406521465(__this, /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		int32_t L_12 = Cell_get_y_m2406521560(__this, /*hidden argument*/NULL);
		int32_t L_13 = V_1;
		int32_t L_14 = Cell_get_z_m2406521399(__this, /*hidden argument*/NULL);
		int32_t L_15 = V_2;
		Vector3_t2243707580  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m1555724485((&L_16), ((float)((float)(((float)((float)L_10)))+(float)((float)((float)(((float)((float)L_11)))*(float)(0.5f))))), ((float)((float)(((float)((float)L_12)))+(float)((float)((float)(((float)((float)L_13)))*(float)(0.5f))))), ((float)((float)(((float)((float)L_14)))+(float)((float)((float)(((float)((float)L_15)))*(float)(0.5f))))), /*hidden argument*/NULL);
		List_1_Add_m2338641291(L_9, L_16, /*hidden argument*/List_1_Add_m2338641291_RuntimeMethod_var);
	}

IL_008b:
	{
		int32_t L_17 = V_2;
		V_2 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_008f:
	{
		int32_t L_18 = V_2;
		if ((((int32_t)L_18) <= ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_19 = V_1;
		V_1 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_20 = V_1;
		if ((((int32_t)L_20) <= ((int32_t)1)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_21 = V_0;
		V_0 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_22 = V_0;
		if ((((int32_t)L_22) <= ((int32_t)1)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Assets.scripts.Link::.ctor()
extern "C"  void Link__ctor_m2788241046 (Link_t776645742 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Assets.scripts.Utility::.ctor()
extern "C"  void Utility__ctor_m32741212 (Utility_t2627328774 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Assets.scripts.Utility::roundPosition(UnityEngine.Transform[])
extern "C"  void Utility_roundPosition_m3988846968 (RuntimeObject * __this /* static, unused */, TransformU5BU5D_t3764228911* ___transforms0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Utility_roundPosition_m3988846968_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	TransformU5BU5D_t3764228911* V_1 = NULL;
	int32_t V_2 = 0;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		TransformU5BU5D_t3764228911* L_0 = ___transforms0;
		V_1 = L_0;
		V_2 = 0;
		goto IL_0051;
	}

IL_0009:
	{
		TransformU5BU5D_t3764228911* L_1 = V_1;
		int32_t L_2 = V_2;
		int32_t L_3 = L_2;
		Transform_t3275118058 * L_4 = (L_1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		Transform_t3275118058 * L_5 = V_0;
		Transform_t3275118058 * L_6 = Component_get_transform_m3374354972(L_5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = Transform_get_position_m2304215762(L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		Transform_t3275118058 * L_8 = V_0;
		Transform_t3275118058 * L_9 = Component_get_transform_m3374354972(L_8, /*hidden argument*/NULL);
		float L_10 = (&V_3)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_11 = bankers_roundf(L_10);
		float L_12 = (&V_3)->get_y_2();
		float L_13 = bankers_roundf(L_12);
		float L_14 = (&V_3)->get_z_3();
		float L_15 = bankers_roundf(L_14);
		Vector3_t2243707580  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m1555724485((&L_16), L_11, L_13, L_15, /*hidden argument*/NULL);
		Transform_set_position_m2942701431(L_9, L_16, /*hidden argument*/NULL);
		int32_t L_17 = V_2;
		V_2 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0051:
	{
		int32_t L_18 = V_2;
		TransformU5BU5D_t3764228911* L_19 = V_1;
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_19)->max_length)))))))
		{
			goto IL_0009;
		}
	}
	{
		return;
	}
}
// System.Void Character::.ctor()
extern "C"  void Character__ctor_m1610743918 (Character_t3726027591 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Character__ctor_m1610743918_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t145766874 * L_0 = (List_1_t145766874 *)il2cpp_codegen_object_new(List_1_t145766874_il2cpp_TypeInfo_var);
		List_1__ctor_m1908755428(L_0, /*hidden argument*/List_1__ctor_m1908755428_RuntimeMethod_var);
		__this->set_path_3(L_0);
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Character::get_isPlayer()
extern "C"  bool Character_get_isPlayer_m2500959794 (Character_t3726027591 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Character_get_isPlayer_m2500959794_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Character::Start()
extern "C"  void Character_Start_m2430444898 (Character_t3726027591 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Character_Start_m2430444898_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1594932300, /*hidden argument*/NULL);
		Level_t866853782 * L_1 = GameObject_GetComponent_TisLevel_t866853782_m3318339701(L_0, /*hidden argument*/GameObject_GetComponent_TisLevel_t866853782_m3318339701_RuntimeMethod_var);
		__this->set_level_4(L_1);
		GameObject_t1756533147 * L_2 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1594932300, /*hidden argument*/NULL);
		Game_t1600141214 * L_3 = GameObject_GetComponent_TisGame_t1600141214_m3908916829(L_2, /*hidden argument*/GameObject_GetComponent_TisGame_t1600141214_m3908916829_RuntimeMethod_var);
		__this->set_game_5(L_3);
		Level_t866853782 * L_4 = __this->get_level_4();
		CellU5BU2CU2CU5D_t1218597327* L_5 = L_4->get_cells_2();
		Transform_t3275118058 * L_6 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = Transform_get_position_m2304215762(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		float L_8 = (&V_0)->get_x_1();
		Transform_t3275118058 * L_9 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_10 = Transform_get_position_m2304215762(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		float L_11 = (&V_1)->get_y_2();
		Transform_t3275118058 * L_12 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_13 = Transform_get_position_m2304215762(L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		float L_14 = (&V_2)->get_z_3();
		Cell_t964604196 * L_15 = ((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_5)->GetAtUnchecked((((int32_t)((int32_t)L_8))), (((int32_t)((int32_t)L_11))), (((int32_t)((int32_t)L_14))));
		__this->set_currentCell_2(L_15);
		Transform_t3275118058 * L_16 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_17 = Transform_get_rotation_m2617026815(L_16, /*hidden argument*/NULL);
		__this->set_initialRotation_10(L_17);
		return;
	}
}
// System.Void Character::Update()
extern "C"  void Character_Update_m4054934635 (Character_t3726027591 * __this, const RuntimeMethod* method)
{
	{
		Character_moveCharacter_m4256772666(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Character::moveCharacter()
extern "C"  void Character_moveCharacter_m4256772666 (Character_t3726027591 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Character_moveCharacter_m4256772666_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Link_t776645742 * L_0 = __this->get_currentLink_6();
		if (L_0)
		{
			goto IL_00a8;
		}
	}
	{
		List_1_t145766874 * L_1 = __this->get_path_3();
		int32_t L_2 = List_1_get_Count_m3786137094(L_1, /*hidden argument*/List_1_get_Count_m3786137094_RuntimeMethod_var);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_00a8;
		}
	}
	{
		Cell_t964604196 * L_3 = __this->get_currentCell_2();
		Trigger_t3844394560 * L_4 = L_3->get_trigger_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0049;
		}
	}
	{
		Cell_t964604196 * L_6 = __this->get_currentCell_2();
		Trigger_t3844394560 * L_7 = L_6->get_trigger_11();
		bool L_8 = __this->get_stopped_11();
		VirtActionInvoker2< bool, Character_t3726027591 * >::Invoke(5 /* System.Void Trigger::exit(System.Boolean,Character) */, L_7, L_8, __this);
	}

IL_0049:
	{
		List_1_t145766874 * L_9 = __this->get_path_3();
		Link_t776645742 * L_10 = List_1_get_Item_m914633333(L_9, 0, /*hidden argument*/List_1_get_Item_m914633333_RuntimeMethod_var);
		__this->set_currentLink_6(L_10);
		List_1_t145766874 * L_11 = __this->get_path_3();
		List_1_RemoveAt_m2271034946(L_11, 0, /*hidden argument*/List_1_RemoveAt_m2271034946_RuntimeMethod_var);
		Link_t776645742 * L_12 = __this->get_currentLink_6();
		Cell_t964604196 * L_13 = L_12->get_second_4();
		__this->set_currentCell_2(L_13);
		__this->set_rotation_7((0.0f));
		Transform_t3275118058 * L_14 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_15 = Transform_get_position_m2304215762(L_14, /*hidden argument*/NULL);
		__this->set_startPos_9(L_15);
		List_1_t145766874 * L_16 = __this->get_path_3();
		int32_t L_17 = List_1_get_Count_m3786137094(L_16, /*hidden argument*/List_1_get_Count_m3786137094_RuntimeMethod_var);
		__this->set_stopped_11((bool)((((int32_t)L_17) == ((int32_t)0))? 1 : 0));
	}

IL_00a8:
	{
		Link_t776645742 * L_18 = __this->get_currentLink_6();
		if (!L_18)
		{
			goto IL_01ac;
		}
	}
	{
		float L_19 = __this->get_rotation_7();
		float L_20 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_rotation_7(((float)((float)L_19+(float)((float)((float)(200.0f)*(float)L_20)))));
		float L_21 = __this->get_rotation_7();
		Link_t776645742 * L_22 = __this->get_currentLink_6();
		float L_23 = L_22->get_angle_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_24 = Mathf_Clamp_m1779415170(NULL /*static, unused*/, L_21, (0.0f), L_23, /*hidden argument*/NULL);
		__this->set_rotation_7(L_24);
		Transform_t3275118058 * L_25 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_26 = __this->get_startPos_9();
		Transform_set_position_m2942701431(L_25, L_26, /*hidden argument*/NULL);
		Transform_t3275118058 * L_27 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_28 = __this->get_initialRotation_10();
		Transform_set_rotation_m2824446320(L_27, L_28, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Link_t776645742 * L_30 = __this->get_currentLink_6();
		Vector3_t2243707580  L_31 = L_30->get_pivot_1();
		Link_t776645742 * L_32 = __this->get_currentLink_6();
		Vector3_t2243707580  L_33 = L_32->get_axis_0();
		float L_34 = __this->get_rotation_7();
		Transform_RotateAround_m132750417(L_29, L_31, L_33, L_34, /*hidden argument*/NULL);
		float L_35 = __this->get_rotation_7();
		Link_t776645742 * L_36 = __this->get_currentLink_6();
		float L_37 = L_36->get_angle_2();
		if ((!(((float)L_35) >= ((float)L_37))))
		{
			goto IL_01ac;
		}
	}
	{
		__this->set_currentLink_6((Link_t776645742 *)NULL);
		Transform_t3275118058 * L_38 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_39 = __this->get_initialRotation_10();
		Transform_set_rotation_m2824446320(L_38, L_39, /*hidden argument*/NULL);
		Cell_t964604196 * L_40 = __this->get_currentCell_2();
		Trigger_t3844394560 * L_41 = L_40->get_trigger_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_42 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_41, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0198;
		}
	}
	{
		Cell_t964604196 * L_43 = __this->get_currentCell_2();
		Trigger_t3844394560 * L_44 = L_43->get_trigger_11();
		List_1_t145766874 * L_45 = __this->get_path_3();
		int32_t L_46 = List_1_get_Count_m3786137094(L_45, /*hidden argument*/List_1_get_Count_m3786137094_RuntimeMethod_var);
		VirtActionInvoker2< bool, Character_t3726027591 * >::Invoke(4 /* System.Void Trigger::enter(System.Boolean,Character) */, L_44, (bool)((((int32_t)L_46) == ((int32_t)0))? 1 : 0), __this);
	}

IL_0198:
	{
		List_1_t145766874 * L_47 = __this->get_path_3();
		int32_t L_48 = List_1_get_Count_m3786137094(L_47, /*hidden argument*/List_1_get_Count_m3786137094_RuntimeMethod_var);
		Character_reachedCell_m2549832251(__this, (bool)((((int32_t)L_48) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_01ac:
	{
		return;
	}
}
// System.Void Character::reachedCell(System.Boolean)
extern "C"  void Character_reachedCell_m2549832251 (Character_t3726027591 * __this, bool ___isDestination0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Character_reachedCell_m2549832251_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Cell_t964604196 * V_0 = NULL;
	{
		bool L_0 = Character_get_isPlayer_m2500959794(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0057;
		}
	}
	{
		Level_t866853782 * L_1 = __this->get_level_4();
		Transform_t3275118058 * L_2 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Transform_get_position_m2304215762(L_2, /*hidden argument*/NULL);
		Cell_t964604196 * L_4 = Level_cellAtPosition_m891825944(L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Cell_t964604196 * L_5 = V_0;
		GameObject_t1756533147 * L_6 = L_5->get_fragment_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0057;
		}
	}
	{
		Cell_t964604196 * L_8 = V_0;
		GameObject_t1756533147 * L_9 = L_8->get_fragment_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m3959286051(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Cell_t964604196 * L_10 = V_0;
		L_10->set_fragment_13((GameObject_t1756533147 *)NULL);
		Game_t1600141214 * L_11 = __this->get_game_5();
		Game_t1600141214 * L_12 = L_11;
		int32_t L_13 = Game_get_fragments_m2335769477(L_12, /*hidden argument*/NULL);
		Game_set_fragments_m4122492370(L_12, ((int32_t)((int32_t)L_13+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void CreateTrigger::.ctor()
extern "C"  void CreateTrigger__ctor_m360915323 (CreateTrigger_t2237538136 * __this, const RuntimeMethod* method)
{
	{
		Trigger__ctor_m1356284031(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CreateTrigger::Start()
extern "C"  void CreateTrigger_Start_m1083303839 (CreateTrigger_t2237538136 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CreateTrigger_Start_m1083303839_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1594932300, /*hidden argument*/NULL);
		Level_t866853782 * L_1 = GameObject_GetComponent_TisLevel_t866853782_m3318339701(L_0, /*hidden argument*/GameObject_GetComponent_TisLevel_t866853782_m3318339701_RuntimeMethod_var);
		__this->set_level_3(L_1);
		return;
	}
}
// System.Void CreateTrigger::enter(System.Boolean,Character)
extern "C"  void CreateTrigger_enter_m956692605 (CreateTrigger_t2237538136 * __this, bool ___isDestination0, Character_t3726027591 * ___character1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CreateTrigger_enter_m956692605_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t1147558386  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	GameObject_t1756533147 * V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = ___isDestination0;
		if (!L_0)
		{
			goto IL_009f;
		}
	}
	{
		bool L_1 = __this->get_created_4();
		if (L_1)
		{
			goto IL_009f;
		}
	}
	{
		__this->set_created_4((bool)1);
		List_1_t1612828712 * L_2 = __this->get_relPositions_2();
		Enumerator_t1147558386  L_3 = List_1_GetEnumerator_m940839541(L_2, /*hidden argument*/List_1_GetEnumerator_m940839541_RuntimeMethod_var);
		V_1 = L_3;
	}

IL_0024:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0080;
		}

IL_0029:
		{
			Vector3_t2243707580  L_4 = Enumerator_get_Current_m857008049((&V_1), /*hidden argument*/Enumerator_get_Current_m857008049_RuntimeMethod_var);
			V_0 = L_4;
			Transform_t3275118058 * L_5 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
			Transform_t3275118058 * L_6 = Transform_get_parent_m2752514051(L_5, /*hidden argument*/NULL);
			Vector3_t2243707580  L_7 = Transform_get_position_m2304215762(L_6, /*hidden argument*/NULL);
			Vector3_t2243707580  L_8 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
			Vector3_t2243707580  L_9 = Vector3_op_Addition_m394909128(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
			V_2 = L_9;
			Object_t1021602117 * L_10 = Resources_Load_m3480536692(NULL /*static, unused*/, _stringLiteral3665939501, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			Object_t1021602117 * L_11 = Object_Instantiate_m698636442(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
			V_3 = ((GameObject_t1756533147 *)CastclassSealed((RuntimeObject*)L_11, GameObject_t1756533147_il2cpp_TypeInfo_var));
			GameObject_t1756533147 * L_12 = V_3;
			GameObject_set_tag_m2565837656(L_12, _stringLiteral3745862197, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_13 = V_3;
			Transform_t3275118058 * L_14 = GameObject_get_transform_m3490276752(L_13, /*hidden argument*/NULL);
			Vector3_t2243707580  L_15 = V_2;
			Transform_set_position_m2942701431(L_14, L_15, /*hidden argument*/NULL);
			Level_t866853782 * L_16 = __this->get_level_3();
			Vector3_t2243707580  L_17 = V_2;
			Level_addBlock_m1332986222(L_16, L_17, /*hidden argument*/NULL);
		}

IL_0080:
		{
			bool L_18 = Enumerator_MoveNext_m2007266881((&V_1), /*hidden argument*/Enumerator_MoveNext_m2007266881_RuntimeMethod_var);
			if (L_18)
			{
				goto IL_0029;
			}
		}

IL_008c:
		{
			IL2CPP_LEAVE(0x9F, FINALLY_0091);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0091;
	}

FINALLY_0091:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3215924523((&V_1), /*hidden argument*/Enumerator_Dispose_m3215924523_RuntimeMethod_var);
		IL2CPP_END_FINALLY(145)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(145)
	{
		IL2CPP_JUMP_TBL(0x9F, IL_009f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_009f:
	{
		return;
	}
}
// System.Void CreateTrigger::exit(System.Boolean,Character)
extern "C"  void CreateTrigger_exit_m1189308469 (CreateTrigger_t2237538136 * __this, bool ___isDestination0, Character_t3726027591 * ___character1, const RuntimeMethod* method)
{
	{
		bool L_0 = ___isDestination0;
		if (!L_0)
		{
			goto IL_0006;
		}
	}

IL_0006:
	{
		return;
	}
}
// System.Void ExplosionTrigger::.ctor()
extern "C"  void ExplosionTrigger__ctor_m2313249416 (ExplosionTrigger_t4191861867 * __this, const RuntimeMethod* method)
{
	{
		Trigger__ctor_m1356284031(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExplosionTrigger::Start()
extern "C"  void ExplosionTrigger_Start_m1958706572 (ExplosionTrigger_t4191861867 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExplosionTrigger_Start_m1958706572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1594932300, /*hidden argument*/NULL);
		Level_t866853782 * L_1 = GameObject_GetComponent_TisLevel_t866853782_m3318339701(L_0, /*hidden argument*/GameObject_GetComponent_TisLevel_t866853782_m3318339701_RuntimeMethod_var);
		__this->set_level_3(L_1);
		return;
	}
}
// System.Void ExplosionTrigger::enter(System.Boolean,Character)
extern "C"  void ExplosionTrigger_enter_m4019835266 (ExplosionTrigger_t4191861867 * __this, bool ___isDestination0, Character_t3726027591 * ___character1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ExplosionTrigger_enter_m4019835266_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Cell_t964604196 * V_0 = NULL;
	{
		bool L_0 = __this->get_destroyed_4();
		if (L_0)
		{
			goto IL_0040;
		}
	}
	{
		Level_t866853782 * L_1 = __this->get_level_3();
		GameObject_t1756533147 * L_2 = __this->get_target_2();
		Transform_t3275118058 * L_3 = GameObject_get_transform_m3490276752(L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Transform_get_position_m2304215762(L_3, /*hidden argument*/NULL);
		Cell_t964604196 * L_5 = Level_cellAtPosition_m891825944(L_1, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Cell_t964604196 * L_6 = V_0;
		L_6->set_isObstacle_12((bool)0);
		GameObject_t1756533147 * L_7 = __this->get_target_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m3959286051(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		__this->set_destroyed_4((bool)1);
	}

IL_0040:
	{
		return;
	}
}
// System.Void Game::.ctor()
extern "C"  void Game__ctor_m1512360653 (Game_t1600141214 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Game::get_fragments()
extern "C"  int32_t Game_get_fragments_m2335769477 (Game_t1600141214 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_numFragments_13();
		return L_0;
	}
}
// System.Void Game::set_fragments(System.Int32)
extern "C"  void Game_set_fragments_m4122492370 (Game_t1600141214 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_numFragments_13(L_0);
		Menu_t4261767481 * L_1 = __this->get_menu_5();
		Menu_updateFragments_m871188344(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Game::Start()
extern "C"  void Game_Start_m3679318685 (Game_t1600141214 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_Start_m3679318685_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Level_t866853782 * L_0 = Component_GetComponent_TisLevel_t866853782_m4130163129(__this, /*hidden argument*/Component_GetComponent_TisLevel_t866853782_m4130163129_RuntimeMethod_var);
		__this->set_level_2(L_0);
		Pathfinder_t1592129589 * L_1 = Component_GetComponent_TisPathfinder_t1592129589_m1369771324(__this, /*hidden argument*/Component_GetComponent_TisPathfinder_t1592129589_m1369771324_RuntimeMethod_var);
		__this->set_pathfinder_3(L_1);
		GameObject_t1756533147 * L_2 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		Character_t3726027591 * L_3 = GameObject_GetComponent_TisCharacter_t3726027591_m2667239018(L_2, /*hidden argument*/GameObject_GetComponent_TisCharacter_t3726027591_m2667239018_RuntimeMethod_var);
		__this->set_player_4(L_3);
		GameObject_t1756533147 * L_4 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1047780110, /*hidden argument*/NULL);
		Menu_t4261767481 * L_5 = GameObject_GetComponent_TisMenu_t4261767481_m3170984502(L_4, /*hidden argument*/GameObject_GetComponent_TisMenu_t4261767481_m3170984502_RuntimeMethod_var);
		__this->set_menu_5(L_5);
		InputController_t3951031284 * L_6 = Component_GetComponent_TisInputController_t3951031284_m1126083993(__this, /*hidden argument*/Component_GetComponent_TisInputController_t3951031284_m1126083993_RuntimeMethod_var);
		__this->set_inputController_6(L_6);
		int32_t L_7 = Application_get_platform_m1727035749(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_android_14((bool)((((int32_t)L_7) == ((int32_t)((int32_t)11)))? 1 : 0));
		Camera_t189460977 * L_8 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_9 = Component_get_transform_m3374354972(L_8, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_10 = Transform_get_rotation_m2617026815(L_9, /*hidden argument*/NULL);
		__this->set_camRot_12(L_10);
		Camera_t189460977 * L_11 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_12 = Component_get_transform_m3374354972(L_11, /*hidden argument*/NULL);
		Character_t3726027591 * L_13 = __this->get_player_4();
		Transform_t3275118058 * L_14 = Component_get_transform_m3374354972(L_13, /*hidden argument*/NULL);
		Vector3_t2243707580  L_15 = Transform_get_position_m2304215762(L_14, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m1555724485((&L_16), (0.0f), (0.0f), (-12.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_17 = Vector3_op_Addition_m394909128(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		Transform_set_position_m2942701431(L_12, L_17, /*hidden argument*/NULL);
		Camera_t189460977 * L_18 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_19 = Component_get_transform_m3374354972(L_18, /*hidden argument*/NULL);
		Character_t3726027591 * L_20 = __this->get_player_4();
		Transform_t3275118058 * L_21 = Component_get_transform_m3374354972(L_20, /*hidden argument*/NULL);
		Vector3_t2243707580  L_22 = Transform_get_position_m2304215762(L_21, /*hidden argument*/NULL);
		Transform_LookAt_m1970949065(L_19, L_22, /*hidden argument*/NULL);
		Camera_t189460977 * L_23 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		Animator_t69676727 * L_24 = Component_GetComponent_TisAnimator_t69676727_m475627522(L_23, /*hidden argument*/Component_GetComponent_TisAnimator_t69676727_m475627522_RuntimeMethod_var);
		Behaviour_set_enabled_m602406666(L_24, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Game::Update()
extern "C"  void Game_Update_m3455215516 (Game_t1600141214 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_Update_m3455215516_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	bool V_3 = false;
	bool G_B13_0 = false;
	{
		float L_0 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0*(float)(120.0f)));
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKey_m4207472870(NULL /*static, unused*/, ((int32_t)97), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		float L_2 = __this->get_camHoriz_10();
		float L_3 = V_0;
		__this->set_camHoriz_10(((float)((float)L_2+(float)L_3)));
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_4 = Input_GetKey_m4207472870(NULL /*static, unused*/, ((int32_t)100), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		float L_5 = __this->get_camHoriz_10();
		float L_6 = V_0;
		__this->set_camHoriz_10(((float)((float)L_5-(float)L_6)));
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_7 = Input_GetKey_m4207472870(NULL /*static, unused*/, ((int32_t)119), /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005a;
		}
	}
	{
		float L_8 = __this->get_camVert_11();
		float L_9 = V_0;
		__this->set_camVert_11(((float)((float)L_8+(float)L_9)));
	}

IL_005a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_10 = Input_GetKey_m4207472870(NULL /*static, unused*/, ((int32_t)115), /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0074;
		}
	}
	{
		float L_11 = __this->get_camVert_11();
		float L_12 = V_0;
		__this->set_camVert_11(((float)((float)L_11-(float)L_12)));
	}

IL_0074:
	{
		InputController_t3951031284 * L_13 = __this->get_inputController_6();
		bool L_14 = InputController_get_dragged_m1166786620(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00d0;
		}
	}
	{
		float L_15 = __this->get_camHoriz_10();
		InputController_t3951031284 * L_16 = __this->get_inputController_6();
		Vector2_t2243707579  L_17 = InputController_get_dragVector_m2113092058(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		float L_18 = (&V_1)->get_x_0();
		__this->set_camHoriz_10(((float)((float)L_15+(float)((float)((float)L_18*(float)(0.5f))))));
		float L_19 = __this->get_camVert_11();
		InputController_t3951031284 * L_20 = __this->get_inputController_6();
		Vector2_t2243707579  L_21 = InputController_get_dragVector_m2113092058(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		float L_22 = (&V_2)->get_y_1();
		__this->set_camVert_11(((float)((float)L_19+(float)((float)((float)L_22*(float)(0.5f))))));
	}

IL_00d0:
	{
		float L_23 = __this->get_camHoriz_10();
		__this->set_camHoriz_10((fmodf(L_23, (360.0f))));
		float L_24 = __this->get_camVert_11();
		__this->set_camVert_11((fmodf(L_24, (360.0f))));
		Camera_t189460977 * L_25 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_26 = Component_get_transform_m3374354972(L_25, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_27 = __this->get_camRot_12();
		Transform_set_rotation_m2824446320(L_26, L_27, /*hidden argument*/NULL);
		Camera_t189460977 * L_28 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m3374354972(L_28, /*hidden argument*/NULL);
		Character_t3726027591 * L_30 = __this->get_player_4();
		Transform_t3275118058 * L_31 = Component_get_transform_m3374354972(L_30, /*hidden argument*/NULL);
		Vector3_t2243707580  L_32 = Transform_get_position_m2304215762(L_31, /*hidden argument*/NULL);
		Vector3_t2243707580  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Vector3__ctor_m1555724485((&L_33), (0.0f), (0.0f), (-12.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_34 = Vector3_op_Addition_m394909128(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		Transform_set_position_m2942701431(L_29, L_34, /*hidden argument*/NULL);
		Camera_t189460977 * L_35 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_36 = Component_get_transform_m3374354972(L_35, /*hidden argument*/NULL);
		Character_t3726027591 * L_37 = __this->get_player_4();
		Transform_t3275118058 * L_38 = Component_get_transform_m3374354972(L_37, /*hidden argument*/NULL);
		Vector3_t2243707580  L_39 = Transform_get_position_m2304215762(L_38, /*hidden argument*/NULL);
		Transform_LookAt_m1970949065(L_36, L_39, /*hidden argument*/NULL);
		Camera_t189460977 * L_40 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_41 = Component_get_transform_m3374354972(L_40, /*hidden argument*/NULL);
		Character_t3726027591 * L_42 = __this->get_player_4();
		Transform_t3275118058 * L_43 = Component_get_transform_m3374354972(L_42, /*hidden argument*/NULL);
		Vector3_t2243707580  L_44 = Transform_get_position_m2304215762(L_43, /*hidden argument*/NULL);
		Vector3_t2243707580  L_45;
		memset(&L_45, 0, sizeof(L_45));
		Vector3__ctor_m1555724485((&L_45), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		float L_46 = __this->get_camHoriz_10();
		Transform_RotateAround_m132750417(L_41, L_44, L_45, L_46, /*hidden argument*/NULL);
		Camera_t189460977 * L_47 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_48 = Component_get_transform_m3374354972(L_47, /*hidden argument*/NULL);
		Character_t3726027591 * L_49 = __this->get_player_4();
		Transform_t3275118058 * L_50 = Component_get_transform_m3374354972(L_49, /*hidden argument*/NULL);
		Vector3_t2243707580  L_51 = Transform_get_position_m2304215762(L_50, /*hidden argument*/NULL);
		Camera_t189460977 * L_52 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_53 = Component_get_transform_m3374354972(L_52, /*hidden argument*/NULL);
		Vector3_t2243707580  L_54 = Transform_get_right_m1162814389(L_53, /*hidden argument*/NULL);
		float L_55 = __this->get_camVert_11();
		Transform_RotateAround_m132750417(L_48, L_51, L_54, L_55, /*hidden argument*/NULL);
		bool L_56 = __this->get_android_14();
		if (!L_56)
		{
			goto IL_01e8;
		}
	}
	{
		InputController_t3951031284 * L_57 = __this->get_inputController_6();
		bool L_58 = InputController_get_tapOverUI_m3908338243(L_57, /*hidden argument*/NULL);
		G_B13_0 = L_58;
		goto IL_01f2;
	}

IL_01e8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventSystem_t3466835263_il2cpp_TypeInfo_var);
		EventSystem_t3466835263 * L_59 = EventSystem_get_current_m319019811(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_60 = EventSystem_IsPointerOverGameObject_m1128779390(L_59, /*hidden argument*/NULL);
		G_B13_0 = L_60;
	}

IL_01f2:
	{
		V_3 = G_B13_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_61 = Input_GetMouseButtonDown_m2313448302(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_61)
		{
			goto IL_0209;
		}
	}
	{
		bool L_62 = __this->get_android_14();
		if (!L_62)
		{
			goto IL_0219;
		}
	}

IL_0209:
	{
		InputController_t3951031284 * L_63 = __this->get_inputController_6();
		bool L_64 = InputController_get_tapped_m2392099722(L_63, /*hidden argument*/NULL);
		if (!L_64)
		{
			goto IL_0225;
		}
	}

IL_0219:
	{
		bool L_65 = V_3;
		if (L_65)
		{
			goto IL_0225;
		}
	}
	{
		Game_movePlayer_m4079979315(__this, /*hidden argument*/NULL);
	}

IL_0225:
	{
		return;
	}
}
// System.Void Game::movePlayer()
extern "C"  void Game_movePlayer_m4079979315 (Game_t1600141214 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Game_movePlayer_m4079979315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	RaycastHit_t87180320  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Cell_t964604196 * V_6 = NULL;
	List_1_t145766874 * V_7 = NULL;
	Vector3_t2243707580  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	{
		Camera_t189460977 * L_0 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		Animator_t69676727 * L_1 = Component_GetComponent_TisAnimator_t69676727_m475627522(L_0, /*hidden argument*/Component_GetComponent_TisAnimator_t69676727_m475627522_RuntimeMethod_var);
		bool L_2 = Behaviour_get_enabled_m646668963(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		return;
	}

IL_0015:
	{
		bool L_3 = __this->get_android_14();
		if (!L_3)
		{
			goto IL_0055;
		}
	}
	{
		InputController_t3951031284 * L_4 = __this->get_inputController_6();
		Vector2_t2243707579  L_5 = InputController_get_tapPosition_m2446245635(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = (&V_1)->get_x_0();
		InputController_t3951031284 * L_7 = __this->get_inputController_6();
		Vector2_t2243707579  L_8 = InputController_get_tapPosition_m2446245635(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		float L_9 = (&V_2)->get_y_1();
		Vector3_t2243707580  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m1555724485((&L_10), L_6, L_9, (0.0f), /*hidden argument*/NULL);
		G_B5_0 = L_10;
		goto IL_005a;
	}

IL_0055:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_11 = Input_get_mousePosition_m2069200279(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B5_0 = L_11;
	}

IL_005a:
	{
		V_0 = G_B5_0;
		Camera_t189460977 * L_12 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_13 = V_0;
		Ray_t2469606224  L_14 = Camera_ScreenPointToRay_m3033403101(L_12, L_13, /*hidden argument*/NULL);
		bool L_15 = Physics_Raycast_m44003754(NULL /*static, unused*/, L_14, (&V_3), /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_010b;
		}
	}
	{
		Transform_t3275118058 * L_16 = RaycastHit_get_transform_m2333455049((&V_3), /*hidden argument*/NULL);
		String_t* L_17 = Component_get_tag_m124558427(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_18 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_17, _stringLiteral3745862197, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_010b;
		}
	}
	{
		Transform_t3275118058 * L_19 = RaycastHit_get_transform_m2333455049((&V_3), /*hidden argument*/NULL);
		Vector3_t2243707580  L_20 = Transform_get_position_m2304215762(L_19, /*hidden argument*/NULL);
		Vector3_t2243707580  L_21 = RaycastHit_get_normal_m391626824((&V_3), /*hidden argument*/NULL);
		V_5 = L_21;
		Vector3_t2243707580  L_22 = Vector3_get_normalized_m1057036856((&V_5), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_23 = Vector3_op_Addition_m394909128(NULL /*static, unused*/, L_20, L_22, /*hidden argument*/NULL);
		V_4 = L_23;
		Level_t866853782 * L_24 = __this->get_level_2();
		Vector3_t2243707580  L_25 = V_4;
		Cell_t964604196 * L_26 = Level_cellAtPosition_m891825944(L_24, L_25, /*hidden argument*/NULL);
		V_6 = L_26;
		Cell_t964604196 * L_27 = V_6;
		if (!L_27)
		{
			goto IL_010b;
		}
	}
	{
		Cell_t964604196 * L_28 = V_6;
		Character_t3726027591 * L_29 = __this->get_player_4();
		Cell_t964604196 * L_30 = L_29->get_currentCell_2();
		if ((((RuntimeObject*)(Cell_t964604196 *)L_28) == ((RuntimeObject*)(Cell_t964604196 *)L_30)))
		{
			goto IL_010b;
		}
	}
	{
		Cell_t964604196 * L_31 = V_6;
		bool L_32 = L_31->get_isObstacle_12();
		if (L_32)
		{
			goto IL_010b;
		}
	}
	{
		Pathfinder_t1592129589 * L_33 = __this->get_pathfinder_3();
		Character_t3726027591 * L_34 = __this->get_player_4();
		Cell_t964604196 * L_35 = L_34->get_currentCell_2();
		Cell_t964604196 * L_36 = V_6;
		List_1_t145766874 * L_37 = Pathfinder_findPath_m1726079702(L_33, L_35, L_36, /*hidden argument*/NULL);
		V_7 = L_37;
		Character_t3726027591 * L_38 = __this->get_player_4();
		List_1_t145766874 * L_39 = V_7;
		L_38->set_path_3(L_39);
	}

IL_010b:
	{
		return;
	}
}
// System.Void GridSnap::.ctor()
extern "C"  void GridSnap__ctor_m2871383599 (GridSnap_t338261636 * __this, const RuntimeMethod* method)
{
	{
		__this->set_snapX_2((1.0f));
		__this->set_snapY_3((1.0f));
		__this->set_snapZ_4((1.0f));
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GridSnap::Update()
extern "C"  void GridSnap_Update_m175371526 (GridSnap_t338261636 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GridSnap_Update_m175371526_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = Application_get_isPlaying_m3003895139(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0074;
		}
	}
	{
		Transform_t3275118058 * L_1 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Transform_get_position_m2304215762(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Transform_t3275118058 * L_3 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		float L_4 = (&V_0)->get_x_1();
		float L_5 = __this->get_snapX_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_6 = floorf(((float)((float)L_4/(float)L_5)));
		float L_7 = __this->get_snapX_2();
		float L_8 = (&V_0)->get_y_2();
		float L_9 = __this->get_snapY_3();
		float L_10 = floorf(((float)((float)L_8/(float)L_9)));
		float L_11 = __this->get_snapY_3();
		float L_12 = (&V_0)->get_z_3();
		float L_13 = __this->get_snapZ_4();
		float L_14 = floorf(((float)((float)L_12/(float)L_13)));
		float L_15 = __this->get_snapZ_4();
		Vector3_t2243707580  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m1555724485((&L_16), ((float)((float)L_6*(float)L_7)), ((float)((float)L_10*(float)L_11)), ((float)((float)L_14*(float)L_15)), /*hidden argument*/NULL);
		Transform_set_position_m2942701431(L_3, L_16, /*hidden argument*/NULL);
	}

IL_0074:
	{
		return;
	}
}
// System.Void HintTrigger::.ctor()
extern "C"  void HintTrigger__ctor_m2000469626 (HintTrigger_t910047485 * __this, const RuntimeMethod* method)
{
	{
		Trigger__ctor_m1356284031(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HintTrigger::Start()
extern "C"  void HintTrigger_Start_m599260802 (HintTrigger_t910047485 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HintTrigger_Start_m599260802_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1047780110, /*hidden argument*/NULL);
		Menu_t4261767481 * L_1 = GameObject_GetComponent_TisMenu_t4261767481_m3170984502(L_0, /*hidden argument*/GameObject_GetComponent_TisMenu_t4261767481_m3170984502_RuntimeMethod_var);
		__this->set_menu_3(L_1);
		return;
	}
}
// System.Void HintTrigger::enter(System.Boolean,Character)
extern "C"  void HintTrigger_enter_m638432472 (HintTrigger_t910047485 * __this, bool ___isDestination0, Character_t3726027591 * ___character1, const RuntimeMethod* method)
{
	{
		bool L_0 = ___isDestination0;
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		Character_t3726027591 * L_1 = ___character1;
		bool L_2 = Character_get_isPlayer_m2500959794(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		Menu_t4261767481 * L_3 = __this->get_menu_3();
		String_t* L_4 = __this->get_message_2();
		Menu_showDialog_m540151737(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void HintTrigger::exit(System.Boolean,Character)
extern "C"  void HintTrigger_exit_m3652678344 (HintTrigger_t910047485 * __this, bool ___isDestination0, Character_t3726027591 * ___character1, const RuntimeMethod* method)
{
	{
		Menu_t4261767481 * L_0 = __this->get_menu_3();
		Menu_hideDialog_m3443374652(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InputController::.ctor()
extern "C"  void InputController__ctor_m1592142465 (InputController_t3951031284 * __this, const RuntimeMethod* method)
{
	{
		__this->set_touchID_2((-1));
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean InputController::get_dragged()
extern "C"  bool InputController_get_dragged_m1166786620 (InputController_t3951031284 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CdraggedU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void InputController::set_dragged(System.Boolean)
extern "C"  void InputController_set_dragged_m1073616225 (InputController_t3951031284 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CdraggedU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Boolean InputController::get_tapped()
extern "C"  bool InputController_get_tapped_m2392099722 (InputController_t3951031284 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CtappedU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void InputController::set_tapped(System.Boolean)
extern "C"  void InputController_set_tapped_m706833503 (InputController_t3951031284 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CtappedU3Ek__BackingField_7(L_0);
		return;
	}
}
// UnityEngine.Vector2 InputController::get_dragVector()
extern "C"  Vector2_t2243707579  InputController_get_dragVector_m2113092058 (InputController_t3951031284 * __this, const RuntimeMethod* method)
{
	{
		Vector2_t2243707579  L_0 = __this->get_U3CdragVectorU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void InputController::set_dragVector(UnityEngine.Vector2)
extern "C"  void InputController_set_dragVector_m2671669613 (InputController_t3951031284 * __this, Vector2_t2243707579  ___value0, const RuntimeMethod* method)
{
	{
		Vector2_t2243707579  L_0 = ___value0;
		__this->set_U3CdragVectorU3Ek__BackingField_8(L_0);
		return;
	}
}
// UnityEngine.Vector2 InputController::get_tapPosition()
extern "C"  Vector2_t2243707579  InputController_get_tapPosition_m2446245635 (InputController_t3951031284 * __this, const RuntimeMethod* method)
{
	{
		Vector2_t2243707579  L_0 = __this->get_U3CtapPositionU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void InputController::set_tapPosition(UnityEngine.Vector2)
extern "C"  void InputController_set_tapPosition_m483651298 (InputController_t3951031284 * __this, Vector2_t2243707579  ___value0, const RuntimeMethod* method)
{
	{
		Vector2_t2243707579  L_0 = ___value0;
		__this->set_U3CtapPositionU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Boolean InputController::get_tapOverUI()
extern "C"  bool InputController_get_tapOverUI_m3908338243 (InputController_t3951031284 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CtapOverUIU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void InputController::set_tapOverUI(System.Boolean)
extern "C"  void InputController_set_tapOverUI_m3011029208 (InputController_t3951031284 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CtapOverUIU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Void InputController::Update()
extern "C"  void InputController_Update_m3485699262 (InputController_t3951031284 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InputController_Update_m3485699262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t407273883  V_0;
	memset(&V_0, 0, sizeof(V_0));
	TouchU5BU5D_t3887265178* V_1 = NULL;
	int32_t V_2 = 0;
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		InputController_set_dragged_m1073616225(__this, (bool)0, /*hidden argument*/NULL);
		InputController_set_tapped_m706833503(__this, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_0 = Vector3_get_zero_m3202043185(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_1 = Vector2_op_Implicit_m385881926(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		InputController_set_dragVector_m2671669613(__this, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		TouchU5BU5D_t3887265178* L_2 = Input_get_touches_m793401621(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_2;
		V_2 = 0;
		goto IL_0169;
	}

IL_002b:
	{
		TouchU5BU5D_t3887265178* L_3 = V_1;
		int32_t L_4 = V_2;
		V_0 = (*(Touch_t407273883 *)((L_3)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_4))));
		int32_t L_5 = Touch_get_phase_m972231807((&V_0), /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_006a;
		}
	}
	{
		int32_t L_6 = __this->get_touchID_2();
		if ((!(((uint32_t)L_6) == ((uint32_t)(-1)))))
		{
			goto IL_006a;
		}
	}
	{
		Vector2_t2243707579  L_7 = Touch_get_position_m261108426((&V_0), /*hidden argument*/NULL);
		__this->set_lastPos_4(L_7);
		int32_t L_8 = Touch_get_fingerId_m3527760158((&V_0), /*hidden argument*/NULL);
		__this->set_touchID_2(L_8);
	}

IL_006a:
	{
		int32_t L_9 = Touch_get_fingerId_m3527760158((&V_0), /*hidden argument*/NULL);
		int32_t L_10 = __this->get_touchID_2();
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_0165;
		}
	}
	{
		int32_t L_11 = Touch_get_phase_m972231807((&V_0), /*hidden argument*/NULL);
		if ((((int32_t)L_11) == ((int32_t)3)))
		{
			goto IL_009f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventSystem_t3466835263_il2cpp_TypeInfo_var);
		EventSystem_t3466835263 * L_12 = EventSystem_get_current_m319019811(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_13 = __this->get_touchID_2();
		bool L_14 = EventSystem_IsPointerOverGameObject_m2415000115(L_12, L_13, /*hidden argument*/NULL);
		InputController_set_tapOverUI_m3011029208(__this, L_14, /*hidden argument*/NULL);
	}

IL_009f:
	{
		int32_t L_15 = Touch_get_phase_m972231807((&V_0), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_15) == ((uint32_t)1))))
		{
			goto IL_012b;
		}
	}
	{
		Vector2_t2243707579  L_16 = Touch_get_position_m261108426((&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_17 = __this->get_lastPos_4();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_18 = Vector2_op_Subtraction_m1667221528(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		V_3 = L_18;
		float L_19 = Vector2_get_magnitude_m915202186((&V_3), /*hidden argument*/NULL);
		if ((!(((float)L_19) > ((float)(15.0f)))))
		{
			goto IL_00ef;
		}
	}
	{
		bool L_20 = __this->get_dragging_5();
		if (L_20)
		{
			goto IL_00ef;
		}
	}
	{
		__this->set_dragging_5((bool)1);
		Vector2_t2243707579  L_21 = Touch_get_position_m261108426((&V_0), /*hidden argument*/NULL);
		__this->set_lastPos_4(L_21);
	}

IL_00ef:
	{
		bool L_22 = __this->get_dragging_5();
		if (!L_22)
		{
			goto IL_0126;
		}
	}
	{
		InputController_set_dragged_m1073616225(__this, (bool)1, /*hidden argument*/NULL);
		Vector2_t2243707579  L_23 = Touch_get_position_m261108426((&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_24 = __this->get_lastPos_4();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_25 = Vector2_op_Subtraction_m1667221528(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		InputController_set_dragVector_m2671669613(__this, L_25, /*hidden argument*/NULL);
		Vector2_t2243707579  L_26 = Touch_get_position_m261108426((&V_0), /*hidden argument*/NULL);
		__this->set_lastPos_4(L_26);
	}

IL_0126:
	{
		goto IL_0165;
	}

IL_012b:
	{
		int32_t L_27 = Touch_get_phase_m972231807((&V_0), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_27) == ((uint32_t)3))))
		{
			goto IL_0165;
		}
	}
	{
		bool L_28 = __this->get_dragging_5();
		if (L_28)
		{
			goto IL_015e;
		}
	}
	{
		Vector2_t2243707579  L_29 = Touch_get_position_m261108426((&V_0), /*hidden argument*/NULL);
		InputController_set_tapPosition_m483651298(__this, L_29, /*hidden argument*/NULL);
		InputController_set_tapped_m706833503(__this, (bool)1, /*hidden argument*/NULL);
		__this->set_touchID_2((-1));
	}

IL_015e:
	{
		__this->set_dragging_5((bool)0);
	}

IL_0165:
	{
		int32_t L_30 = V_2;
		V_2 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_0169:
	{
		int32_t L_31 = V_2;
		TouchU5BU5D_t3887265178* L_32 = V_1;
		if ((((int32_t)L_31) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_32)->max_length)))))))
		{
			goto IL_002b;
		}
	}
	{
		return;
	}
}
// System.Void JumpTrigger::.ctor()
extern "C"  void JumpTrigger__ctor_m2796838257 (JumpTrigger_t1208645988 * __this, const RuntimeMethod* method)
{
	{
		Trigger__ctor_m1356284031(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JumpTrigger::Start()
extern "C"  void JumpTrigger_Start_m2965133417 (JumpTrigger_t1208645988 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JumpTrigger_Start_m2965133417_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		Character_t3726027591 * L_1 = GameObject_GetComponent_TisCharacter_t3726027591_m2667239018(L_0, /*hidden argument*/GameObject_GetComponent_TisCharacter_t3726027591_m2667239018_RuntimeMethod_var);
		__this->set_character_8(L_1);
		GameObject_t1756533147 * L_2 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1594932300, /*hidden argument*/NULL);
		Level_t866853782 * L_3 = GameObject_GetComponent_TisLevel_t866853782_m3318339701(L_2, /*hidden argument*/GameObject_GetComponent_TisLevel_t866853782_m3318339701_RuntimeMethod_var);
		__this->set_level_9(L_3);
		float L_4 = __this->get_width_4();
		__this->set_halfWidth_7(((float)((float)L_4/(float)(2.0f))));
		Vector3_t2243707580 * L_5 = __this->get_address_of_vert_2();
		Vector3_Normalize_m2606725557(L_5, /*hidden argument*/NULL);
		Vector3_t2243707580 * L_6 = __this->get_address_of_horiz_3();
		Vector3_Normalize_m2606725557(L_6, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = __this->get_vert_2();
		Vector3_t2243707580  L_8 = __this->get_horiz_3();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_9 = Vector3_Cross_m693157500(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		__this->set_axis_6(L_9);
		return;
	}
}
// System.Void JumpTrigger::Update()
extern "C"  void JumpTrigger_Update_m717189614 (JumpTrigger_t1208645988 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JumpTrigger_Update_m717189614_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		bool L_0 = __this->get_executing_10();
		if (!L_0)
		{
			goto IL_0117;
		}
	}
	{
		float L_1 = __this->get_progress_11();
		float L_2 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_progress_11(((float)((float)L_1+(float)((float)((float)L_2*(float)(1.0f))))));
		float L_3 = __this->get_progress_11();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_4 = Mathf_Clamp_m1779415170(NULL /*static, unused*/, L_3, (0.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_progress_11(L_4);
		float L_5 = __this->get_progress_11();
		V_0 = ((float)((float)L_5*(float)(3.14159274f)));
		Character_t3726027591 * L_6 = __this->get_character_8();
		Transform_t3275118058 * L_7 = Component_get_transform_m3374354972(L_6, /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = __this->get_start_12();
		float L_9 = __this->get_halfWidth_7();
		float L_10 = __this->get_halfWidth_7();
		float L_11 = V_0;
		float L_12 = cosf(L_11);
		Vector3_t2243707580  L_13 = __this->get_horiz_3();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_14 = Vector3_op_Multiply_m1869676276(NULL /*static, unused*/, ((float)((float)L_9-(float)((float)((float)L_10*(float)L_12)))), L_13, /*hidden argument*/NULL);
		Vector3_t2243707580  L_15 = Vector3_op_Addition_m394909128(NULL /*static, unused*/, L_8, L_14, /*hidden argument*/NULL);
		float L_16 = V_0;
		float L_17 = sinf(L_16);
		float L_18 = __this->get_height_5();
		Vector3_t2243707580  L_19 = __this->get_vert_2();
		Vector3_t2243707580  L_20 = Vector3_op_Multiply_m1869676276(NULL /*static, unused*/, ((float)((float)L_17*(float)L_18)), L_19, /*hidden argument*/NULL);
		Vector3_t2243707580  L_21 = Vector3_op_Addition_m394909128(NULL /*static, unused*/, L_15, L_20, /*hidden argument*/NULL);
		Transform_set_position_m2942701431(L_7, L_21, /*hidden argument*/NULL);
		Character_t3726027591 * L_22 = __this->get_character_8();
		Transform_t3275118058 * L_23 = Component_get_transform_m3374354972(L_22, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_24 = __this->get_rotation_13();
		Transform_set_rotation_m2824446320(L_23, L_24, /*hidden argument*/NULL);
		Character_t3726027591 * L_25 = __this->get_character_8();
		Transform_t3275118058 * L_26 = Component_get_transform_m3374354972(L_25, /*hidden argument*/NULL);
		Vector3_t2243707580  L_27 = __this->get_axis_6();
		float L_28 = __this->get_progress_11();
		Transform_Rotate_m1159264663(L_26, L_27, ((float)((float)L_28*(float)(360.0f))), /*hidden argument*/NULL);
		float L_29 = __this->get_progress_11();
		if ((!(((float)L_29) >= ((float)(1.0f)))))
		{
			goto IL_0117;
		}
	}
	{
		__this->set_executing_10((bool)0);
		Character_t3726027591 * L_30 = __this->get_character_8();
		Level_t866853782 * L_31 = __this->get_level_9();
		Character_t3726027591 * L_32 = __this->get_character_8();
		Transform_t3275118058 * L_33 = Component_get_transform_m3374354972(L_32, /*hidden argument*/NULL);
		Vector3_t2243707580  L_34 = Transform_get_position_m2304215762(L_33, /*hidden argument*/NULL);
		Cell_t964604196 * L_35 = Level_cellAtPosition_m891825944(L_31, L_34, /*hidden argument*/NULL);
		L_30->set_currentCell_2(L_35);
	}

IL_0117:
	{
		return;
	}
}
// System.Void JumpTrigger::enter(System.Boolean,Character)
extern "C"  void JumpTrigger_enter_m827476143 (JumpTrigger_t1208645988 * __this, bool ___isDestination0, Character_t3726027591 * ___character1, const RuntimeMethod* method)
{
	{
		bool L_0 = ___isDestination0;
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		Character_t3726027591 * L_1 = ___character1;
		__this->set_character_8(L_1);
		__this->set_executing_10((bool)1);
		__this->set_progress_11((0.0f));
		Character_t3726027591 * L_2 = ___character1;
		Transform_t3275118058 * L_3 = Component_get_transform_m3374354972(L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Transform_get_position_m2304215762(L_3, /*hidden argument*/NULL);
		__this->set_start_12(L_4);
		Character_t3726027591 * L_5 = ___character1;
		Transform_t3275118058 * L_6 = Component_get_transform_m3374354972(L_5, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_7 = Transform_get_rotation_m2617026815(L_6, /*hidden argument*/NULL);
		__this->set_rotation_13(L_7);
	}

IL_0041:
	{
		return;
	}
}
// System.Void Level::.ctor()
extern "C"  void Level__ctor_m1134081185 (Level_t866853782 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Level::Awake()
extern "C"  void Level_Awake_m3831100418 (Level_t866853782 * __this, const RuntimeMethod* method)
{
	{
		Level_createGrid_m485254149(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Level::createGrid()
extern "C"  void Level_createGrid_m485254149 (Level_t866853782 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Level_createGrid_m485254149_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	GameObject_t1756533147 * V_6 = NULL;
	GameObjectU5BU5D_t3057952154* V_7 = NULL;
	int32_t V_8 = 0;
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	GameObject_t1756533147 * V_14 = NULL;
	GameObjectU5BU5D_t3057952154* V_15 = NULL;
	int32_t V_16 = 0;
	Vector3_t2243707580  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Cell_t964604196 * V_18 = NULL;
	int32_t V_19 = 0;
	Renderer_t257310565 * V_20 = NULL;
	RendererU5BU5D_t2810717544* V_21 = NULL;
	int32_t V_22 = 0;
	GameObject_t1756533147 * V_23 = NULL;
	GameObjectU5BU5D_t3057952154* V_24 = NULL;
	int32_t V_25 = 0;
	Vector3_t2243707580  V_26;
	memset(&V_26, 0, sizeof(V_26));
	int32_t V_27 = 0;
	int32_t V_28 = 0;
	int32_t V_29 = 0;
	GameObject_t1756533147 * V_30 = NULL;
	GameObjectU5BU5D_t3057952154* V_31 = NULL;
	int32_t V_32 = 0;
	Vector3_t2243707580  V_33;
	memset(&V_33, 0, sizeof(V_33));
	Cell_t964604196 * V_34 = NULL;
	GameObject_t1756533147 * V_35 = NULL;
	GameObjectU5BU5D_t3057952154* V_36 = NULL;
	int32_t V_37 = 0;
	Vector3_t2243707580  V_38;
	memset(&V_38, 0, sizeof(V_38));
	Cell_t964604196 * V_39 = NULL;
	Trigger_t3844394560 * V_40 = NULL;
	TriggerU5BU5D_t3145789633* V_41 = NULL;
	int32_t V_42 = 0;
	Vector3_t2243707580  V_43;
	memset(&V_43, 0, sizeof(V_43));
	Vector3_t2243707580  V_44;
	memset(&V_44, 0, sizeof(V_44));
	Cell_t964604196 * V_45 = NULL;
	GameObject_t1756533147 * V_46 = NULL;
	GameObjectU5BU5D_t3057952154* V_47 = NULL;
	int32_t V_48 = 0;
	Vector3_t2243707580  V_49;
	memset(&V_49, 0, sizeof(V_49));
	Cell_t964604196 * V_50 = NULL;
	Cell_t964604196 * V_51 = NULL;
	CellU5BU2CU2CU5D_t1218597327* V_52 = NULL;
	int32_t V_53 = 0;
	int32_t V_54 = 0;
	int32_t V_55 = 0;
	int32_t V_56 = 0;
	int32_t V_57 = 0;
	int32_t V_58 = 0;
	Cell_t964604196 * V_59 = NULL;
	Cell_t964604196 * V_60 = NULL;
	CellU5BU2CU2CU5D_t1218597327* V_61 = NULL;
	int32_t V_62 = 0;
	int32_t V_63 = 0;
	int32_t V_64 = 0;
	int32_t V_65 = 0;
	int32_t V_66 = 0;
	int32_t V_67 = 0;
	Cell_t964604196 * V_68 = NULL;
	Enumerator_t4163422298  V_69;
	memset(&V_69, 0, sizeof(V_69));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (std::numeric_limits<float>::max());
		V_1 = (-std::numeric_limits<float>::max());
		V_2 = (std::numeric_limits<float>::max());
		V_3 = (-std::numeric_limits<float>::max());
		V_4 = (std::numeric_limits<float>::max());
		V_5 = (-std::numeric_limits<float>::max());
		GameObjectU5BU5D_t3057952154* L_0 = GameObject_FindGameObjectsWithTag_m3720927271(NULL /*static, unused*/, _stringLiteral3745862197, /*hidden argument*/NULL);
		V_7 = L_0;
		V_8 = 0;
		goto IL_00ad;
	}

IL_003a:
	{
		GameObjectU5BU5D_t3057952154* L_1 = V_7;
		int32_t L_2 = V_8;
		int32_t L_3 = L_2;
		GameObject_t1756533147 * L_4 = (L_1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_3));
		V_6 = L_4;
		GameObject_t1756533147 * L_5 = V_6;
		Transform_t3275118058 * L_6 = GameObject_get_transform_m3490276752(L_5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = Transform_get_position_m2304215762(L_6, /*hidden argument*/NULL);
		V_9 = L_7;
		float L_8 = V_0;
		float L_9 = (&V_9)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_10 = Mathf_Min_m1552415280(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		float L_11 = V_1;
		float L_12 = (&V_9)->get_x_1();
		float L_13 = Mathf_Max_m2043094730(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		float L_14 = V_2;
		float L_15 = (&V_9)->get_y_2();
		float L_16 = Mathf_Min_m1552415280(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		V_2 = L_16;
		float L_17 = V_3;
		float L_18 = (&V_9)->get_y_2();
		float L_19 = Mathf_Max_m2043094730(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		float L_20 = V_4;
		float L_21 = (&V_9)->get_z_3();
		float L_22 = Mathf_Min_m1552415280(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		V_4 = L_22;
		float L_23 = V_5;
		float L_24 = (&V_9)->get_z_3();
		float L_25 = Mathf_Max_m2043094730(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		V_5 = L_25;
		int32_t L_26 = V_8;
		V_8 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00ad:
	{
		int32_t L_27 = V_8;
		GameObjectU5BU5D_t3057952154* L_28 = V_7;
		if ((((int32_t)L_27) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_28)->max_length)))))))
		{
			goto IL_003a;
		}
	}
	{
		V_10 = 3;
		Transform_t3275118058 * L_29 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_30 = L_29;
		Vector3_t2243707580  L_31 = Transform_get_position_m2304215762(L_30, /*hidden argument*/NULL);
		float L_32 = V_0;
		int32_t L_33 = V_10;
		float L_34 = V_2;
		int32_t L_35 = V_10;
		float L_36 = V_4;
		int32_t L_37 = V_10;
		Vector3_t2243707580  L_38;
		memset(&L_38, 0, sizeof(L_38));
		Vector3__ctor_m1555724485((&L_38), ((-((float)((float)L_32-(float)(((float)((float)L_33))))))), ((-((float)((float)L_34-(float)(((float)((float)L_35))))))), ((-((float)((float)L_36-(float)(((float)((float)L_37))))))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_39 = Vector3_op_Addition_m394909128(NULL /*static, unused*/, L_31, L_38, /*hidden argument*/NULL);
		Transform_set_position_m2942701431(L_30, L_39, /*hidden argument*/NULL);
		float L_40 = V_1;
		float L_41 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_42 = bankers_roundf(((float)((float)L_40-(float)L_41)));
		int32_t L_43 = V_10;
		V_11 = ((int32_t)((int32_t)(((int32_t)((int32_t)L_42)))+(int32_t)((int32_t)((int32_t)2*(int32_t)L_43))));
		float L_44 = V_3;
		float L_45 = V_2;
		float L_46 = bankers_roundf(((float)((float)L_44-(float)L_45)));
		int32_t L_47 = V_10;
		V_12 = ((int32_t)((int32_t)(((int32_t)((int32_t)L_46)))+(int32_t)((int32_t)((int32_t)2*(int32_t)L_47))));
		float L_48 = V_5;
		float L_49 = V_4;
		float L_50 = bankers_roundf(((float)((float)L_48-(float)L_49)));
		int32_t L_51 = V_10;
		V_13 = ((int32_t)((int32_t)(((int32_t)((int32_t)L_50)))+(int32_t)((int32_t)((int32_t)2*(int32_t)L_51))));
		int32_t L_52 = V_11;
		int32_t L_53 = V_12;
		int32_t L_54 = V_13;
		il2cpp_array_size_t L_56[] = { (il2cpp_array_size_t)L_52, (il2cpp_array_size_t)L_53, (il2cpp_array_size_t)L_54 };
		CellU5BU2CU2CU5D_t1218597327* L_55 = (CellU5BU2CU2CU5D_t1218597327*)GenArrayNew(CellU5BU2CU2CU5D_t1218597327_il2cpp_TypeInfo_var, L_56);
		__this->set_cells_2((CellU5BU2CU2CU5D_t1218597327*)L_55);
		GameObjectU5BU5D_t3057952154* L_57 = GameObject_FindGameObjectsWithTag_m3720927271(NULL /*static, unused*/, _stringLiteral3745862197, /*hidden argument*/NULL);
		V_15 = L_57;
		V_16 = 0;
		goto IL_0219;
	}

IL_0140:
	{
		GameObjectU5BU5D_t3057952154* L_58 = V_15;
		int32_t L_59 = V_16;
		int32_t L_60 = L_59;
		GameObject_t1756533147 * L_61 = (L_58)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_60));
		V_14 = L_61;
		GameObject_t1756533147 * L_62 = V_14;
		Transform_t3275118058 * L_63 = GameObject_get_transform_m3490276752(L_62, /*hidden argument*/NULL);
		Vector3_t2243707580  L_64 = Transform_get_position_m2304215762(L_63, /*hidden argument*/NULL);
		V_17 = L_64;
		float L_65 = (&V_17)->get_x_1();
		float L_66 = (&V_17)->get_y_2();
		float L_67 = (&V_17)->get_z_3();
		Cell_t964604196 * L_68 = (Cell_t964604196 *)il2cpp_codegen_object_new(Cell_t964604196_il2cpp_TypeInfo_var);
		Cell__ctor_m2554603460(L_68, (((int32_t)((int32_t)L_65))), (((int32_t)((int32_t)L_66))), (((int32_t)((int32_t)L_67))), (bool)1, /*hidden argument*/NULL);
		V_18 = L_68;
		CellU5BU2CU2CU5D_t1218597327* L_69 = __this->get_cells_2();
		float L_70 = (&V_17)->get_x_1();
		float L_71 = (&V_17)->get_y_2();
		float L_72 = (&V_17)->get_z_3();
		Cell_t964604196 * L_73 = V_18;
		((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_69)->SetAtUnchecked((((int32_t)((int32_t)L_70))), (((int32_t)((int32_t)L_71))), (((int32_t)((int32_t)L_72))), L_73);
		V_19 = 0;
		goto IL_0200;
	}

IL_01a2:
	{
		GameObject_t1756533147 * L_74 = V_14;
		Transform_t3275118058 * L_75 = GameObject_get_transform_m3490276752(L_74, /*hidden argument*/NULL);
		int32_t L_76 = V_19;
		Transform_t3275118058 * L_77 = Transform_GetChild_m3136145191(L_75, L_76, /*hidden argument*/NULL);
		String_t* L_78 = Component_get_tag_m124558427(L_77, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_79 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_78, _stringLiteral2025730826, /*hidden argument*/NULL);
		if (!L_79)
		{
			goto IL_01fa;
		}
	}
	{
		GameObject_t1756533147 * L_80 = V_14;
		RendererU5BU5D_t2810717544* L_81 = GameObject_GetComponentsInChildren_TisRenderer_t257310565_m2883698982(L_80, /*hidden argument*/GameObject_GetComponentsInChildren_TisRenderer_t257310565_m2883698982_RuntimeMethod_var);
		V_21 = L_81;
		V_22 = 0;
		goto IL_01ea;
	}

IL_01d5:
	{
		RendererU5BU5D_t2810717544* L_82 = V_21;
		int32_t L_83 = V_22;
		int32_t L_84 = L_83;
		Renderer_t257310565 * L_85 = (L_82)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_84));
		V_20 = L_85;
		Renderer_t257310565 * L_86 = V_20;
		Renderer_set_enabled_m383916764(L_86, (bool)0, /*hidden argument*/NULL);
		int32_t L_87 = V_22;
		V_22 = ((int32_t)((int32_t)L_87+(int32_t)1));
	}

IL_01ea:
	{
		int32_t L_88 = V_22;
		RendererU5BU5D_t2810717544* L_89 = V_21;
		if ((((int32_t)L_88) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_89)->max_length)))))))
		{
			goto IL_01d5;
		}
	}
	{
		goto IL_0213;
	}

IL_01fa:
	{
		int32_t L_90 = V_19;
		V_19 = ((int32_t)((int32_t)L_90+(int32_t)1));
	}

IL_0200:
	{
		int32_t L_91 = V_19;
		GameObject_t1756533147 * L_92 = V_14;
		Transform_t3275118058 * L_93 = GameObject_get_transform_m3490276752(L_92, /*hidden argument*/NULL);
		int32_t L_94 = Transform_get_childCount_m196843760(L_93, /*hidden argument*/NULL);
		if ((((int32_t)L_91) < ((int32_t)L_94)))
		{
			goto IL_01a2;
		}
	}

IL_0213:
	{
		int32_t L_95 = V_16;
		V_16 = ((int32_t)((int32_t)L_95+(int32_t)1));
	}

IL_0219:
	{
		int32_t L_96 = V_16;
		GameObjectU5BU5D_t3057952154* L_97 = V_15;
		if ((((int32_t)L_96) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_97)->max_length)))))))
		{
			goto IL_0140;
		}
	}
	{
		GameObjectU5BU5D_t3057952154* L_98 = GameObject_FindGameObjectsWithTag_m3720927271(NULL /*static, unused*/, _stringLiteral3745862197, /*hidden argument*/NULL);
		V_24 = L_98;
		V_25 = 0;
		goto IL_03c7;
	}

IL_0238:
	{
		GameObjectU5BU5D_t3057952154* L_99 = V_24;
		int32_t L_100 = V_25;
		int32_t L_101 = L_100;
		GameObject_t1756533147 * L_102 = (L_99)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_101));
		V_23 = L_102;
		GameObject_t1756533147 * L_103 = V_23;
		Transform_t3275118058 * L_104 = GameObject_get_transform_m3490276752(L_103, /*hidden argument*/NULL);
		Vector3_t2243707580  L_105 = Transform_get_position_m2304215762(L_104, /*hidden argument*/NULL);
		V_26 = L_105;
		float L_106 = (&V_26)->get_x_1();
		V_27 = (((int32_t)((int32_t)L_106)));
		float L_107 = (&V_26)->get_y_2();
		V_28 = (((int32_t)((int32_t)L_107)));
		float L_108 = (&V_26)->get_z_3();
		V_29 = (((int32_t)((int32_t)L_108)));
		CellU5BU2CU2CU5D_t1218597327* L_109 = __this->get_cells_2();
		int32_t L_110 = V_27;
		int32_t L_111 = V_28;
		int32_t L_112 = V_29;
		Cell_t964604196 * L_113 = ((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_109)->GetAtUnchecked(((int32_t)((int32_t)L_110+(int32_t)1)), L_111, L_112);
		if (L_113)
		{
			goto IL_02a4;
		}
	}
	{
		CellU5BU2CU2CU5D_t1218597327* L_114 = __this->get_cells_2();
		int32_t L_115 = V_27;
		int32_t L_116 = V_28;
		int32_t L_117 = V_29;
		int32_t L_118 = V_27;
		int32_t L_119 = V_28;
		int32_t L_120 = V_29;
		Cell_t964604196 * L_121 = (Cell_t964604196 *)il2cpp_codegen_object_new(Cell_t964604196_il2cpp_TypeInfo_var);
		Cell__ctor_m2554603460(L_121, ((int32_t)((int32_t)L_118+(int32_t)1)), L_119, L_120, (bool)0, /*hidden argument*/NULL);
		((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_114)->SetAtUnchecked(((int32_t)((int32_t)L_115+(int32_t)1)), L_116, L_117, L_121);
	}

IL_02a4:
	{
		CellU5BU2CU2CU5D_t1218597327* L_122 = __this->get_cells_2();
		int32_t L_123 = V_27;
		int32_t L_124 = V_28;
		int32_t L_125 = V_29;
		Cell_t964604196 * L_126 = ((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_122)->GetAtUnchecked(((int32_t)((int32_t)L_123-(int32_t)1)), L_124, L_125);
		if (L_126)
		{
			goto IL_02dd;
		}
	}
	{
		CellU5BU2CU2CU5D_t1218597327* L_127 = __this->get_cells_2();
		int32_t L_128 = V_27;
		int32_t L_129 = V_28;
		int32_t L_130 = V_29;
		int32_t L_131 = V_27;
		int32_t L_132 = V_28;
		int32_t L_133 = V_29;
		Cell_t964604196 * L_134 = (Cell_t964604196 *)il2cpp_codegen_object_new(Cell_t964604196_il2cpp_TypeInfo_var);
		Cell__ctor_m2554603460(L_134, ((int32_t)((int32_t)L_131-(int32_t)1)), L_132, L_133, (bool)0, /*hidden argument*/NULL);
		((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_127)->SetAtUnchecked(((int32_t)((int32_t)L_128-(int32_t)1)), L_129, L_130, L_134);
	}

IL_02dd:
	{
		CellU5BU2CU2CU5D_t1218597327* L_135 = __this->get_cells_2();
		int32_t L_136 = V_27;
		int32_t L_137 = V_28;
		int32_t L_138 = V_29;
		Cell_t964604196 * L_139 = ((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_135)->GetAtUnchecked(L_136, ((int32_t)((int32_t)L_137+(int32_t)1)), L_138);
		if (L_139)
		{
			goto IL_0316;
		}
	}
	{
		CellU5BU2CU2CU5D_t1218597327* L_140 = __this->get_cells_2();
		int32_t L_141 = V_27;
		int32_t L_142 = V_28;
		int32_t L_143 = V_29;
		int32_t L_144 = V_27;
		int32_t L_145 = V_28;
		int32_t L_146 = V_29;
		Cell_t964604196 * L_147 = (Cell_t964604196 *)il2cpp_codegen_object_new(Cell_t964604196_il2cpp_TypeInfo_var);
		Cell__ctor_m2554603460(L_147, L_144, ((int32_t)((int32_t)L_145+(int32_t)1)), L_146, (bool)0, /*hidden argument*/NULL);
		((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_140)->SetAtUnchecked(L_141, ((int32_t)((int32_t)L_142+(int32_t)1)), L_143, L_147);
	}

IL_0316:
	{
		CellU5BU2CU2CU5D_t1218597327* L_148 = __this->get_cells_2();
		int32_t L_149 = V_27;
		int32_t L_150 = V_28;
		int32_t L_151 = V_29;
		Cell_t964604196 * L_152 = ((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_148)->GetAtUnchecked(L_149, ((int32_t)((int32_t)L_150-(int32_t)1)), L_151);
		if (L_152)
		{
			goto IL_034f;
		}
	}
	{
		CellU5BU2CU2CU5D_t1218597327* L_153 = __this->get_cells_2();
		int32_t L_154 = V_27;
		int32_t L_155 = V_28;
		int32_t L_156 = V_29;
		int32_t L_157 = V_27;
		int32_t L_158 = V_28;
		int32_t L_159 = V_29;
		Cell_t964604196 * L_160 = (Cell_t964604196 *)il2cpp_codegen_object_new(Cell_t964604196_il2cpp_TypeInfo_var);
		Cell__ctor_m2554603460(L_160, L_157, ((int32_t)((int32_t)L_158-(int32_t)1)), L_159, (bool)0, /*hidden argument*/NULL);
		((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_153)->SetAtUnchecked(L_154, ((int32_t)((int32_t)L_155-(int32_t)1)), L_156, L_160);
	}

IL_034f:
	{
		CellU5BU2CU2CU5D_t1218597327* L_161 = __this->get_cells_2();
		int32_t L_162 = V_27;
		int32_t L_163 = V_28;
		int32_t L_164 = V_29;
		Cell_t964604196 * L_165 = ((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_161)->GetAtUnchecked(L_162, L_163, ((int32_t)((int32_t)L_164+(int32_t)1)));
		if (L_165)
		{
			goto IL_0388;
		}
	}
	{
		CellU5BU2CU2CU5D_t1218597327* L_166 = __this->get_cells_2();
		int32_t L_167 = V_27;
		int32_t L_168 = V_28;
		int32_t L_169 = V_29;
		int32_t L_170 = V_27;
		int32_t L_171 = V_28;
		int32_t L_172 = V_29;
		Cell_t964604196 * L_173 = (Cell_t964604196 *)il2cpp_codegen_object_new(Cell_t964604196_il2cpp_TypeInfo_var);
		Cell__ctor_m2554603460(L_173, L_170, L_171, ((int32_t)((int32_t)L_172+(int32_t)1)), (bool)0, /*hidden argument*/NULL);
		((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_166)->SetAtUnchecked(L_167, L_168, ((int32_t)((int32_t)L_169+(int32_t)1)), L_173);
	}

IL_0388:
	{
		CellU5BU2CU2CU5D_t1218597327* L_174 = __this->get_cells_2();
		int32_t L_175 = V_27;
		int32_t L_176 = V_28;
		int32_t L_177 = V_29;
		Cell_t964604196 * L_178 = ((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_174)->GetAtUnchecked(L_175, L_176, ((int32_t)((int32_t)L_177-(int32_t)1)));
		if (L_178)
		{
			goto IL_03c1;
		}
	}
	{
		CellU5BU2CU2CU5D_t1218597327* L_179 = __this->get_cells_2();
		int32_t L_180 = V_27;
		int32_t L_181 = V_28;
		int32_t L_182 = V_29;
		int32_t L_183 = V_27;
		int32_t L_184 = V_28;
		int32_t L_185 = V_29;
		Cell_t964604196 * L_186 = (Cell_t964604196 *)il2cpp_codegen_object_new(Cell_t964604196_il2cpp_TypeInfo_var);
		Cell__ctor_m2554603460(L_186, L_183, L_184, ((int32_t)((int32_t)L_185-(int32_t)1)), (bool)0, /*hidden argument*/NULL);
		((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_179)->SetAtUnchecked(L_180, L_181, ((int32_t)((int32_t)L_182-(int32_t)1)), L_186);
	}

IL_03c1:
	{
		int32_t L_187 = V_25;
		V_25 = ((int32_t)((int32_t)L_187+(int32_t)1));
	}

IL_03c7:
	{
		int32_t L_188 = V_25;
		GameObjectU5BU5D_t3057952154* L_189 = V_24;
		if ((((int32_t)L_188) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_189)->max_length)))))))
		{
			goto IL_0238;
		}
	}
	{
		GameObjectU5BU5D_t3057952154* L_190 = GameObject_FindGameObjectsWithTag_m3720927271(NULL /*static, unused*/, _stringLiteral2124864605, /*hidden argument*/NULL);
		V_31 = L_190;
		V_32 = 0;
		goto IL_041a;
	}

IL_03e6:
	{
		GameObjectU5BU5D_t3057952154* L_191 = V_31;
		int32_t L_192 = V_32;
		int32_t L_193 = L_192;
		GameObject_t1756533147 * L_194 = (L_191)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_193));
		V_30 = L_194;
		GameObject_t1756533147 * L_195 = V_30;
		Transform_t3275118058 * L_196 = GameObject_get_transform_m3490276752(L_195, /*hidden argument*/NULL);
		Vector3_t2243707580  L_197 = Transform_get_position_m2304215762(L_196, /*hidden argument*/NULL);
		V_33 = L_197;
		Vector3_t2243707580  L_198 = V_33;
		Cell_t964604196 * L_199 = Level_cellAtPosition_m891825944(__this, L_198, /*hidden argument*/NULL);
		V_34 = L_199;
		Cell_t964604196 * L_200 = V_34;
		if (!L_200)
		{
			goto IL_0414;
		}
	}
	{
		Cell_t964604196 * L_201 = V_34;
		L_201->set_isObstacle_12((bool)1);
	}

IL_0414:
	{
		int32_t L_202 = V_32;
		V_32 = ((int32_t)((int32_t)L_202+(int32_t)1));
	}

IL_041a:
	{
		int32_t L_203 = V_32;
		GameObjectU5BU5D_t3057952154* L_204 = V_31;
		if ((((int32_t)L_203) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_204)->max_length)))))))
		{
			goto IL_03e6;
		}
	}
	{
		GameObjectU5BU5D_t3057952154* L_205 = GameObject_FindGameObjectsWithTag_m3720927271(NULL /*static, unused*/, _stringLiteral1093546118, /*hidden argument*/NULL);
		V_36 = L_205;
		V_37 = 0;
		goto IL_049e;
	}

IL_0439:
	{
		GameObjectU5BU5D_t3057952154* L_206 = V_36;
		int32_t L_207 = V_37;
		int32_t L_208 = L_207;
		GameObject_t1756533147 * L_209 = (L_206)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_208));
		V_35 = L_209;
		GameObject_t1756533147 * L_210 = V_35;
		Transform_t3275118058 * L_211 = GameObject_get_transform_m3490276752(L_210, /*hidden argument*/NULL);
		Vector3_t2243707580  L_212 = Transform_get_position_m2304215762(L_211, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_213 = V_35;
		Transform_t3275118058 * L_214 = GameObject_get_transform_m3490276752(L_213, /*hidden argument*/NULL);
		Vector3_t2243707580  L_215 = Transform_get_position_m2304215762(L_214, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_216 = V_35;
		Transform_t3275118058 * L_217 = GameObject_get_transform_m3490276752(L_216, /*hidden argument*/NULL);
		Transform_t3275118058 * L_218 = Transform_get_parent_m2752514051(L_217, /*hidden argument*/NULL);
		Vector3_t2243707580  L_219 = Transform_get_position_m2304215762(L_218, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_220 = Vector3_op_Subtraction_m4046047256(NULL /*static, unused*/, L_215, L_219, /*hidden argument*/NULL);
		Vector3_t2243707580  L_221 = Vector3_op_Multiply_m1869676276(NULL /*static, unused*/, (2.0f), L_220, /*hidden argument*/NULL);
		Vector3_t2243707580  L_222 = Vector3_op_Addition_m394909128(NULL /*static, unused*/, L_212, L_221, /*hidden argument*/NULL);
		V_38 = L_222;
		Vector3_t2243707580  L_223 = V_38;
		Cell_t964604196 * L_224 = Level_cellAtPosition_m891825944(__this, L_223, /*hidden argument*/NULL);
		V_39 = L_224;
		Cell_t964604196 * L_225 = V_39;
		if (!L_225)
		{
			goto IL_0498;
		}
	}
	{
		Cell_t964604196 * L_226 = V_39;
		L_226->set_isObstacle_12((bool)1);
	}

IL_0498:
	{
		int32_t L_227 = V_37;
		V_37 = ((int32_t)((int32_t)L_227+(int32_t)1));
	}

IL_049e:
	{
		int32_t L_228 = V_37;
		GameObjectU5BU5D_t3057952154* L_229 = V_36;
		if ((((int32_t)L_228) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_229)->max_length)))))))
		{
			goto IL_0439;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_230 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Trigger_t3844394560_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t4217747464* L_231 = Object_FindObjectsOfType_m3335374525(NULL /*static, unused*/, L_230, /*hidden argument*/NULL);
		V_41 = ((TriggerU5BU5D_t3145789633*)Castclass((RuntimeObject*)L_231, TriggerU5BU5D_t3145789633_il2cpp_TypeInfo_var));
		V_42 = 0;
		goto IL_0556;
	}

IL_04c7:
	{
		TriggerU5BU5D_t3145789633* L_232 = V_41;
		int32_t L_233 = V_42;
		int32_t L_234 = L_233;
		Trigger_t3844394560 * L_235 = (L_232)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_234));
		V_40 = L_235;
		Trigger_t3844394560 * L_236 = V_40;
		Transform_t3275118058 * L_237 = Component_get_transform_m3374354972(L_236, /*hidden argument*/NULL);
		Vector3_t2243707580  L_238 = Transform_get_position_m2304215762(L_237, /*hidden argument*/NULL);
		Trigger_t3844394560 * L_239 = V_40;
		Transform_t3275118058 * L_240 = Component_get_transform_m3374354972(L_239, /*hidden argument*/NULL);
		Transform_t3275118058 * L_241 = Transform_get_parent_m2752514051(L_240, /*hidden argument*/NULL);
		Vector3_t2243707580  L_242 = Transform_get_position_m2304215762(L_241, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_243 = Vector3_op_Subtraction_m4046047256(NULL /*static, unused*/, L_238, L_242, /*hidden argument*/NULL);
		Vector3_t2243707580  L_244 = Vector3_op_Multiply_m2498445460(NULL /*static, unused*/, L_243, (2.0f), /*hidden argument*/NULL);
		V_43 = L_244;
		Trigger_t3844394560 * L_245 = V_40;
		Transform_t3275118058 * L_246 = Component_get_transform_m3374354972(L_245, /*hidden argument*/NULL);
		Transform_t3275118058 * L_247 = Transform_get_parent_m2752514051(L_246, /*hidden argument*/NULL);
		Vector3_t2243707580  L_248 = Transform_get_position_m2304215762(L_247, /*hidden argument*/NULL);
		Vector3_t2243707580  L_249 = V_43;
		Vector3_t2243707580  L_250 = Vector3_op_Addition_m394909128(NULL /*static, unused*/, L_248, L_249, /*hidden argument*/NULL);
		V_44 = L_250;
		CellU5BU2CU2CU5D_t1218597327* L_251 = __this->get_cells_2();
		float L_252 = (&V_44)->get_x_1();
		float L_253 = (&V_44)->get_y_2();
		float L_254 = (&V_44)->get_z_3();
		Cell_t964604196 * L_255 = ((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_251)->GetAtUnchecked((((int32_t)((int32_t)L_252))), (((int32_t)((int32_t)L_253))), (((int32_t)((int32_t)L_254))));
		V_45 = L_255;
		Cell_t964604196 * L_256 = V_45;
		if (!L_256)
		{
			goto IL_0550;
		}
	}
	{
		Cell_t964604196 * L_257 = V_45;
		Trigger_t3844394560 * L_258 = V_40;
		Trigger_t3844394560 * L_259 = Component_GetComponent_TisTrigger_t3844394560_m3031482907(L_258, /*hidden argument*/Component_GetComponent_TisTrigger_t3844394560_m3031482907_RuntimeMethod_var);
		L_257->set_trigger_11(L_259);
	}

IL_0550:
	{
		int32_t L_260 = V_42;
		V_42 = ((int32_t)((int32_t)L_260+(int32_t)1));
	}

IL_0556:
	{
		int32_t L_261 = V_42;
		TriggerU5BU5D_t3145789633* L_262 = V_41;
		if ((((int32_t)L_261) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_262)->max_length)))))))
		{
			goto IL_04c7;
		}
	}
	{
		GameObjectU5BU5D_t3057952154* L_263 = GameObject_FindGameObjectsWithTag_m3720927271(NULL /*static, unused*/, _stringLiteral2596549766, /*hidden argument*/NULL);
		V_47 = L_263;
		V_48 = 0;
		goto IL_05c5;
	}

IL_0575:
	{
		GameObjectU5BU5D_t3057952154* L_264 = V_47;
		int32_t L_265 = V_48;
		int32_t L_266 = L_265;
		GameObject_t1756533147 * L_267 = (L_264)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_266));
		V_46 = L_267;
		GameObject_t1756533147 * L_268 = V_46;
		Transform_t3275118058 * L_269 = GameObject_get_transform_m3490276752(L_268, /*hidden argument*/NULL);
		Vector3_t2243707580  L_270 = Transform_get_position_m2304215762(L_269, /*hidden argument*/NULL);
		V_49 = L_270;
		CellU5BU2CU2CU5D_t1218597327* L_271 = __this->get_cells_2();
		float L_272 = (&V_49)->get_x_1();
		float L_273 = (&V_49)->get_y_2();
		float L_274 = (&V_49)->get_z_3();
		Cell_t964604196 * L_275 = ((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_271)->GetAtUnchecked((((int32_t)((int32_t)L_272))), (((int32_t)((int32_t)L_273))), (((int32_t)((int32_t)L_274))));
		V_50 = L_275;
		Cell_t964604196 * L_276 = V_50;
		if (!L_276)
		{
			goto IL_05bf;
		}
	}
	{
		Cell_t964604196 * L_277 = V_50;
		GameObject_t1756533147 * L_278 = V_46;
		L_277->set_fragment_13(L_278);
	}

IL_05bf:
	{
		int32_t L_279 = V_48;
		V_48 = ((int32_t)((int32_t)L_279+(int32_t)1));
	}

IL_05c5:
	{
		int32_t L_280 = V_48;
		GameObjectU5BU5D_t3057952154* L_281 = V_47;
		if ((((int32_t)L_280) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_281)->max_length)))))))
		{
			goto IL_0575;
		}
	}
	{
		CellU5BU2CU2CU5D_t1218597327* L_282 = __this->get_cells_2();
		V_52 = (CellU5BU2CU2CU5D_t1218597327*)L_282;
		CellU5BU2CU2CU5D_t1218597327* L_283 = V_52;
		int32_t L_284 = Array_GetLength_m2083296647((RuntimeArray *)(RuntimeArray *)L_283, 0, /*hidden argument*/NULL);
		V_53 = L_284;
		CellU5BU2CU2CU5D_t1218597327* L_285 = V_52;
		int32_t L_286 = Array_GetLength_m2083296647((RuntimeArray *)(RuntimeArray *)L_285, 1, /*hidden argument*/NULL);
		V_54 = L_286;
		CellU5BU2CU2CU5D_t1218597327* L_287 = V_52;
		int32_t L_288 = Array_GetLength_m2083296647((RuntimeArray *)(RuntimeArray *)L_287, 2, /*hidden argument*/NULL);
		V_55 = L_288;
		V_56 = 0;
		goto IL_07fd;
	}

IL_05fe:
	{
		V_57 = 0;
		goto IL_07ee;
	}

IL_0606:
	{
		V_58 = 0;
		goto IL_07df;
	}

IL_060e:
	{
		CellU5BU2CU2CU5D_t1218597327* L_289 = V_52;
		int32_t L_290 = V_56;
		int32_t L_291 = V_57;
		int32_t L_292 = V_58;
		Cell_t964604196 * L_293 = (L_289)->GetAtUnchecked(L_290, L_291, L_292);
		V_51 = L_293;
		Cell_t964604196 * L_294 = V_51;
		if (L_294)
		{
			goto IL_0629;
		}
	}
	{
		goto IL_07d9;
	}

IL_0629:
	{
		Cell_t964604196 * L_295 = V_51;
		bool L_296 = Cell_get_isBlock_m3829309610(L_295, /*hidden argument*/NULL);
		if (L_296)
		{
			goto IL_07d9;
		}
	}
	{
		Cell_t964604196 * L_297 = V_51;
		List_1_t333725328 * L_298 = Cell_get_blocks_m153328201(L_297, /*hidden argument*/NULL);
		List_1_Clear_m3715491197(L_298, /*hidden argument*/List_1_Clear_m3715491197_RuntimeMethod_var);
		CellU5BU2CU2CU5D_t1218597327* L_299 = __this->get_cells_2();
		Cell_t964604196 * L_300 = V_51;
		int32_t L_301 = Cell_get_x_m2406521465(L_300, /*hidden argument*/NULL);
		Cell_t964604196 * L_302 = V_51;
		int32_t L_303 = Cell_get_y_m2406521560(L_302, /*hidden argument*/NULL);
		Cell_t964604196 * L_304 = V_51;
		int32_t L_305 = Cell_get_z_m2406521399(L_304, /*hidden argument*/NULL);
		Cell_t964604196 * L_306 = ((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_299)->GetAtUnchecked(((int32_t)((int32_t)L_301-(int32_t)1)), L_303, L_305);
		Cell_t964604196 * L_307 = L_306;
		V_59 = L_307;
		if (!L_307)
		{
			goto IL_0685;
		}
	}
	{
		Cell_t964604196 * L_308 = V_59;
		bool L_309 = Cell_get_isBlock_m3829309610(L_308, /*hidden argument*/NULL);
		if (!L_309)
		{
			goto IL_0685;
		}
	}
	{
		Cell_t964604196 * L_310 = V_51;
		List_1_t333725328 * L_311 = Cell_get_blocks_m153328201(L_310, /*hidden argument*/NULL);
		Cell_t964604196 * L_312 = V_59;
		List_1_Add_m1728370848(L_311, L_312, /*hidden argument*/List_1_Add_m1728370848_RuntimeMethod_var);
	}

IL_0685:
	{
		CellU5BU2CU2CU5D_t1218597327* L_313 = __this->get_cells_2();
		Cell_t964604196 * L_314 = V_51;
		int32_t L_315 = Cell_get_x_m2406521465(L_314, /*hidden argument*/NULL);
		Cell_t964604196 * L_316 = V_51;
		int32_t L_317 = Cell_get_y_m2406521560(L_316, /*hidden argument*/NULL);
		Cell_t964604196 * L_318 = V_51;
		int32_t L_319 = Cell_get_z_m2406521399(L_318, /*hidden argument*/NULL);
		Cell_t964604196 * L_320 = ((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_313)->GetAtUnchecked(((int32_t)((int32_t)L_315+(int32_t)1)), L_317, L_319);
		Cell_t964604196 * L_321 = L_320;
		V_59 = L_321;
		if (!L_321)
		{
			goto IL_06c9;
		}
	}
	{
		Cell_t964604196 * L_322 = V_59;
		bool L_323 = Cell_get_isBlock_m3829309610(L_322, /*hidden argument*/NULL);
		if (!L_323)
		{
			goto IL_06c9;
		}
	}
	{
		Cell_t964604196 * L_324 = V_51;
		List_1_t333725328 * L_325 = Cell_get_blocks_m153328201(L_324, /*hidden argument*/NULL);
		Cell_t964604196 * L_326 = V_59;
		List_1_Add_m1728370848(L_325, L_326, /*hidden argument*/List_1_Add_m1728370848_RuntimeMethod_var);
	}

IL_06c9:
	{
		CellU5BU2CU2CU5D_t1218597327* L_327 = __this->get_cells_2();
		Cell_t964604196 * L_328 = V_51;
		int32_t L_329 = Cell_get_x_m2406521465(L_328, /*hidden argument*/NULL);
		Cell_t964604196 * L_330 = V_51;
		int32_t L_331 = Cell_get_y_m2406521560(L_330, /*hidden argument*/NULL);
		Cell_t964604196 * L_332 = V_51;
		int32_t L_333 = Cell_get_z_m2406521399(L_332, /*hidden argument*/NULL);
		Cell_t964604196 * L_334 = ((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_327)->GetAtUnchecked(L_329, ((int32_t)((int32_t)L_331-(int32_t)1)), L_333);
		Cell_t964604196 * L_335 = L_334;
		V_59 = L_335;
		if (!L_335)
		{
			goto IL_070d;
		}
	}
	{
		Cell_t964604196 * L_336 = V_59;
		bool L_337 = Cell_get_isBlock_m3829309610(L_336, /*hidden argument*/NULL);
		if (!L_337)
		{
			goto IL_070d;
		}
	}
	{
		Cell_t964604196 * L_338 = V_51;
		List_1_t333725328 * L_339 = Cell_get_blocks_m153328201(L_338, /*hidden argument*/NULL);
		Cell_t964604196 * L_340 = V_59;
		List_1_Add_m1728370848(L_339, L_340, /*hidden argument*/List_1_Add_m1728370848_RuntimeMethod_var);
	}

IL_070d:
	{
		CellU5BU2CU2CU5D_t1218597327* L_341 = __this->get_cells_2();
		Cell_t964604196 * L_342 = V_51;
		int32_t L_343 = Cell_get_x_m2406521465(L_342, /*hidden argument*/NULL);
		Cell_t964604196 * L_344 = V_51;
		int32_t L_345 = Cell_get_y_m2406521560(L_344, /*hidden argument*/NULL);
		Cell_t964604196 * L_346 = V_51;
		int32_t L_347 = Cell_get_z_m2406521399(L_346, /*hidden argument*/NULL);
		Cell_t964604196 * L_348 = ((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_341)->GetAtUnchecked(L_343, ((int32_t)((int32_t)L_345+(int32_t)1)), L_347);
		Cell_t964604196 * L_349 = L_348;
		V_59 = L_349;
		if (!L_349)
		{
			goto IL_0751;
		}
	}
	{
		Cell_t964604196 * L_350 = V_59;
		bool L_351 = Cell_get_isBlock_m3829309610(L_350, /*hidden argument*/NULL);
		if (!L_351)
		{
			goto IL_0751;
		}
	}
	{
		Cell_t964604196 * L_352 = V_51;
		List_1_t333725328 * L_353 = Cell_get_blocks_m153328201(L_352, /*hidden argument*/NULL);
		Cell_t964604196 * L_354 = V_59;
		List_1_Add_m1728370848(L_353, L_354, /*hidden argument*/List_1_Add_m1728370848_RuntimeMethod_var);
	}

IL_0751:
	{
		CellU5BU2CU2CU5D_t1218597327* L_355 = __this->get_cells_2();
		Cell_t964604196 * L_356 = V_51;
		int32_t L_357 = Cell_get_x_m2406521465(L_356, /*hidden argument*/NULL);
		Cell_t964604196 * L_358 = V_51;
		int32_t L_359 = Cell_get_y_m2406521560(L_358, /*hidden argument*/NULL);
		Cell_t964604196 * L_360 = V_51;
		int32_t L_361 = Cell_get_z_m2406521399(L_360, /*hidden argument*/NULL);
		Cell_t964604196 * L_362 = ((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_355)->GetAtUnchecked(L_357, L_359, ((int32_t)((int32_t)L_361-(int32_t)1)));
		Cell_t964604196 * L_363 = L_362;
		V_59 = L_363;
		if (!L_363)
		{
			goto IL_0795;
		}
	}
	{
		Cell_t964604196 * L_364 = V_59;
		bool L_365 = Cell_get_isBlock_m3829309610(L_364, /*hidden argument*/NULL);
		if (!L_365)
		{
			goto IL_0795;
		}
	}
	{
		Cell_t964604196 * L_366 = V_51;
		List_1_t333725328 * L_367 = Cell_get_blocks_m153328201(L_366, /*hidden argument*/NULL);
		Cell_t964604196 * L_368 = V_59;
		List_1_Add_m1728370848(L_367, L_368, /*hidden argument*/List_1_Add_m1728370848_RuntimeMethod_var);
	}

IL_0795:
	{
		CellU5BU2CU2CU5D_t1218597327* L_369 = __this->get_cells_2();
		Cell_t964604196 * L_370 = V_51;
		int32_t L_371 = Cell_get_x_m2406521465(L_370, /*hidden argument*/NULL);
		Cell_t964604196 * L_372 = V_51;
		int32_t L_373 = Cell_get_y_m2406521560(L_372, /*hidden argument*/NULL);
		Cell_t964604196 * L_374 = V_51;
		int32_t L_375 = Cell_get_z_m2406521399(L_374, /*hidden argument*/NULL);
		Cell_t964604196 * L_376 = ((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_369)->GetAtUnchecked(L_371, L_373, ((int32_t)((int32_t)L_375+(int32_t)1)));
		Cell_t964604196 * L_377 = L_376;
		V_59 = L_377;
		if (!L_377)
		{
			goto IL_07d9;
		}
	}
	{
		Cell_t964604196 * L_378 = V_59;
		bool L_379 = Cell_get_isBlock_m3829309610(L_378, /*hidden argument*/NULL);
		if (!L_379)
		{
			goto IL_07d9;
		}
	}
	{
		Cell_t964604196 * L_380 = V_51;
		List_1_t333725328 * L_381 = Cell_get_blocks_m153328201(L_380, /*hidden argument*/NULL);
		Cell_t964604196 * L_382 = V_59;
		List_1_Add_m1728370848(L_381, L_382, /*hidden argument*/List_1_Add_m1728370848_RuntimeMethod_var);
	}

IL_07d9:
	{
		int32_t L_383 = V_58;
		V_58 = ((int32_t)((int32_t)L_383+(int32_t)1));
	}

IL_07df:
	{
		int32_t L_384 = V_58;
		int32_t L_385 = V_55;
		if ((((int32_t)L_384) < ((int32_t)L_385)))
		{
			goto IL_060e;
		}
	}
	{
		int32_t L_386 = V_57;
		V_57 = ((int32_t)((int32_t)L_386+(int32_t)1));
	}

IL_07ee:
	{
		int32_t L_387 = V_57;
		int32_t L_388 = V_54;
		if ((((int32_t)L_387) < ((int32_t)L_388)))
		{
			goto IL_0606;
		}
	}
	{
		int32_t L_389 = V_56;
		V_56 = ((int32_t)((int32_t)L_389+(int32_t)1));
	}

IL_07fd:
	{
		int32_t L_390 = V_56;
		int32_t L_391 = V_53;
		if ((((int32_t)L_390) < ((int32_t)L_391)))
		{
			goto IL_05fe;
		}
	}
	{
		CellU5BU2CU2CU5D_t1218597327* L_392 = __this->get_cells_2();
		V_61 = (CellU5BU2CU2CU5D_t1218597327*)L_392;
		CellU5BU2CU2CU5D_t1218597327* L_393 = V_61;
		int32_t L_394 = Array_GetLength_m2083296647((RuntimeArray *)(RuntimeArray *)L_393, 0, /*hidden argument*/NULL);
		V_62 = L_394;
		CellU5BU2CU2CU5D_t1218597327* L_395 = V_61;
		int32_t L_396 = Array_GetLength_m2083296647((RuntimeArray *)(RuntimeArray *)L_395, 1, /*hidden argument*/NULL);
		V_63 = L_396;
		CellU5BU2CU2CU5D_t1218597327* L_397 = V_61;
		int32_t L_398 = Array_GetLength_m2083296647((RuntimeArray *)(RuntimeArray *)L_397, 2, /*hidden argument*/NULL);
		V_64 = L_398;
		V_65 = 0;
		goto IL_08e3;
	}

IL_0834:
	{
		V_66 = 0;
		goto IL_08d4;
	}

IL_083c:
	{
		V_67 = 0;
		goto IL_08c5;
	}

IL_0844:
	{
		CellU5BU2CU2CU5D_t1218597327* L_399 = V_61;
		int32_t L_400 = V_65;
		int32_t L_401 = V_66;
		int32_t L_402 = V_67;
		Cell_t964604196 * L_403 = (L_399)->GetAtUnchecked(L_400, L_401, L_402);
		V_60 = L_403;
		Cell_t964604196 * L_404 = V_60;
		if (!L_404)
		{
			goto IL_0866;
		}
	}
	{
		Cell_t964604196 * L_405 = V_60;
		bool L_406 = Cell_get_isBlock_m3829309610(L_405, /*hidden argument*/NULL);
		if (!L_406)
		{
			goto IL_086b;
		}
	}

IL_0866:
	{
		goto IL_08bf;
	}

IL_086b:
	{
		Cell_t964604196 * L_407 = V_60;
		List_1_t333725328 * L_408 = Level_adjacentCells_m69322768(__this, L_407, (bool)0, /*hidden argument*/NULL);
		Enumerator_t4163422298  L_409 = List_1_GetEnumerator_m174512497(L_408, /*hidden argument*/List_1_GetEnumerator_m174512497_RuntimeMethod_var);
		V_69 = L_409;
	}

IL_087b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_08a0;
		}

IL_0880:
		{
			Cell_t964604196 * L_410 = Enumerator_get_Current_m4246516629((&V_69), /*hidden argument*/Enumerator_get_Current_m4246516629_RuntimeMethod_var);
			V_68 = L_410;
			Cell_t964604196 * L_411 = V_68;
			bool L_412 = Cell_get_isBlock_m3829309610(L_411, /*hidden argument*/NULL);
			if (L_412)
			{
				goto IL_08a0;
			}
		}

IL_0895:
		{
			Cell_t964604196 * L_413 = V_60;
			Cell_t964604196 * L_414 = V_68;
			Level_createLink_m2520085500(__this, L_413, L_414, (bool)0, /*hidden argument*/NULL);
		}

IL_08a0:
		{
			bool L_415 = Enumerator_MoveNext_m1435586957((&V_69), /*hidden argument*/Enumerator_MoveNext_m1435586957_RuntimeMethod_var);
			if (L_415)
			{
				goto IL_0880;
			}
		}

IL_08ac:
		{
			IL2CPP_LEAVE(0x8BF, FINALLY_08b1);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_08b1;
	}

FINALLY_08b1:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3632502891((&V_69), /*hidden argument*/Enumerator_Dispose_m3632502891_RuntimeMethod_var);
		IL2CPP_END_FINALLY(2225)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(2225)
	{
		IL2CPP_JUMP_TBL(0x8BF, IL_08bf)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_08bf:
	{
		int32_t L_416 = V_67;
		V_67 = ((int32_t)((int32_t)L_416+(int32_t)1));
	}

IL_08c5:
	{
		int32_t L_417 = V_67;
		int32_t L_418 = V_64;
		if ((((int32_t)L_417) < ((int32_t)L_418)))
		{
			goto IL_0844;
		}
	}
	{
		int32_t L_419 = V_66;
		V_66 = ((int32_t)((int32_t)L_419+(int32_t)1));
	}

IL_08d4:
	{
		int32_t L_420 = V_66;
		int32_t L_421 = V_63;
		if ((((int32_t)L_420) < ((int32_t)L_421)))
		{
			goto IL_083c;
		}
	}
	{
		int32_t L_422 = V_65;
		V_65 = ((int32_t)((int32_t)L_422+(int32_t)1));
	}

IL_08e3:
	{
		int32_t L_423 = V_65;
		int32_t L_424 = V_62;
		if ((((int32_t)L_423) < ((int32_t)L_424)))
		{
			goto IL_0834;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.List`1<Assets.scripts.Cell> Level::adjacentCells(Assets.scripts.Cell,System.Boolean)
extern "C"  List_1_t333725328 * Level_adjacentCells_m69322768 (Level_t866853782 * __this, Cell_t964604196 * ___cell0, bool ___direct1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Level_adjacentCells_m69322768_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t333725328 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		List_1_t333725328 * L_0 = (List_1_t333725328 *)il2cpp_codegen_object_new(List_1_t333725328_il2cpp_TypeInfo_var);
		List_1__ctor_m2909086300(L_0, /*hidden argument*/List_1__ctor_m2909086300_RuntimeMethod_var);
		V_0 = L_0;
		V_1 = (-1);
		goto IL_00c4;
	}

IL_000d:
	{
		V_2 = (-1);
		goto IL_00b9;
	}

IL_0014:
	{
		V_3 = (-1);
		goto IL_00ae;
	}

IL_001b:
	{
		bool L_1 = ___direct1;
		if (!L_1)
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_2 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_3 = Mathf_Abs_m722236531(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		int32_t L_4 = V_2;
		int32_t L_5 = Mathf_Abs_m722236531(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		int32_t L_6 = V_3;
		int32_t L_7 = Mathf_Abs_m722236531(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_3+(int32_t)L_5))+(int32_t)L_7))) == ((int32_t)1)))
		{
			goto IL_0077;
		}
	}

IL_003b:
	{
		bool L_8 = ___direct1;
		if (L_8)
		{
			goto IL_00aa;
		}
	}
	{
		int32_t L_9 = V_1;
		if (L_9)
		{
			goto IL_0053;
		}
	}
	{
		int32_t L_10 = V_2;
		if (L_10)
		{
			goto IL_0053;
		}
	}
	{
		int32_t L_11 = V_3;
		if (!L_11)
		{
			goto IL_00aa;
		}
	}

IL_0053:
	{
		int32_t L_12 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_13 = Mathf_Abs_m722236531(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_13) == ((uint32_t)1))))
		{
			goto IL_0077;
		}
	}
	{
		int32_t L_14 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_15 = Mathf_Abs_m722236531(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_15) == ((uint32_t)1))))
		{
			goto IL_0077;
		}
	}
	{
		int32_t L_16 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_17 = Mathf_Abs_m722236531(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_17) == ((int32_t)1)))
		{
			goto IL_00aa;
		}
	}

IL_0077:
	{
		Cell_t964604196 * L_18 = ___cell0;
		Vector3_t2243707580  L_19 = Cell_get_pos_m113374293(L_18, /*hidden argument*/NULL);
		int32_t L_20 = V_1;
		int32_t L_21 = V_2;
		int32_t L_22 = V_3;
		Vector3_t2243707580  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Vector3__ctor_m1555724485((&L_23), (((float)((float)L_20))), (((float)((float)L_21))), (((float)((float)L_22))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_24 = Vector3_op_Addition_m394909128(NULL /*static, unused*/, L_19, L_23, /*hidden argument*/NULL);
		V_4 = L_24;
		Vector3_t2243707580  L_25 = V_4;
		Cell_t964604196 * L_26 = Level_cellAtPosition_m891825944(__this, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00aa;
		}
	}
	{
		List_1_t333725328 * L_27 = V_0;
		Vector3_t2243707580  L_28 = V_4;
		Cell_t964604196 * L_29 = Level_cellAtPosition_m891825944(__this, L_28, /*hidden argument*/NULL);
		List_1_Add_m1728370848(L_27, L_29, /*hidden argument*/List_1_Add_m1728370848_RuntimeMethod_var);
	}

IL_00aa:
	{
		int32_t L_30 = V_3;
		V_3 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_00ae:
	{
		int32_t L_31 = V_3;
		if ((((int32_t)L_31) <= ((int32_t)1)))
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_32 = V_2;
		V_2 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00b9:
	{
		int32_t L_33 = V_2;
		if ((((int32_t)L_33) <= ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_34 = V_1;
		V_1 = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_00c4:
	{
		int32_t L_35 = V_1;
		if ((((int32_t)L_35) <= ((int32_t)1)))
		{
			goto IL_000d;
		}
	}
	{
		List_1_t333725328 * L_36 = V_0;
		return L_36;
	}
}
// System.Void Level::addBlock(UnityEngine.Vector3)
extern "C"  void Level_addBlock_m1332986222 (Level_t866853782 * __this, Vector3_t2243707580  ___pos0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Level_addBlock_m1332986222_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Cell_t964604196 * V_0 = NULL;
	Cell_t964604196 * V_1 = NULL;
	Enumerator_t4163422298  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t V_3 = 0;
	Link_t776645742 * V_4 = NULL;
	Cell_t964604196 * V_5 = NULL;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	Cell_t964604196 * V_9 = NULL;
	List_1_t333725328 * V_10 = NULL;
	Cell_t964604196 * V_11 = NULL;
	Enumerator_t4163422298  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Cell_t964604196 * V_13 = NULL;
	Enumerator_t4163422298  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Vector3_t2243707580  L_0 = ___pos0;
		Cell_t964604196 * L_1 = Level_cellAtPosition_m891825944(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_009f;
		}
	}
	{
		Vector3_t2243707580  L_2 = ___pos0;
		Cell_t964604196 * L_3 = Level_cellAtPosition_m891825944(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Cell_t964604196 * L_4 = V_0;
		bool L_5 = Cell_get_isBlock_m3829309610(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0020;
		}
	}
	{
		return;
	}

IL_0020:
	{
		Cell_t964604196 * L_6 = V_0;
		List_1_t333725328 * L_7 = Level_adjacentCells_m69322768(__this, L_6, (bool)0, /*hidden argument*/NULL);
		Enumerator_t4163422298  L_8 = List_1_GetEnumerator_m174512497(L_7, /*hidden argument*/List_1_GetEnumerator_m174512497_RuntimeMethod_var);
		V_2 = L_8;
	}

IL_002e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0080;
		}

IL_0033:
		{
			Cell_t964604196 * L_9 = Enumerator_get_Current_m4246516629((&V_2), /*hidden argument*/Enumerator_get_Current_m4246516629_RuntimeMethod_var);
			V_1 = L_9;
			Cell_t964604196 * L_10 = V_1;
			List_1_t145766874 * L_11 = Cell_get_links_m2033144484(L_10, /*hidden argument*/NULL);
			int32_t L_12 = List_1_get_Count_m3786137094(L_11, /*hidden argument*/List_1_get_Count_m3786137094_RuntimeMethod_var);
			V_3 = ((int32_t)((int32_t)L_12-(int32_t)1));
			goto IL_0079;
		}

IL_004e:
		{
			Cell_t964604196 * L_13 = V_1;
			List_1_t145766874 * L_14 = Cell_get_links_m2033144484(L_13, /*hidden argument*/NULL);
			int32_t L_15 = V_3;
			Link_t776645742 * L_16 = List_1_get_Item_m914633333(L_14, L_15, /*hidden argument*/List_1_get_Item_m914633333_RuntimeMethod_var);
			V_4 = L_16;
			Link_t776645742 * L_17 = V_4;
			Cell_t964604196 * L_18 = L_17->get_second_4();
			Cell_t964604196 * L_19 = V_0;
			if ((!(((RuntimeObject*)(Cell_t964604196 *)L_18) == ((RuntimeObject*)(Cell_t964604196 *)L_19))))
			{
				goto IL_0075;
			}
		}

IL_0069:
		{
			Cell_t964604196 * L_20 = V_1;
			List_1_t145766874 * L_21 = Cell_get_links_m2033144484(L_20, /*hidden argument*/NULL);
			int32_t L_22 = V_3;
			List_1_RemoveAt_m2271034946(L_21, L_22, /*hidden argument*/List_1_RemoveAt_m2271034946_RuntimeMethod_var);
		}

IL_0075:
		{
			int32_t L_23 = V_3;
			V_3 = ((int32_t)((int32_t)L_23-(int32_t)1));
		}

IL_0079:
		{
			int32_t L_24 = V_3;
			if ((((int32_t)L_24) >= ((int32_t)0)))
			{
				goto IL_004e;
			}
		}

IL_0080:
		{
			bool L_25 = Enumerator_MoveNext_m1435586957((&V_2), /*hidden argument*/Enumerator_MoveNext_m1435586957_RuntimeMethod_var);
			if (L_25)
			{
				goto IL_0033;
			}
		}

IL_008c:
		{
			IL2CPP_LEAVE(0x9F, FINALLY_0091);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0091;
	}

FINALLY_0091:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3632502891((&V_2), /*hidden argument*/Enumerator_Dispose_m3632502891_RuntimeMethod_var);
		IL2CPP_END_FINALLY(145)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(145)
	{
		IL2CPP_JUMP_TBL(0x9F, IL_009f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_009f:
	{
		float L_26 = (&___pos0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_27 = bankers_roundf(L_26);
		float L_28 = (&___pos0)->get_y_2();
		float L_29 = bankers_roundf(L_28);
		float L_30 = (&___pos0)->get_z_3();
		float L_31 = bankers_roundf(L_30);
		Cell_t964604196 * L_32 = (Cell_t964604196 *)il2cpp_codegen_object_new(Cell_t964604196_il2cpp_TypeInfo_var);
		Cell__ctor_m2554603460(L_32, (((int32_t)((int32_t)L_27))), (((int32_t)((int32_t)L_29))), (((int32_t)((int32_t)L_31))), (bool)1, /*hidden argument*/NULL);
		V_5 = L_32;
		CellU5BU2CU2CU5D_t1218597327* L_33 = __this->get_cells_2();
		Cell_t964604196 * L_34 = V_5;
		int32_t L_35 = Cell_get_x_m2406521465(L_34, /*hidden argument*/NULL);
		Cell_t964604196 * L_36 = V_5;
		int32_t L_37 = Cell_get_y_m2406521560(L_36, /*hidden argument*/NULL);
		Cell_t964604196 * L_38 = V_5;
		int32_t L_39 = Cell_get_z_m2406521399(L_38, /*hidden argument*/NULL);
		Cell_t964604196 * L_40 = V_5;
		((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_33)->SetAtUnchecked(L_35, L_37, L_39, L_40);
		V_6 = (-1);
		goto IL_0207;
	}

IL_00f8:
	{
		V_7 = (-1);
		goto IL_01f9;
	}

IL_0100:
	{
		V_8 = (-1);
		goto IL_01eb;
	}

IL_0108:
	{
		int32_t L_41 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_42 = Mathf_Abs_m722236531(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		int32_t L_43 = V_7;
		int32_t L_44 = Mathf_Abs_m722236531(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		int32_t L_45 = V_8;
		int32_t L_46 = Mathf_Abs_m722236531(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		if ((!(((uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_42+(int32_t)L_44))+(int32_t)L_46))) == ((uint32_t)1))))
		{
			goto IL_01e5;
		}
	}
	{
		CellU5BU2CU2CU5D_t1218597327* L_47 = __this->get_cells_2();
		Cell_t964604196 * L_48 = V_5;
		int32_t L_49 = Cell_get_x_m2406521465(L_48, /*hidden argument*/NULL);
		int32_t L_50 = V_6;
		Cell_t964604196 * L_51 = V_5;
		int32_t L_52 = Cell_get_y_m2406521560(L_51, /*hidden argument*/NULL);
		int32_t L_53 = V_7;
		Cell_t964604196 * L_54 = V_5;
		int32_t L_55 = Cell_get_z_m2406521399(L_54, /*hidden argument*/NULL);
		int32_t L_56 = V_8;
		Cell_t964604196 * L_57 = ((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_47)->GetAtUnchecked(((int32_t)((int32_t)L_49+(int32_t)L_50)), ((int32_t)((int32_t)L_52+(int32_t)L_53)), ((int32_t)((int32_t)L_55+(int32_t)L_56)));
		if (L_57)
		{
			goto IL_01a0;
		}
	}
	{
		CellU5BU2CU2CU5D_t1218597327* L_58 = __this->get_cells_2();
		Cell_t964604196 * L_59 = V_5;
		int32_t L_60 = Cell_get_x_m2406521465(L_59, /*hidden argument*/NULL);
		int32_t L_61 = V_6;
		Cell_t964604196 * L_62 = V_5;
		int32_t L_63 = Cell_get_y_m2406521560(L_62, /*hidden argument*/NULL);
		int32_t L_64 = V_7;
		Cell_t964604196 * L_65 = V_5;
		int32_t L_66 = Cell_get_z_m2406521399(L_65, /*hidden argument*/NULL);
		int32_t L_67 = V_8;
		Cell_t964604196 * L_68 = V_5;
		int32_t L_69 = Cell_get_x_m2406521465(L_68, /*hidden argument*/NULL);
		int32_t L_70 = V_6;
		Cell_t964604196 * L_71 = V_5;
		int32_t L_72 = Cell_get_y_m2406521560(L_71, /*hidden argument*/NULL);
		int32_t L_73 = V_7;
		Cell_t964604196 * L_74 = V_5;
		int32_t L_75 = Cell_get_z_m2406521399(L_74, /*hidden argument*/NULL);
		int32_t L_76 = V_8;
		Cell_t964604196 * L_77 = (Cell_t964604196 *)il2cpp_codegen_object_new(Cell_t964604196_il2cpp_TypeInfo_var);
		Cell__ctor_m2554603460(L_77, ((int32_t)((int32_t)L_69+(int32_t)L_70)), ((int32_t)((int32_t)L_72+(int32_t)L_73)), ((int32_t)((int32_t)L_75+(int32_t)L_76)), (bool)0, /*hidden argument*/NULL);
		((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_58)->SetAtUnchecked(((int32_t)((int32_t)L_60+(int32_t)L_61)), ((int32_t)((int32_t)L_63+(int32_t)L_64)), ((int32_t)((int32_t)L_66+(int32_t)L_67)), L_77);
	}

IL_01a0:
	{
		CellU5BU2CU2CU5D_t1218597327* L_78 = __this->get_cells_2();
		Cell_t964604196 * L_79 = V_5;
		int32_t L_80 = Cell_get_x_m2406521465(L_79, /*hidden argument*/NULL);
		int32_t L_81 = V_6;
		Cell_t964604196 * L_82 = V_5;
		int32_t L_83 = Cell_get_y_m2406521560(L_82, /*hidden argument*/NULL);
		int32_t L_84 = V_7;
		Cell_t964604196 * L_85 = V_5;
		int32_t L_86 = Cell_get_z_m2406521399(L_85, /*hidden argument*/NULL);
		int32_t L_87 = V_8;
		Cell_t964604196 * L_88 = ((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_78)->GetAtUnchecked(((int32_t)((int32_t)L_80+(int32_t)L_81)), ((int32_t)((int32_t)L_83+(int32_t)L_84)), ((int32_t)((int32_t)L_86+(int32_t)L_87)));
		V_9 = L_88;
		Cell_t964604196 * L_89 = V_9;
		bool L_90 = Cell_get_isBlock_m3829309610(L_89, /*hidden argument*/NULL);
		if (L_90)
		{
			goto IL_01e5;
		}
	}
	{
		Cell_t964604196 * L_91 = V_9;
		List_1_t333725328 * L_92 = Cell_get_blocks_m153328201(L_91, /*hidden argument*/NULL);
		Cell_t964604196 * L_93 = V_5;
		List_1_Add_m1728370848(L_92, L_93, /*hidden argument*/List_1_Add_m1728370848_RuntimeMethod_var);
	}

IL_01e5:
	{
		int32_t L_94 = V_8;
		V_8 = ((int32_t)((int32_t)L_94+(int32_t)1));
	}

IL_01eb:
	{
		int32_t L_95 = V_8;
		if ((((int32_t)L_95) <= ((int32_t)1)))
		{
			goto IL_0108;
		}
	}
	{
		int32_t L_96 = V_7;
		V_7 = ((int32_t)((int32_t)L_96+(int32_t)1));
	}

IL_01f9:
	{
		int32_t L_97 = V_7;
		if ((((int32_t)L_97) <= ((int32_t)1)))
		{
			goto IL_0100;
		}
	}
	{
		int32_t L_98 = V_6;
		V_6 = ((int32_t)((int32_t)L_98+(int32_t)1));
	}

IL_0207:
	{
		int32_t L_99 = V_6;
		if ((((int32_t)L_99) <= ((int32_t)1)))
		{
			goto IL_00f8;
		}
	}
	{
		Cell_t964604196 * L_100 = V_5;
		List_1_t333725328 * L_101 = Level_adjacentCells_m69322768(__this, L_100, (bool)0, /*hidden argument*/NULL);
		V_10 = L_101;
		List_1_t333725328 * L_102 = V_10;
		Enumerator_t4163422298  L_103 = List_1_GetEnumerator_m174512497(L_102, /*hidden argument*/List_1_GetEnumerator_m174512497_RuntimeMethod_var);
		V_12 = L_103;
	}

IL_0223:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0293;
		}

IL_0228:
		{
			Cell_t964604196 * L_104 = Enumerator_get_Current_m4246516629((&V_12), /*hidden argument*/Enumerator_get_Current_m4246516629_RuntimeMethod_var);
			V_11 = L_104;
			Cell_t964604196 * L_105 = V_11;
			bool L_106 = Cell_get_isBlock_m3829309610(L_105, /*hidden argument*/NULL);
			if (L_106)
			{
				goto IL_0293;
			}
		}

IL_023d:
		{
			List_1_t333725328 * L_107 = V_10;
			Enumerator_t4163422298  L_108 = List_1_GetEnumerator_m174512497(L_107, /*hidden argument*/List_1_GetEnumerator_m174512497_RuntimeMethod_var);
			V_14 = L_108;
		}

IL_0246:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0274;
			}

IL_024b:
			{
				Cell_t964604196 * L_109 = Enumerator_get_Current_m4246516629((&V_14), /*hidden argument*/Enumerator_get_Current_m4246516629_RuntimeMethod_var);
				V_13 = L_109;
				Cell_t964604196 * L_110 = V_13;
				bool L_111 = Cell_get_isBlock_m3829309610(L_110, /*hidden argument*/NULL);
				if (L_111)
				{
					goto IL_0274;
				}
			}

IL_0260:
			{
				Cell_t964604196 * L_112 = V_11;
				Cell_t964604196 * L_113 = V_13;
				if ((((RuntimeObject*)(Cell_t964604196 *)L_112) == ((RuntimeObject*)(Cell_t964604196 *)L_113)))
				{
					goto IL_0274;
				}
			}

IL_0269:
			{
				Cell_t964604196 * L_114 = V_11;
				Cell_t964604196 * L_115 = V_13;
				Level_createLink_m2520085500(__this, L_114, L_115, (bool)1, /*hidden argument*/NULL);
			}

IL_0274:
			{
				bool L_116 = Enumerator_MoveNext_m1435586957((&V_14), /*hidden argument*/Enumerator_MoveNext_m1435586957_RuntimeMethod_var);
				if (L_116)
				{
					goto IL_024b;
				}
			}

IL_0280:
			{
				IL2CPP_LEAVE(0x293, FINALLY_0285);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_0285;
		}

FINALLY_0285:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m3632502891((&V_14), /*hidden argument*/Enumerator_Dispose_m3632502891_RuntimeMethod_var);
			IL2CPP_END_FINALLY(645)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(645)
		{
			IL2CPP_JUMP_TBL(0x293, IL_0293)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_0293:
		{
			bool L_117 = Enumerator_MoveNext_m1435586957((&V_12), /*hidden argument*/Enumerator_MoveNext_m1435586957_RuntimeMethod_var);
			if (L_117)
			{
				goto IL_0228;
			}
		}

IL_029f:
		{
			IL2CPP_LEAVE(0x2B2, FINALLY_02a4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_02a4;
	}

FINALLY_02a4:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3632502891((&V_12), /*hidden argument*/Enumerator_Dispose_m3632502891_RuntimeMethod_var);
		IL2CPP_END_FINALLY(676)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(676)
	{
		IL2CPP_JUMP_TBL(0x2B2, IL_02b2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_02b2:
	{
		return;
	}
}
// System.Void Level::createLink(Assets.scripts.Cell,Assets.scripts.Cell,System.Boolean)
extern "C"  void Level_createLink_m2520085500 (Level_t866853782 * __this, Cell_t964604196 * ___cell10, Cell_t964604196 * ___cell21, bool ___twofold2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Level_createLink_m2520085500_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t1147558386  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Enumerator_t1147558386  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	bool V_6 = false;
	Cell_t964604196 * V_7 = NULL;
	Cell_t964604196 * V_8 = NULL;
	Cell_t964604196 * V_9 = NULL;
	Enumerator_t4163422298  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Enumerator_t1147558386  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Cell_t964604196 * V_14 = NULL;
	Enumerator_t4163422298  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector3_t2243707580  V_16;
	memset(&V_16, 0, sizeof(V_16));
	Enumerator_t1147558386  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Vector3_t2243707580  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Vector3_t2243707580  V_19;
	memset(&V_19, 0, sizeof(V_19));
	Vector3_t2243707580  V_20;
	memset(&V_20, 0, sizeof(V_20));
	Vector3_t2243707580  V_21;
	memset(&V_21, 0, sizeof(V_21));
	Link_t776645742 * V_22 = NULL;
	Link_t776645742 * V_23 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Link_t776645742 * G_B34_0 = NULL;
	Link_t776645742 * G_B33_0 = NULL;
	int32_t G_B35_0 = 0;
	Link_t776645742 * G_B35_1 = NULL;
	{
		V_0 = (0.05f);
		Cell_t964604196 * L_0 = ___cell10;
		List_1_t1612828712 * L_1 = Cell_get_midPoints_m2313462662(L_0, /*hidden argument*/NULL);
		Enumerator_t1147558386  L_2 = List_1_GetEnumerator_m940839541(L_1, /*hidden argument*/List_1_GetEnumerator_m940839541_RuntimeMethod_var);
		V_2 = L_2;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_02aa;
		}

IL_0017:
		{
			Vector3_t2243707580  L_3 = Enumerator_get_Current_m857008049((&V_2), /*hidden argument*/Enumerator_get_Current_m857008049_RuntimeMethod_var);
			V_1 = L_3;
			Cell_t964604196 * L_4 = ___cell21;
			List_1_t1612828712 * L_5 = Cell_get_midPoints_m2313462662(L_4, /*hidden argument*/NULL);
			Enumerator_t1147558386  L_6 = List_1_GetEnumerator_m940839541(L_5, /*hidden argument*/List_1_GetEnumerator_m940839541_RuntimeMethod_var);
			V_4 = L_6;
		}

IL_002c:
		try
		{ // begin try (depth: 2)
			{
				goto IL_028b;
			}

IL_0031:
			{
				Vector3_t2243707580  L_7 = Enumerator_get_Current_m857008049((&V_4), /*hidden argument*/Enumerator_get_Current_m857008049_RuntimeMethod_var);
				V_3 = L_7;
				Vector3_t2243707580  L_8 = V_1;
				Vector3_t2243707580  L_9 = V_3;
				IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
				Vector3_t2243707580  L_10 = Vector3_op_Subtraction_m4046047256(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
				V_5 = L_10;
				float L_11 = Vector3_get_sqrMagnitude_m1445517847((&V_5), /*hidden argument*/NULL);
				float L_12 = V_0;
				if ((!(((float)L_11) < ((float)L_12))))
				{
					goto IL_028b;
				}
			}

IL_004f:
			{
				V_6 = (bool)0;
				V_7 = (Cell_t964604196 *)NULL;
				V_8 = (Cell_t964604196 *)NULL;
				Cell_t964604196 * L_13 = ___cell10;
				List_1_t333725328 * L_14 = Cell_get_blocks_m153328201(L_13, /*hidden argument*/NULL);
				Enumerator_t4163422298  L_15 = List_1_GetEnumerator_m174512497(L_14, /*hidden argument*/List_1_GetEnumerator_m174512497_RuntimeMethod_var);
				V_10 = L_15;
			}

IL_0065:
			try
			{ // begin try (depth: 3)
				{
					goto IL_00d1;
				}

IL_006a:
				{
					Cell_t964604196 * L_16 = Enumerator_get_Current_m4246516629((&V_10), /*hidden argument*/Enumerator_get_Current_m4246516629_RuntimeMethod_var);
					V_9 = L_16;
					Cell_t964604196 * L_17 = V_9;
					List_1_t1612828712 * L_18 = Cell_get_midPoints_m2313462662(L_17, /*hidden argument*/NULL);
					Enumerator_t1147558386  L_19 = List_1_GetEnumerator_m940839541(L_18, /*hidden argument*/List_1_GetEnumerator_m940839541_RuntimeMethod_var);
					V_12 = L_19;
				}

IL_0081:
				try
				{ // begin try (depth: 4)
					{
						goto IL_00b2;
					}

IL_0086:
					{
						Vector3_t2243707580  L_20 = Enumerator_get_Current_m857008049((&V_12), /*hidden argument*/Enumerator_get_Current_m857008049_RuntimeMethod_var);
						V_11 = L_20;
						Vector3_t2243707580  L_21 = V_11;
						Vector3_t2243707580  L_22 = V_1;
						IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
						Vector3_t2243707580  L_23 = Vector3_op_Subtraction_m4046047256(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
						V_13 = L_23;
						float L_24 = Vector3_get_sqrMagnitude_m1445517847((&V_13), /*hidden argument*/NULL);
						float L_25 = V_0;
						if ((!(((float)L_24) < ((float)L_25))))
						{
							goto IL_00b2;
						}
					}

IL_00a6:
					{
						Cell_t964604196 * L_26 = V_9;
						V_7 = L_26;
						V_6 = (bool)1;
						goto IL_00be;
					}

IL_00b2:
					{
						bool L_27 = Enumerator_MoveNext_m2007266881((&V_12), /*hidden argument*/Enumerator_MoveNext_m2007266881_RuntimeMethod_var);
						if (L_27)
						{
							goto IL_0086;
						}
					}

IL_00be:
					{
						IL2CPP_LEAVE(0xD1, FINALLY_00c3);
					}
				} // end try (depth: 4)
				catch(Il2CppExceptionWrapper& e)
				{
					__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
					goto FINALLY_00c3;
				}

FINALLY_00c3:
				{ // begin finally (depth: 4)
					Enumerator_Dispose_m3215924523((&V_12), /*hidden argument*/Enumerator_Dispose_m3215924523_RuntimeMethod_var);
					IL2CPP_END_FINALLY(195)
				} // end finally (depth: 4)
				IL2CPP_CLEANUP(195)
				{
					IL2CPP_JUMP_TBL(0xD1, IL_00d1)
					IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
				}

IL_00d1:
				{
					bool L_28 = Enumerator_MoveNext_m1435586957((&V_10), /*hidden argument*/Enumerator_MoveNext_m1435586957_RuntimeMethod_var);
					if (L_28)
					{
						goto IL_006a;
					}
				}

IL_00dd:
				{
					IL2CPP_LEAVE(0xF0, FINALLY_00e2);
				}
			} // end try (depth: 3)
			catch(Il2CppExceptionWrapper& e)
			{
				__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
				goto FINALLY_00e2;
			}

FINALLY_00e2:
			{ // begin finally (depth: 3)
				Enumerator_Dispose_m3632502891((&V_10), /*hidden argument*/Enumerator_Dispose_m3632502891_RuntimeMethod_var);
				IL2CPP_END_FINALLY(226)
			} // end finally (depth: 3)
			IL2CPP_CLEANUP(226)
			{
				IL2CPP_JUMP_TBL(0xF0, IL_00f0)
				IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
			}

IL_00f0:
			{
				bool L_29 = V_6;
				if (L_29)
				{
					goto IL_00fc;
				}
			}

IL_00f7:
			{
				goto IL_028b;
			}

IL_00fc:
			{
				V_6 = (bool)0;
				Cell_t964604196 * L_30 = ___cell21;
				List_1_t333725328 * L_31 = Cell_get_blocks_m153328201(L_30, /*hidden argument*/NULL);
				Enumerator_t4163422298  L_32 = List_1_GetEnumerator_m174512497(L_31, /*hidden argument*/List_1_GetEnumerator_m174512497_RuntimeMethod_var);
				V_15 = L_32;
			}

IL_010c:
			try
			{ // begin try (depth: 3)
				{
					goto IL_0178;
				}

IL_0111:
				{
					Cell_t964604196 * L_33 = Enumerator_get_Current_m4246516629((&V_15), /*hidden argument*/Enumerator_get_Current_m4246516629_RuntimeMethod_var);
					V_14 = L_33;
					Cell_t964604196 * L_34 = V_14;
					List_1_t1612828712 * L_35 = Cell_get_midPoints_m2313462662(L_34, /*hidden argument*/NULL);
					Enumerator_t1147558386  L_36 = List_1_GetEnumerator_m940839541(L_35, /*hidden argument*/List_1_GetEnumerator_m940839541_RuntimeMethod_var);
					V_17 = L_36;
				}

IL_0128:
				try
				{ // begin try (depth: 4)
					{
						goto IL_0159;
					}

IL_012d:
					{
						Vector3_t2243707580  L_37 = Enumerator_get_Current_m857008049((&V_17), /*hidden argument*/Enumerator_get_Current_m857008049_RuntimeMethod_var);
						V_16 = L_37;
						Vector3_t2243707580  L_38 = V_16;
						Vector3_t2243707580  L_39 = V_1;
						IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
						Vector3_t2243707580  L_40 = Vector3_op_Subtraction_m4046047256(NULL /*static, unused*/, L_38, L_39, /*hidden argument*/NULL);
						V_18 = L_40;
						float L_41 = Vector3_get_sqrMagnitude_m1445517847((&V_18), /*hidden argument*/NULL);
						float L_42 = V_0;
						if ((!(((float)L_41) < ((float)L_42))))
						{
							goto IL_0159;
						}
					}

IL_014d:
					{
						Cell_t964604196 * L_43 = V_14;
						V_8 = L_43;
						V_6 = (bool)1;
						goto IL_0165;
					}

IL_0159:
					{
						bool L_44 = Enumerator_MoveNext_m2007266881((&V_17), /*hidden argument*/Enumerator_MoveNext_m2007266881_RuntimeMethod_var);
						if (L_44)
						{
							goto IL_012d;
						}
					}

IL_0165:
					{
						IL2CPP_LEAVE(0x178, FINALLY_016a);
					}
				} // end try (depth: 4)
				catch(Il2CppExceptionWrapper& e)
				{
					__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
					goto FINALLY_016a;
				}

FINALLY_016a:
				{ // begin finally (depth: 4)
					Enumerator_Dispose_m3215924523((&V_17), /*hidden argument*/Enumerator_Dispose_m3215924523_RuntimeMethod_var);
					IL2CPP_END_FINALLY(362)
				} // end finally (depth: 4)
				IL2CPP_CLEANUP(362)
				{
					IL2CPP_JUMP_TBL(0x178, IL_0178)
					IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
				}

IL_0178:
				{
					bool L_45 = Enumerator_MoveNext_m1435586957((&V_15), /*hidden argument*/Enumerator_MoveNext_m1435586957_RuntimeMethod_var);
					if (L_45)
					{
						goto IL_0111;
					}
				}

IL_0184:
				{
					IL2CPP_LEAVE(0x197, FINALLY_0189);
				}
			} // end try (depth: 3)
			catch(Il2CppExceptionWrapper& e)
			{
				__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
				goto FINALLY_0189;
			}

FINALLY_0189:
			{ // begin finally (depth: 3)
				Enumerator_Dispose_m3632502891((&V_15), /*hidden argument*/Enumerator_Dispose_m3632502891_RuntimeMethod_var);
				IL2CPP_END_FINALLY(393)
			} // end finally (depth: 3)
			IL2CPP_CLEANUP(393)
			{
				IL2CPP_JUMP_TBL(0x197, IL_0197)
				IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
			}

IL_0197:
			{
				bool L_46 = V_6;
				if (!L_46)
				{
					goto IL_028b;
				}
			}

IL_019e:
			{
				Cell_t964604196 * L_47 = V_7;
				Vector3_t2243707580  L_48 = Cell_get_pos_m113374293(L_47, /*hidden argument*/NULL);
				Cell_t964604196 * L_49 = V_8;
				Vector3_t2243707580  L_50 = Cell_get_pos_m113374293(L_49, /*hidden argument*/NULL);
				IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
				Vector3_t2243707580  L_51 = Vector3_op_Addition_m394909128(NULL /*static, unused*/, L_48, L_50, /*hidden argument*/NULL);
				Vector3_t2243707580  L_52 = Vector3_op_Division_m3670947479(NULL /*static, unused*/, L_51, (2.0f), /*hidden argument*/NULL);
				V_19 = L_52;
				Cell_t964604196 * L_53 = ___cell21;
				Vector3_t2243707580  L_54 = Cell_get_pos_m113374293(L_53, /*hidden argument*/NULL);
				Cell_t964604196 * L_55 = ___cell10;
				Vector3_t2243707580  L_56 = Cell_get_pos_m113374293(L_55, /*hidden argument*/NULL);
				Vector3_t2243707580  L_57 = Vector3_op_Subtraction_m4046047256(NULL /*static, unused*/, L_54, L_56, /*hidden argument*/NULL);
				V_20 = L_57;
				Vector3_t2243707580  L_58 = V_1;
				Vector3_t2243707580  L_59 = V_19;
				Vector3_t2243707580  L_60 = Vector3_op_Subtraction_m4046047256(NULL /*static, unused*/, L_58, L_59, /*hidden argument*/NULL);
				V_21 = L_60;
				Link_t776645742 * L_61 = (Link_t776645742 *)il2cpp_codegen_object_new(Link_t776645742_il2cpp_TypeInfo_var);
				Link__ctor_m2788241046(L_61, /*hidden argument*/NULL);
				V_22 = L_61;
				Link_t776645742 * L_62 = V_22;
				Vector3_t2243707580  L_63 = V_1;
				L_62->set_pivot_1(L_63);
				Link_t776645742 * L_64 = V_22;
				Cell_t964604196 * L_65 = ___cell10;
				L_64->set_first_3(L_65);
				Link_t776645742 * L_66 = V_22;
				Cell_t964604196 * L_67 = ___cell21;
				L_66->set_second_4(L_67);
				Link_t776645742 * L_68 = V_22;
				Vector3_t2243707580  L_69 = V_21;
				Vector3_t2243707580  L_70 = V_20;
				Vector3_t2243707580  L_71 = Vector3_Cross_m693157500(NULL /*static, unused*/, L_69, L_70, /*hidden argument*/NULL);
				L_68->set_axis_0(L_71);
				Link_t776645742 * L_72 = V_22;
				Cell_t964604196 * L_73 = V_7;
				Cell_t964604196 * L_74 = V_8;
				G_B33_0 = L_72;
				if ((!(((RuntimeObject*)(Cell_t964604196 *)L_73) == ((RuntimeObject*)(Cell_t964604196 *)L_74))))
				{
					G_B34_0 = L_72;
					goto IL_021e;
				}
			}

IL_0214:
			{
				G_B35_0 = ((int32_t)180);
				G_B35_1 = G_B33_0;
				goto IL_0220;
			}

IL_021e:
			{
				G_B35_0 = ((int32_t)90);
				G_B35_1 = G_B34_0;
			}

IL_0220:
			{
				G_B35_1->set_angle_2((((float)((float)G_B35_0))));
				Cell_t964604196 * L_75 = ___cell10;
				List_1_t145766874 * L_76 = Cell_get_links_m2033144484(L_75, /*hidden argument*/NULL);
				Link_t776645742 * L_77 = V_22;
				List_1_Add_m257155336(L_76, L_77, /*hidden argument*/List_1_Add_m257155336_RuntimeMethod_var);
				bool L_78 = ___twofold2;
				if (!L_78)
				{
					goto IL_0286;
				}
			}

IL_0239:
			{
				Link_t776645742 * L_79 = (Link_t776645742 *)il2cpp_codegen_object_new(Link_t776645742_il2cpp_TypeInfo_var);
				Link__ctor_m2788241046(L_79, /*hidden argument*/NULL);
				V_23 = L_79;
				Link_t776645742 * L_80 = V_23;
				Vector3_t2243707580  L_81 = V_1;
				L_80->set_pivot_1(L_81);
				Link_t776645742 * L_82 = V_23;
				Cell_t964604196 * L_83 = ___cell21;
				L_82->set_first_3(L_83);
				Link_t776645742 * L_84 = V_23;
				Cell_t964604196 * L_85 = ___cell10;
				L_84->set_second_4(L_85);
				Link_t776645742 * L_86 = V_23;
				Link_t776645742 * L_87 = V_22;
				Vector3_t2243707580  L_88 = L_87->get_axis_0();
				IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
				Vector3_t2243707580  L_89 = Vector3_op_UnaryNegation_m2337886919(NULL /*static, unused*/, L_88, /*hidden argument*/NULL);
				L_86->set_axis_0(L_89);
				Link_t776645742 * L_90 = V_23;
				Link_t776645742 * L_91 = V_22;
				float L_92 = L_91->get_angle_2();
				L_90->set_angle_2(L_92);
				Cell_t964604196 * L_93 = ___cell21;
				List_1_t145766874 * L_94 = Cell_get_links_m2033144484(L_93, /*hidden argument*/NULL);
				Link_t776645742 * L_95 = V_23;
				List_1_Add_m257155336(L_94, L_95, /*hidden argument*/List_1_Add_m257155336_RuntimeMethod_var);
			}

IL_0286:
			{
				IL2CPP_LEAVE(0x2C9, FINALLY_029c);
			}

IL_028b:
			{
				bool L_96 = Enumerator_MoveNext_m2007266881((&V_4), /*hidden argument*/Enumerator_MoveNext_m2007266881_RuntimeMethod_var);
				if (L_96)
				{
					goto IL_0031;
				}
			}

IL_0297:
			{
				IL2CPP_LEAVE(0x2AA, FINALLY_029c);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
			goto FINALLY_029c;
		}

FINALLY_029c:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m3215924523((&V_4), /*hidden argument*/Enumerator_Dispose_m3215924523_RuntimeMethod_var);
			IL2CPP_END_FINALLY(668)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(668)
		{
			IL2CPP_END_CLEANUP(0x2C9, FINALLY_02bb);
			IL2CPP_JUMP_TBL(0x2AA, IL_02aa)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
		}

IL_02aa:
		{
			bool L_97 = Enumerator_MoveNext_m2007266881((&V_2), /*hidden argument*/Enumerator_MoveNext_m2007266881_RuntimeMethod_var);
			if (L_97)
			{
				goto IL_0017;
			}
		}

IL_02b6:
		{
			IL2CPP_LEAVE(0x2C9, FINALLY_02bb);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_02bb;
	}

FINALLY_02bb:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3215924523((&V_2), /*hidden argument*/Enumerator_Dispose_m3215924523_RuntimeMethod_var);
		IL2CPP_END_FINALLY(699)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(699)
	{
		IL2CPP_JUMP_TBL(0x2C9, IL_02c9)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_02c9:
	{
		return;
	}
}
// Assets.scripts.Cell Level::cellAtPosition(UnityEngine.Vector3)
extern "C"  Cell_t964604196 * Level_cellAtPosition_m891825944 (Level_t866853782 * __this, Vector3_t2243707580  ___position0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Level_cellAtPosition_m891825944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CellU5BU2CU2CU5D_t1218597327* L_0 = __this->get_cells_2();
		float L_1 = (&___position0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = bankers_roundf(L_1);
		float L_3 = (&___position0)->get_y_2();
		float L_4 = bankers_roundf(L_3);
		float L_5 = (&___position0)->get_z_3();
		float L_6 = bankers_roundf(L_5);
		Cell_t964604196 * L_7 = ((CellU5BU2CU2CU5D_t1218597327*)(CellU5BU2CU2CU5D_t1218597327*)L_0)->GetAtUnchecked((((int32_t)((int32_t)L_2))), (((int32_t)((int32_t)L_4))), (((int32_t)((int32_t)L_6))));
		return L_7;
	}
}
// System.Void Menu::.ctor()
extern "C"  void Menu__ctor_m2507984538 (Menu_t4261767481 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Menu::Start()
extern "C"  void Menu_Start_m3632225306 (Menu_t4261767481 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Menu_Start_m3632225306_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = AudioListener_get_volume_m1738024862(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_volume_12(L_0);
		AudioListener_set_volume_m4197446127(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1594932300, /*hidden argument*/NULL);
		Game_t1600141214 * L_2 = GameObject_GetComponent_TisGame_t1600141214_m3908916829(L_1, /*hidden argument*/GameObject_GetComponent_TisGame_t1600141214_m3908916829_RuntimeMethod_var);
		__this->set_game_10(L_2);
		GameObject_t1756533147 * L_3 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2995787169, /*hidden argument*/NULL);
		Text_t356221433 * L_4 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_3, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_RuntimeMethod_var);
		__this->set_textWin_2(L_4);
		GameObject_t1756533147 * L_5 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2386992142, /*hidden argument*/NULL);
		Text_t356221433 * L_6 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_5, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_RuntimeMethod_var);
		__this->set_textFragments_3(L_6);
		GameObject_t1756533147 * L_7 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral3343504797, /*hidden argument*/NULL);
		__this->set_dialogBox_8(L_7);
		GameObject_t1756533147 * L_8 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2686302870, /*hidden argument*/NULL);
		Text_t356221433 * L_9 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_8, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_RuntimeMethod_var);
		__this->set_textDialogBox_4(L_9);
		GameObject_t1756533147 * L_10 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral618982595, /*hidden argument*/NULL);
		__this->set_panelSettings_9(L_10);
		GameObject_t1756533147 * L_11 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1761466451, /*hidden argument*/NULL);
		Button_t2872111280 * L_12 = GameObject_GetComponent_TisButton_t2872111280_m1008560876(L_11, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m1008560876_RuntimeMethod_var);
		__this->set_buttonSettings_5(L_12);
		Button_t2872111280 * L_13 = __this->get_buttonSettings_5();
		ButtonClickedEvent_t2455055323 * L_14 = Button_get_onClick_m1595880935(L_13, /*hidden argument*/NULL);
		intptr_t L_15 = (intptr_t)Menu_U3CStartU3Em__0_m959812099_RuntimeMethod_var;
		UnityAction_t4025899511 * L_16 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m3825983746(L_16, __this, L_15, /*hidden argument*/NULL);
		UnityEvent_AddListener_m3787170584(L_14, L_16, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_17 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2998934070, /*hidden argument*/NULL);
		Button_t2872111280 * L_18 = GameObject_GetComponent_TisButton_t2872111280_m1008560876(L_17, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m1008560876_RuntimeMethod_var);
		__this->set_buttonDone_6(L_18);
		Button_t2872111280 * L_19 = __this->get_buttonDone_6();
		ButtonClickedEvent_t2455055323 * L_20 = Button_get_onClick_m1595880935(L_19, /*hidden argument*/NULL);
		intptr_t L_21 = (intptr_t)Menu_U3CStartU3Em__1_m959812004_RuntimeMethod_var;
		UnityAction_t4025899511 * L_22 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m3825983746(L_22, __this, L_21, /*hidden argument*/NULL);
		UnityEvent_AddListener_m3787170584(L_20, L_22, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_23 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1659439092, /*hidden argument*/NULL);
		Button_t2872111280 * L_24 = GameObject_GetComponent_TisButton_t2872111280_m1008560876(L_23, /*hidden argument*/GameObject_GetComponent_TisButton_t2872111280_m1008560876_RuntimeMethod_var);
		__this->set_buttonVolume_7(L_24);
		Button_t2872111280 * L_25 = __this->get_buttonVolume_7();
		ButtonClickedEvent_t2455055323 * L_26 = Button_get_onClick_m1595880935(L_25, /*hidden argument*/NULL);
		intptr_t L_27 = (intptr_t)Menu_U3CStartU3Em__2_m959812033_RuntimeMethod_var;
		UnityAction_t4025899511 * L_28 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m3825983746(L_28, __this, L_27, /*hidden argument*/NULL);
		UnityEvent_AddListener_m3787170584(L_26, L_28, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_29 = __this->get_dialogBox_8();
		GameObject_SetActive_m2693135142(L_29, (bool)0, /*hidden argument*/NULL);
		Text_t356221433 * L_30 = __this->get_textWin_2();
		GameObject_t1756533147 * L_31 = Component_get_gameObject_m2159020946(L_30, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_31, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_32 = __this->get_panelSettings_9();
		GameObject_SetActive_m2693135142(L_32, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Menu::finishLevel()
extern "C"  void Menu_finishLevel_m1671178345 (Menu_t4261767481 * __this, const RuntimeMethod* method)
{
	{
		Text_t356221433 * L_0 = __this->get_textWin_2();
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m2159020946(L_0, /*hidden argument*/NULL);
		GameObject_SetActive_m2693135142(L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Menu::updateFragments()
extern "C"  void Menu_updateFragments_m871188344 (Menu_t4261767481 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Menu_updateFragments_m871188344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	RuntimeObject * G_B2_1 = NULL;
	Text_t356221433 * G_B2_2 = NULL;
	String_t* G_B1_0 = NULL;
	RuntimeObject * G_B1_1 = NULL;
	Text_t356221433 * G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B3_1 = NULL;
	RuntimeObject * G_B3_2 = NULL;
	Text_t356221433 * G_B3_3 = NULL;
	{
		Text_t356221433 * L_0 = __this->get_textFragments_3();
		Game_t1600141214 * L_1 = __this->get_game_10();
		int32_t L_2 = Game_get_fragments_m2335769477(L_1, /*hidden argument*/NULL);
		int32_t L_3 = L_2;
		RuntimeObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		Game_t1600141214 * L_5 = __this->get_game_10();
		int32_t L_6 = Game_get_fragments_m2335769477(L_5, /*hidden argument*/NULL);
		G_B1_0 = _stringLiteral1993429030;
		G_B1_1 = L_4;
		G_B1_2 = L_0;
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			G_B2_0 = _stringLiteral1993429030;
			G_B2_1 = L_4;
			G_B2_2 = L_0;
			goto IL_0036;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		G_B3_0 = L_7;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003b;
	}

IL_0036:
	{
		G_B3_0 = _stringLiteral372029391;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m2000667605(NULL /*static, unused*/, G_B3_2, G_B3_1, G_B3_0, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, G_B3_3, L_8);
		return;
	}
}
// System.Void Menu::showDialog(System.String)
extern "C"  void Menu_showDialog_m540151737 (Menu_t4261767481 * __this, String_t* ___message0, const RuntimeMethod* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_dialogBox_8();
		GameObject_SetActive_m2693135142(L_0, (bool)1, /*hidden argument*/NULL);
		Text_t356221433 * L_1 = __this->get_textDialogBox_4();
		String_t* L_2 = ___message0;
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_2);
		return;
	}
}
// System.Void Menu::hideDialog()
extern "C"  void Menu_hideDialog_m3443374652 (Menu_t4261767481 * __this, const RuntimeMethod* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_dialogBox_8();
		GameObject_SetActive_m2693135142(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Menu::<Start>m__0()
extern "C"  void Menu_U3CStartU3Em__0_m959812099 (Menu_t4261767481 * __this, const RuntimeMethod* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_panelSettings_9();
		GameObject_SetActive_m2693135142(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Menu::<Start>m__1()
extern "C"  void Menu_U3CStartU3Em__1_m959812004 (Menu_t4261767481 * __this, const RuntimeMethod* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_panelSettings_9();
		GameObject_SetActive_m2693135142(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Menu::<Start>m__2()
extern "C"  void Menu_U3CStartU3Em__2_m959812033 (Menu_t4261767481 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Menu_U3CStartU3Em__2_m959812033_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float G_B3_0 = 0.0f;
	Image_t2042527209 * G_B5_0 = NULL;
	Image_t2042527209 * G_B4_0 = NULL;
	Sprite_t309593783 * G_B6_0 = NULL;
	Image_t2042527209 * G_B6_1 = NULL;
	{
		bool L_0 = __this->get_volumeOn_11();
		__this->set_volumeOn_11((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		bool L_1 = __this->get_volumeOn_11();
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		float L_2 = __this->get_volume_12();
		G_B3_0 = L_2;
		goto IL_002a;
	}

IL_0025:
	{
		G_B3_0 = (0.0f);
	}

IL_002a:
	{
		AudioListener_set_volume_m4197446127(NULL /*static, unused*/, G_B3_0, /*hidden argument*/NULL);
		Button_t2872111280 * L_3 = __this->get_buttonVolume_7();
		Image_t2042527209 * L_4 = Component_GetComponent_TisImage_t2042527209_m2189462422(L_3, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_RuntimeMethod_var);
		bool L_5 = __this->get_volumeOn_11();
		G_B4_0 = L_4;
		if (!L_5)
		{
			G_B5_0 = L_4;
			goto IL_005a;
		}
	}
	{
		Button_t2872111280 * L_6 = __this->get_buttonVolume_7();
		ToggleButton_t3926247120 * L_7 = Component_GetComponent_TisToggleButton_t3926247120_m2729835643(L_6, /*hidden argument*/Component_GetComponent_TisToggleButton_t3926247120_m2729835643_RuntimeMethod_var);
		Sprite_t309593783 * L_8 = L_7->get_imageOn_2();
		G_B6_0 = L_8;
		G_B6_1 = G_B4_0;
		goto IL_006a;
	}

IL_005a:
	{
		Button_t2872111280 * L_9 = __this->get_buttonVolume_7();
		ToggleButton_t3926247120 * L_10 = Component_GetComponent_TisToggleButton_t3926247120_m2729835643(L_9, /*hidden argument*/Component_GetComponent_TisToggleButton_t3926247120_m2729835643_RuntimeMethod_var);
		Sprite_t309593783 * L_11 = L_10->get_imageOff_3();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
	}

IL_006a:
	{
		Image_set_sprite_m1800056820(G_B6_1, G_B6_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinder::.ctor()
extern "C"  void Pathfinder__ctor_m2491665482 (Pathfinder_t1592129589 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pathfinder::Start()
extern "C"  void Pathfinder_Start_m3564303874 (Pathfinder_t1592129589 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pathfinder_Start_m3564303874_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Level_t866853782 * L_0 = Component_GetComponent_TisLevel_t866853782_m4130163129(__this, /*hidden argument*/Component_GetComponent_TisLevel_t866853782_m4130163129_RuntimeMethod_var);
		__this->set_level_2(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<Assets.scripts.Link> Pathfinder::findPath(Assets.scripts.Cell,Assets.scripts.Cell)
extern "C"  List_1_t145766874 * Pathfinder_findPath_m1726079702 (Pathfinder_t1592129589 * __this, Cell_t964604196 * ___start0, Cell_t964604196 * ___destination1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pathfinder_findPath_m1726079702_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t145766874 * V_0 = NULL;
	List_1_t333725328 * V_1 = NULL;
	List_1_t333725328 * V_2 = NULL;
	Cell_t964604196 * V_3 = NULL;
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	float V_10 = 0.0f;
	Cell_t964604196 * V_11 = NULL;
	Enumerator_t4163422298  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Link_t776645742 * V_13 = NULL;
	Enumerator_t3975463844  V_14;
	memset(&V_14, 0, sizeof(V_14));
	float V_15 = 0.0f;
	Cell_t964604196 * V_16 = NULL;
	Enumerator_t4163422298  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Cell_t964604196 * V_18 = NULL;
	Enumerator_t4163422298  V_19;
	memset(&V_19, 0, sizeof(V_19));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t145766874 * L_0 = (List_1_t145766874 *)il2cpp_codegen_object_new(List_1_t145766874_il2cpp_TypeInfo_var);
		List_1__ctor_m1908755428(L_0, /*hidden argument*/List_1__ctor_m1908755428_RuntimeMethod_var);
		V_0 = L_0;
		List_1_t333725328 * L_1 = (List_1_t333725328 *)il2cpp_codegen_object_new(List_1_t333725328_il2cpp_TypeInfo_var);
		List_1__ctor_m2909086300(L_1, /*hidden argument*/List_1__ctor_m2909086300_RuntimeMethod_var);
		V_1 = L_1;
		List_1_t333725328 * L_2 = (List_1_t333725328 *)il2cpp_codegen_object_new(List_1_t333725328_il2cpp_TypeInfo_var);
		List_1__ctor_m2909086300(L_2, /*hidden argument*/List_1__ctor_m2909086300_RuntimeMethod_var);
		V_2 = L_2;
		Cell_t964604196 * L_3 = ___start0;
		V_3 = L_3;
		Cell_t964604196 * L_4 = V_3;
		Cell_t964604196 * L_5 = V_3;
		Vector3_t2243707580  L_6 = Cell_get_pos_m113374293(L_5, /*hidden argument*/NULL);
		V_4 = L_6;
		float L_7 = (&V_4)->get_x_1();
		Cell_t964604196 * L_8 = ___destination1;
		Vector3_t2243707580  L_9 = Cell_get_pos_m113374293(L_8, /*hidden argument*/NULL);
		V_5 = L_9;
		float L_10 = (&V_5)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_11 = fabsf(((float)((float)L_7-(float)L_10)));
		Cell_t964604196 * L_12 = V_3;
		Vector3_t2243707580  L_13 = Cell_get_pos_m113374293(L_12, /*hidden argument*/NULL);
		V_6 = L_13;
		float L_14 = (&V_6)->get_y_2();
		Cell_t964604196 * L_15 = ___destination1;
		Vector3_t2243707580  L_16 = Cell_get_pos_m113374293(L_15, /*hidden argument*/NULL);
		V_7 = L_16;
		float L_17 = (&V_7)->get_y_2();
		float L_18 = fabsf(((float)((float)L_14-(float)L_17)));
		Cell_t964604196 * L_19 = V_3;
		Vector3_t2243707580  L_20 = Cell_get_pos_m113374293(L_19, /*hidden argument*/NULL);
		V_8 = L_20;
		float L_21 = (&V_8)->get_z_3();
		Cell_t964604196 * L_22 = ___destination1;
		Vector3_t2243707580  L_23 = Cell_get_pos_m113374293(L_22, /*hidden argument*/NULL);
		V_9 = L_23;
		float L_24 = (&V_9)->get_z_3();
		float L_25 = fabsf(((float)((float)L_21-(float)L_24)));
		L_4->set_h_10(((float)((float)((float)((float)L_11+(float)L_18))+(float)L_25)));
		List_1_t333725328 * L_26 = V_2;
		Cell_t964604196 * L_27 = V_3;
		List_1_Add_m1728370848(L_26, L_27, /*hidden argument*/List_1_Add_m1728370848_RuntimeMethod_var);
		goto IL_022c;
	}

IL_0094:
	{
		V_10 = (std::numeric_limits<float>::max());
		List_1_t333725328 * L_28 = V_2;
		Enumerator_t4163422298  L_29 = List_1_GetEnumerator_m174512497(L_28, /*hidden argument*/List_1_GetEnumerator_m174512497_RuntimeMethod_var);
		V_12 = L_29;
	}

IL_00a3:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00cb;
		}

IL_00a8:
		{
			Cell_t964604196 * L_30 = Enumerator_get_Current_m4246516629((&V_12), /*hidden argument*/Enumerator_get_Current_m4246516629_RuntimeMethod_var);
			V_11 = L_30;
			Cell_t964604196 * L_31 = V_11;
			float L_32 = Cell_get_f_m753931623(L_31, /*hidden argument*/NULL);
			float L_33 = V_10;
			if ((!(((float)L_32) < ((float)L_33))))
			{
				goto IL_00cb;
			}
		}

IL_00bf:
		{
			Cell_t964604196 * L_34 = V_11;
			V_3 = L_34;
			Cell_t964604196 * L_35 = V_11;
			float L_36 = Cell_get_f_m753931623(L_35, /*hidden argument*/NULL);
			V_10 = L_36;
		}

IL_00cb:
		{
			bool L_37 = Enumerator_MoveNext_m1435586957((&V_12), /*hidden argument*/Enumerator_MoveNext_m1435586957_RuntimeMethod_var);
			if (L_37)
			{
				goto IL_00a8;
			}
		}

IL_00d7:
		{
			IL2CPP_LEAVE(0xEA, FINALLY_00dc);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00dc;
	}

FINALLY_00dc:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3632502891((&V_12), /*hidden argument*/Enumerator_Dispose_m3632502891_RuntimeMethod_var);
		IL2CPP_END_FINALLY(220)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(220)
	{
		IL2CPP_JUMP_TBL(0xEA, IL_00ea)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00ea:
	{
		List_1_t333725328 * L_38 = V_2;
		Cell_t964604196 * L_39 = V_3;
		List_1_Remove_m1927217927(L_38, L_39, /*hidden argument*/List_1_Remove_m1927217927_RuntimeMethod_var);
		List_1_t333725328 * L_40 = V_1;
		Cell_t964604196 * L_41 = V_3;
		List_1_Add_m1728370848(L_40, L_41, /*hidden argument*/List_1_Add_m1728370848_RuntimeMethod_var);
		Cell_t964604196 * L_42 = V_3;
		Cell_t964604196 * L_43 = ___destination1;
		if ((!(((RuntimeObject*)(Cell_t964604196 *)L_42) == ((RuntimeObject*)(Cell_t964604196 *)L_43))))
		{
			goto IL_0105;
		}
	}
	{
		goto IL_0237;
	}

IL_0105:
	{
		Cell_t964604196 * L_44 = V_3;
		List_1_t145766874 * L_45 = Cell_get_links_m2033144484(L_44, /*hidden argument*/NULL);
		Enumerator_t3975463844  L_46 = List_1_GetEnumerator_m3554822035(L_45, /*hidden argument*/List_1_GetEnumerator_m3554822035_RuntimeMethod_var);
		V_14 = L_46;
	}

IL_0112:
	try
	{ // begin try (depth: 1)
		{
			goto IL_020d;
		}

IL_0117:
		{
			Link_t776645742 * L_47 = Enumerator_get_Current_m1037497887((&V_14), /*hidden argument*/Enumerator_get_Current_m1037497887_RuntimeMethod_var);
			V_13 = L_47;
			Link_t776645742 * L_48 = V_13;
			Cell_t964604196 * L_49 = L_48->get_second_4();
			bool L_50 = L_49->get_isObstacle_12();
			if (L_50)
			{
				goto IL_020d;
			}
		}

IL_0131:
		{
			List_1_t333725328 * L_51 = V_1;
			Link_t776645742 * L_52 = V_13;
			Cell_t964604196 * L_53 = L_52->get_second_4();
			bool L_54 = List_1_Contains_m2414020574(L_51, L_53, /*hidden argument*/List_1_Contains_m2414020574_RuntimeMethod_var);
			if (L_54)
			{
				goto IL_020d;
			}
		}

IL_0143:
		{
			Cell_t964604196 * L_55 = ___destination1;
			int32_t L_56 = Cell_get_x_m2406521465(L_55, /*hidden argument*/NULL);
			Link_t776645742 * L_57 = V_13;
			Cell_t964604196 * L_58 = L_57->get_second_4();
			int32_t L_59 = Cell_get_x_m2406521465(L_58, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
			int32_t L_60 = Mathf_Abs_m722236531(NULL /*static, unused*/, ((int32_t)((int32_t)L_56-(int32_t)L_59)), /*hidden argument*/NULL);
			Cell_t964604196 * L_61 = ___destination1;
			int32_t L_62 = Cell_get_y_m2406521560(L_61, /*hidden argument*/NULL);
			Link_t776645742 * L_63 = V_13;
			Cell_t964604196 * L_64 = L_63->get_second_4();
			int32_t L_65 = Cell_get_y_m2406521560(L_64, /*hidden argument*/NULL);
			int32_t L_66 = Mathf_Abs_m722236531(NULL /*static, unused*/, ((int32_t)((int32_t)L_62-(int32_t)L_65)), /*hidden argument*/NULL);
			Cell_t964604196 * L_67 = ___destination1;
			int32_t L_68 = Cell_get_z_m2406521399(L_67, /*hidden argument*/NULL);
			Link_t776645742 * L_69 = V_13;
			Cell_t964604196 * L_70 = L_69->get_second_4();
			int32_t L_71 = Cell_get_z_m2406521399(L_70, /*hidden argument*/NULL);
			int32_t L_72 = Mathf_Abs_m722236531(NULL /*static, unused*/, ((int32_t)((int32_t)L_68-(int32_t)L_71)), /*hidden argument*/NULL);
			V_15 = (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_60+(int32_t)L_66))+(int32_t)L_72)))));
			List_1_t333725328 * L_73 = V_2;
			Link_t776645742 * L_74 = V_13;
			Cell_t964604196 * L_75 = L_74->get_second_4();
			bool L_76 = List_1_Contains_m2414020574(L_73, L_75, /*hidden argument*/List_1_Contains_m2414020574_RuntimeMethod_var);
			if (L_76)
			{
				goto IL_01b4;
			}
		}

IL_01a2:
		{
			List_1_t333725328 * L_77 = V_2;
			Link_t776645742 * L_78 = V_13;
			Cell_t964604196 * L_79 = L_78->get_second_4();
			List_1_Add_m1728370848(L_77, L_79, /*hidden argument*/List_1_Add_m1728370848_RuntimeMethod_var);
			goto IL_01d9;
		}

IL_01b4:
		{
			Link_t776645742 * L_80 = V_13;
			Cell_t964604196 * L_81 = L_80->get_second_4();
			float L_82 = Cell_get_f_m753931623(L_81, /*hidden argument*/NULL);
			float L_83 = V_15;
			Cell_t964604196 * L_84 = V_3;
			float L_85 = L_84->get_g_9();
			if ((!(((float)L_82) < ((float)((float)((float)((float)((float)L_83+(float)L_85))+(float)(1.0f)))))))
			{
				goto IL_01d9;
			}
		}

IL_01d4:
		{
			goto IL_020d;
		}

IL_01d9:
		{
			Link_t776645742 * L_86 = V_13;
			Cell_t964604196 * L_87 = L_86->get_second_4();
			Link_t776645742 * L_88 = V_13;
			L_87->set_predLink_8(L_88);
			Link_t776645742 * L_89 = V_13;
			Cell_t964604196 * L_90 = L_89->get_second_4();
			Cell_t964604196 * L_91 = V_3;
			float L_92 = L_91->get_g_9();
			L_90->set_g_9(((float)((float)L_92+(float)(1.0f))));
			Link_t776645742 * L_93 = V_13;
			Cell_t964604196 * L_94 = L_93->get_second_4();
			float L_95 = V_15;
			L_94->set_h_10(L_95);
		}

IL_020d:
		{
			bool L_96 = Enumerator_MoveNext_m218825171((&V_14), /*hidden argument*/Enumerator_MoveNext_m218825171_RuntimeMethod_var);
			if (L_96)
			{
				goto IL_0117;
			}
		}

IL_0219:
		{
			IL2CPP_LEAVE(0x22C, FINALLY_021e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_021e;
	}

FINALLY_021e:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2728416437((&V_14), /*hidden argument*/Enumerator_Dispose_m2728416437_RuntimeMethod_var);
		IL2CPP_END_FINALLY(542)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(542)
	{
		IL2CPP_JUMP_TBL(0x22C, IL_022c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_022c:
	{
		List_1_t333725328 * L_97 = V_2;
		int32_t L_98 = List_1_get_Count_m349349022(L_97, /*hidden argument*/List_1_get_Count_m349349022_RuntimeMethod_var);
		if (L_98)
		{
			goto IL_0094;
		}
	}

IL_0237:
	{
		Cell_t964604196 * L_99 = V_3;
		Cell_t964604196 * L_100 = ___destination1;
		if ((!(((RuntimeObject*)(Cell_t964604196 *)L_99) == ((RuntimeObject*)(Cell_t964604196 *)L_100))))
		{
			goto IL_0267;
		}
	}
	{
		goto IL_025c;
	}

IL_0243:
	{
		List_1_t145766874 * L_101 = V_0;
		Cell_t964604196 * L_102 = V_3;
		Link_t776645742 * L_103 = L_102->get_predLink_8();
		List_1_Insert_m2431457653(L_101, 0, L_103, /*hidden argument*/List_1_Insert_m2431457653_RuntimeMethod_var);
		Cell_t964604196 * L_104 = V_3;
		Link_t776645742 * L_105 = L_104->get_predLink_8();
		Cell_t964604196 * L_106 = L_105->get_first_3();
		V_3 = L_106;
	}

IL_025c:
	{
		Cell_t964604196 * L_107 = V_3;
		Link_t776645742 * L_108 = L_107->get_predLink_8();
		if (L_108)
		{
			goto IL_0243;
		}
	}

IL_0267:
	{
		List_1_t333725328 * L_109 = V_1;
		Enumerator_t4163422298  L_110 = List_1_GetEnumerator_m174512497(L_109, /*hidden argument*/List_1_GetEnumerator_m174512497_RuntimeMethod_var);
		V_17 = L_110;
	}

IL_026f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_029d;
		}

IL_0274:
		{
			Cell_t964604196 * L_111 = Enumerator_get_Current_m4246516629((&V_17), /*hidden argument*/Enumerator_get_Current_m4246516629_RuntimeMethod_var);
			V_16 = L_111;
			Cell_t964604196 * L_112 = V_16;
			L_112->set_g_9((0.0f));
			Cell_t964604196 * L_113 = V_16;
			L_113->set_h_10((0.0f));
			Cell_t964604196 * L_114 = V_16;
			L_114->set_predLink_8((Link_t776645742 *)NULL);
		}

IL_029d:
		{
			bool L_115 = Enumerator_MoveNext_m1435586957((&V_17), /*hidden argument*/Enumerator_MoveNext_m1435586957_RuntimeMethod_var);
			if (L_115)
			{
				goto IL_0274;
			}
		}

IL_02a9:
		{
			IL2CPP_LEAVE(0x2BC, FINALLY_02ae);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_02ae;
	}

FINALLY_02ae:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3632502891((&V_17), /*hidden argument*/Enumerator_Dispose_m3632502891_RuntimeMethod_var);
		IL2CPP_END_FINALLY(686)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(686)
	{
		IL2CPP_JUMP_TBL(0x2BC, IL_02bc)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_02bc:
	{
		List_1_t333725328 * L_116 = V_2;
		Enumerator_t4163422298  L_117 = List_1_GetEnumerator_m174512497(L_116, /*hidden argument*/List_1_GetEnumerator_m174512497_RuntimeMethod_var);
		V_19 = L_117;
	}

IL_02c4:
	try
	{ // begin try (depth: 1)
		{
			goto IL_02f2;
		}

IL_02c9:
		{
			Cell_t964604196 * L_118 = Enumerator_get_Current_m4246516629((&V_19), /*hidden argument*/Enumerator_get_Current_m4246516629_RuntimeMethod_var);
			V_18 = L_118;
			Cell_t964604196 * L_119 = V_18;
			L_119->set_g_9((0.0f));
			Cell_t964604196 * L_120 = V_18;
			L_120->set_h_10((0.0f));
			Cell_t964604196 * L_121 = V_18;
			L_121->set_predLink_8((Link_t776645742 *)NULL);
		}

IL_02f2:
		{
			bool L_122 = Enumerator_MoveNext_m1435586957((&V_19), /*hidden argument*/Enumerator_MoveNext_m1435586957_RuntimeMethod_var);
			if (L_122)
			{
				goto IL_02c9;
			}
		}

IL_02fe:
		{
			IL2CPP_LEAVE(0x311, FINALLY_0303);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0303;
	}

FINALLY_0303:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3632502891((&V_19), /*hidden argument*/Enumerator_Dispose_m3632502891_RuntimeMethod_var);
		IL2CPP_END_FINALLY(771)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(771)
	{
		IL2CPP_JUMP_TBL(0x311, IL_0311)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0311:
	{
		List_1_t145766874 * L_123 = V_0;
		return L_123;
	}
}
// System.Void PlatformTrigger::.ctor()
extern "C"  void PlatformTrigger__ctor_m3939710668 (PlatformTrigger_t3477208093 * __this, const RuntimeMethod* method)
{
	{
		__this->set_direction_9(1);
		Trigger__ctor_m1356284031(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlatformTrigger::Start()
extern "C"  void PlatformTrigger_Start_m2311334308 (PlatformTrigger_t3477208093 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformTrigger_Start_m2311334308_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1594932300, /*hidden argument*/NULL);
		Level_t866853782 * L_1 = GameObject_GetComponent_TisLevel_t866853782_m3318339701(L_0, /*hidden argument*/GameObject_GetComponent_TisLevel_t866853782_m3318339701_RuntimeMethod_var);
		__this->set_level_8(L_1);
		Transform_t3275118058 * L_2 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = Transform_get_parent_m2752514051(L_2, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m2159020946(L_3, /*hidden argument*/NULL);
		__this->set_platform_2(L_4);
		return;
	}
}
// System.Void PlatformTrigger::Update()
extern "C"  void PlatformTrigger_Update_m3597706753 (PlatformTrigger_t3477208093 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformTrigger_Update_m3597706753_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Cell_t964604196 * V_1 = NULL;
	{
		bool L_0 = __this->get_moving_6();
		if (!L_0)
		{
			goto IL_0116;
		}
	}
	{
		float L_1 = __this->get_distance_11();
		float L_2 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_distance_11(((float)((float)L_1+(float)((float)((float)L_2*(float)(3.0f))))));
		float L_3 = __this->get_distance_11();
		float L_4 = __this->get_totalDistance_12();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_5 = Mathf_Clamp_m1779415170(NULL /*static, unused*/, L_3, (0.0f), L_4, /*hidden argument*/NULL);
		__this->set_distance_11(L_5);
		float L_6 = __this->get_distance_11();
		Vector3_t2243707580  L_7 = PlatformTrigger_getMovement_m3962613840(__this, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		GameObject_t1756533147 * L_8 = __this->get_platform_2();
		Transform_t3275118058 * L_9 = GameObject_get_transform_m3490276752(L_8, /*hidden argument*/NULL);
		Vector3_t2243707580  L_10 = __this->get_platformStart_5();
		Vector3_t2243707580  L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_12 = Vector3_op_Addition_m394909128(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		Transform_set_position_m2942701431(L_9, L_12, /*hidden argument*/NULL);
		Character_t3726027591 * L_13 = __this->get_character_7();
		Transform_t3275118058 * L_14 = Component_get_transform_m3374354972(L_13, /*hidden argument*/NULL);
		Vector3_t2243707580  L_15 = __this->get_characterStart_4();
		Vector3_t2243707580  L_16 = V_0;
		Vector3_t2243707580  L_17 = Vector3_op_Addition_m394909128(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		Transform_set_position_m2942701431(L_14, L_17, /*hidden argument*/NULL);
		float L_18 = __this->get_distance_11();
		float L_19 = __this->get_totalDistance_12();
		if ((!(((float)L_18) >= ((float)L_19))))
		{
			goto IL_0116;
		}
	}
	{
		__this->set_moving_6((bool)0);
		__this->set_distance_11((0.0f));
		Level_t866853782 * L_20 = __this->get_level_8();
		Character_t3726027591 * L_21 = __this->get_character_7();
		Transform_t3275118058 * L_22 = Component_get_transform_m3374354972(L_21, /*hidden argument*/NULL);
		Vector3_t2243707580  L_23 = Transform_get_position_m2304215762(L_22, /*hidden argument*/NULL);
		Cell_t964604196 * L_24 = Level_cellAtPosition_m891825944(L_20, L_23, /*hidden argument*/NULL);
		V_1 = L_24;
		Cell_t964604196 * L_25 = V_1;
		L_25->set_trigger_11(__this);
		Character_t3726027591 * L_26 = __this->get_character_7();
		Cell_t964604196 * L_27 = V_1;
		L_26->set_currentCell_2(L_27);
		int32_t L_28 = __this->get_direction_9();
		__this->set_direction_9(((int32_t)((int32_t)L_28*(int32_t)(-1))));
		List_1_t1612828712 * L_29 = __this->get_moves_3();
		List_1_Reverse_m723397084(L_29, /*hidden argument*/List_1_Reverse_m723397084_RuntimeMethod_var);
		TransformU5BU5D_t3764228911* L_30 = ((TransformU5BU5D_t3764228911*)SZArrayNew(TransformU5BU5D_t3764228911_il2cpp_TypeInfo_var, (uint32_t)2));
		GameObject_t1756533147 * L_31 = __this->get_platform_2();
		Transform_t3275118058 * L_32 = GameObject_get_transform_m3490276752(L_31, /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_30, L_32);
		(L_30)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Transform_t3275118058 *)L_32);
		TransformU5BU5D_t3764228911* L_33 = L_30;
		Character_t3726027591 * L_34 = __this->get_character_7();
		Transform_t3275118058 * L_35 = Component_get_transform_m3374354972(L_34, /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_33, L_35);
		(L_33)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Transform_t3275118058 *)L_35);
		Utility_roundPosition_m3988846968(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
	}

IL_0116:
	{
		return;
	}
}
// System.Void PlatformTrigger::enter(System.Boolean,Character)
extern "C"  void PlatformTrigger_enter_m1061072086 (PlatformTrigger_t3477208093 * __this, bool ___isDestination0, Character_t3726027591 * ___character1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformTrigger_enter_m1061072086_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t1147558386  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = ___isDestination0;
		if (!L_0)
		{
			goto IL_00c4;
		}
	}
	{
		Character_t3726027591 * L_1 = ___character1;
		bool L_2 = Character_get_isPlayer_m2500959794(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00c4;
		}
	}
	{
		Level_t866853782 * L_3 = __this->get_level_8();
		Character_t3726027591 * L_4 = ___character1;
		Transform_t3275118058 * L_5 = Component_get_transform_m3374354972(L_4, /*hidden argument*/NULL);
		Vector3_t2243707580  L_6 = Transform_get_position_m2304215762(L_5, /*hidden argument*/NULL);
		Cell_t964604196 * L_7 = Level_cellAtPosition_m891825944(L_3, L_6, /*hidden argument*/NULL);
		L_7->set_trigger_11((Trigger_t3844394560 *)NULL);
		__this->set_moving_6((bool)1);
		Character_t3726027591 * L_8 = ___character1;
		__this->set_character_7(L_8);
		Character_t3726027591 * L_9 = ___character1;
		Transform_t3275118058 * L_10 = Component_get_transform_m3374354972(L_9, /*hidden argument*/NULL);
		Vector3_t2243707580  L_11 = Transform_get_position_m2304215762(L_10, /*hidden argument*/NULL);
		__this->set_characterStart_4(L_11);
		GameObject_t1756533147 * L_12 = __this->get_platform_2();
		Transform_t3275118058 * L_13 = GameObject_get_transform_m3490276752(L_12, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14 = Transform_get_position_m2304215762(L_13, /*hidden argument*/NULL);
		__this->set_platformStart_5(L_14);
		__this->set_distance_11((0.0f));
		__this->set_totalDistance_12((0.0f));
		List_1_t1612828712 * L_15 = __this->get_moves_3();
		Enumerator_t1147558386  L_16 = List_1_GetEnumerator_m940839541(L_15, /*hidden argument*/List_1_GetEnumerator_m940839541_RuntimeMethod_var);
		V_1 = L_16;
	}

IL_0084:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00a5;
		}

IL_0089:
		{
			Vector3_t2243707580  L_17 = Enumerator_get_Current_m857008049((&V_1), /*hidden argument*/Enumerator_get_Current_m857008049_RuntimeMethod_var);
			V_0 = L_17;
			float L_18 = __this->get_totalDistance_12();
			float L_19 = Vector3_get_magnitude_m4021906415((&V_0), /*hidden argument*/NULL);
			__this->set_totalDistance_12(((float)((float)L_18+(float)L_19)));
		}

IL_00a5:
		{
			bool L_20 = Enumerator_MoveNext_m2007266881((&V_1), /*hidden argument*/Enumerator_MoveNext_m2007266881_RuntimeMethod_var);
			if (L_20)
			{
				goto IL_0089;
			}
		}

IL_00b1:
		{
			IL2CPP_LEAVE(0xC4, FINALLY_00b6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00b6;
	}

FINALLY_00b6:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3215924523((&V_1), /*hidden argument*/Enumerator_Dispose_m3215924523_RuntimeMethod_var);
		IL2CPP_END_FINALLY(182)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(182)
	{
		IL2CPP_JUMP_TBL(0xC4, IL_00c4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00c4:
	{
		return;
	}
}
// UnityEngine.Vector3 PlatformTrigger::getMovement(System.Single)
extern "C"  Vector3_t2243707580  PlatformTrigger_getMovement_m3962613840 (PlatformTrigger_t3477208093 * __this, float ___distance0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformTrigger_getMovement_m3962613840_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Enumerator_t1147558386  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		float L_0 = ___distance0;
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_1 = Vector3_get_zero_m3202043185(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		List_1_t1612828712 * L_2 = __this->get_moves_3();
		Enumerator_t1147558386  L_3 = List_1_GetEnumerator_m940839541(L_2, /*hidden argument*/List_1_GetEnumerator_m940839541_RuntimeMethod_var);
		V_3 = L_3;
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0058;
		}

IL_0019:
		{
			Vector3_t2243707580  L_4 = Enumerator_get_Current_m857008049((&V_3), /*hidden argument*/Enumerator_get_Current_m857008049_RuntimeMethod_var);
			V_2 = L_4;
			float L_5 = V_0;
			float L_6 = Vector3_get_magnitude_m4021906415((&V_2), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
			float L_7 = Mathf_Min_m1552415280(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
			V_4 = L_7;
			float L_8 = V_0;
			float L_9 = V_4;
			V_0 = ((float)((float)L_8-(float)L_9));
			Vector3_t2243707580  L_10 = V_1;
			float L_11 = V_4;
			float L_12 = Vector3_get_magnitude_m4021906415((&V_2), /*hidden argument*/NULL);
			Vector3_t2243707580  L_13 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
			Vector3_t2243707580  L_14 = Vector3_op_Multiply_m1869676276(NULL /*static, unused*/, ((float)((float)L_11/(float)L_12)), L_13, /*hidden argument*/NULL);
			int32_t L_15 = __this->get_direction_9();
			Vector3_t2243707580  L_16 = Vector3_op_Multiply_m2498445460(NULL /*static, unused*/, L_14, (((float)((float)L_15))), /*hidden argument*/NULL);
			Vector3_t2243707580  L_17 = Vector3_op_Addition_m394909128(NULL /*static, unused*/, L_10, L_16, /*hidden argument*/NULL);
			V_1 = L_17;
		}

IL_0058:
		{
			bool L_18 = Enumerator_MoveNext_m2007266881((&V_3), /*hidden argument*/Enumerator_MoveNext_m2007266881_RuntimeMethod_var);
			if (L_18)
			{
				goto IL_0019;
			}
		}

IL_0064:
		{
			IL2CPP_LEAVE(0x77, FINALLY_0069);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0069;
	}

FINALLY_0069:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3215924523((&V_3), /*hidden argument*/Enumerator_Dispose_m3215924523_RuntimeMethod_var);
		IL2CPP_END_FINALLY(105)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(105)
	{
		IL2CPP_JUMP_TBL(0x77, IL_0077)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0077:
	{
		Vector3_t2243707580  L_19 = V_1;
		return L_19;
	}
}
// System.Void RevealTrigger::.ctor()
extern "C"  void RevealTrigger__ctor_m188986982 (RevealTrigger_t3929941135 * __this, const RuntimeMethod* method)
{
	{
		Trigger__ctor_m1356284031(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RevealTrigger::Start()
extern "C"  void RevealTrigger_Start_m1537311146 (RevealTrigger_t3929941135 * __this, const RuntimeMethod* method)
{
	{
		RevealTrigger_putMaterial_m3028200551(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RevealTrigger::enter(System.Boolean,Character)
extern "C"  void RevealTrigger_enter_m819161060 (RevealTrigger_t3929941135 * __this, bool ___isDestination0, Character_t3726027591 * ___character1, const RuntimeMethod* method)
{
	{
		bool L_0 = ___isDestination0;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		RevealTrigger_putMaterial_m3028200551(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_000d:
	{
		return;
	}
}
// System.Void RevealTrigger::exit(System.Boolean,Character)
extern "C"  void RevealTrigger_exit_m615731092 (RevealTrigger_t3929941135 * __this, bool ___isDestination0, Character_t3726027591 * ___character1, const RuntimeMethod* method)
{
	{
		bool L_0 = ___isDestination0;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		RevealTrigger_putMaterial_m3028200551(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_000d:
	{
		return;
	}
}
// System.Void RevealTrigger::putMaterial(System.Boolean)
extern "C"  void RevealTrigger_putMaterial_m3028200551 (RevealTrigger_t3929941135 * __this, bool ___hidden0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RevealTrigger_putMaterial_m3028200551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	Enumerator_t660383953  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Renderer_t257310565 * G_B4_0 = NULL;
	Renderer_t257310565 * G_B3_0 = NULL;
	Material_t193706927 * G_B5_0 = NULL;
	Renderer_t257310565 * G_B5_1 = NULL;
	{
		List_1_t1125654279 * L_0 = __this->get_children_2();
		Enumerator_t660383953  L_1 = List_1_GetEnumerator_m970439620(L_0, /*hidden argument*/List_1_GetEnumerator_m970439620_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003b;
		}

IL_0011:
		{
			GameObject_t1756533147 * L_2 = Enumerator_get_Current_m2389888842((&V_1), /*hidden argument*/Enumerator_get_Current_m2389888842_RuntimeMethod_var);
			V_0 = L_2;
			GameObject_t1756533147 * L_3 = V_0;
			Renderer_t257310565 * L_4 = GameObject_GetComponent_TisRenderer_t257310565_m1312615893(L_3, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m1312615893_RuntimeMethod_var);
			bool L_5 = ___hidden0;
			G_B3_0 = L_4;
			if (!L_5)
			{
				G_B4_0 = L_4;
				goto IL_0030;
			}
		}

IL_0025:
		{
			Material_t193706927 * L_6 = __this->get_hiddenMaterial_3();
			G_B5_0 = L_6;
			G_B5_1 = G_B3_0;
			goto IL_0036;
		}

IL_0030:
		{
			Material_t193706927 * L_7 = __this->get_revealMaterial_4();
			G_B5_0 = L_7;
			G_B5_1 = G_B4_0;
		}

IL_0036:
		{
			Renderer_set_material_m3158565337(G_B5_1, G_B5_0, /*hidden argument*/NULL);
		}

IL_003b:
		{
			bool L_8 = Enumerator_MoveNext_m3635305532((&V_1), /*hidden argument*/Enumerator_MoveNext_m3635305532_RuntimeMethod_var);
			if (L_8)
			{
				goto IL_0011;
			}
		}

IL_0047:
		{
			IL2CPP_LEAVE(0x5A, FINALLY_004c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_004c;
	}

FINALLY_004c:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1242097970((&V_1), /*hidden argument*/Enumerator_Dispose_m1242097970_RuntimeMethod_var);
		IL2CPP_END_FINALLY(76)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(76)
	{
		IL2CPP_JUMP_TBL(0x5A, IL_005a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_005a:
	{
		return;
	}
}
// System.Void SpectateTrigger::.ctor()
extern "C"  void SpectateTrigger__ctor_m2062551772 (SpectateTrigger_t3335062351 * __this, const RuntimeMethod* method)
{
	{
		Trigger__ctor_m1356284031(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SpectateTrigger::Start()
extern "C"  void SpectateTrigger_Start_m1580828672 (SpectateTrigger_t3335062351 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpectateTrigger_Start_m1580828672_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t189460977 * L_0 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		Animator_t69676727 * L_1 = Component_GetComponent_TisAnimator_t69676727_m475627522(L_0, /*hidden argument*/Component_GetComponent_TisAnimator_t69676727_m475627522_RuntimeMethod_var);
		__this->set_animator_2(L_1);
		return;
	}
}
// System.Void SpectateTrigger::Update()
extern "C"  void SpectateTrigger_Update_m157198807 (SpectateTrigger_t3335062351 * __this, const RuntimeMethod* method)
{
	AnimatorStateInfo_t2577870592  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = __this->get_animating_4();
		if (!L_0)
		{
			goto IL_0048;
		}
	}
	{
		bool L_1 = __this->get_previousAnimating_5();
		if (!L_1)
		{
			goto IL_0048;
		}
	}
	{
		Animator_t69676727 * L_2 = __this->get_animator_2();
		AnimatorStateInfo_t2577870592  L_3 = Animator_GetCurrentAnimatorStateInfo_m2354582050(L_2, 0, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = __this->get_animationName_3();
		bool L_5 = AnimatorStateInfo_IsName_m1591389294((&V_0), L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0048;
		}
	}
	{
		Animator_t69676727 * L_6 = __this->get_animator_2();
		Behaviour_set_enabled_m602406666(L_6, (bool)0, /*hidden argument*/NULL);
		__this->set_animating_4((bool)0);
	}

IL_0048:
	{
		bool L_7 = __this->get_animating_4();
		__this->set_previousAnimating_5(L_7);
		return;
	}
}
// System.Void SpectateTrigger::enter(System.Boolean,Character)
extern "C"  void SpectateTrigger_enter_m578463014 (SpectateTrigger_t3335062351 * __this, bool ___isDestination0, Character_t3726027591 * ___character1, const RuntimeMethod* method)
{
	{
		Character_t3726027591 * L_0 = ___character1;
		bool L_1 = Character_get_isPlayer_m2500959794(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Animator_t69676727 * L_2 = __this->get_animator_2();
		Behaviour_set_enabled_m602406666(L_2, (bool)1, /*hidden argument*/NULL);
		Animator_t69676727 * L_3 = __this->get_animator_2();
		String_t* L_4 = __this->get_animationName_3();
		Animator_Play_m577397764(L_3, L_4, /*hidden argument*/NULL);
		__this->set_animating_4((bool)1);
	}

IL_002f:
	{
		return;
	}
}
// System.Void ToggleButton::.ctor()
extern "C"  void ToggleButton__ctor_m3114583191 (ToggleButton_t3926247120 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ToggleButton::Start()
extern "C"  void ToggleButton_Start_m199321403 (ToggleButton_t3926247120 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void ToggleButton::Update()
extern "C"  void ToggleButton_Update_m3386185550 (ToggleButton_t3926247120 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void ToggleTrigger::.ctor()
extern "C"  void ToggleTrigger__ctor_m2950995521 (ToggleTrigger_t2585261492 * __this, const RuntimeMethod* method)
{
	{
		Trigger__ctor_m1356284031(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ToggleTrigger::get_isActive()
extern "C"  bool ToggleTrigger_get_isActive_m1735499190 (ToggleTrigger_t2585261492 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_active_2();
		return L_0;
	}
}
// System.Void ToggleTrigger::Start()
extern "C"  void ToggleTrigger_Start_m3095179001 (ToggleTrigger_t2585261492 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToggleTrigger_Start_m3095179001_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Material_t193706927_0_0_0_var), /*hidden argument*/NULL);
		Object_t1021602117 * L_1 = Resources_Load_m4276439101(NULL /*static, unused*/, _stringLiteral572223054, L_0, /*hidden argument*/NULL);
		__this->set_mat1_3(((Material_t193706927 *)CastclassClass((RuntimeObject*)L_1, Material_t193706927_il2cpp_TypeInfo_var)));
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Material_t193706927_0_0_0_var), /*hidden argument*/NULL);
		Object_t1021602117 * L_3 = Resources_Load_m4276439101(NULL /*static, unused*/, _stringLiteral572223055, L_2, /*hidden argument*/NULL);
		__this->set_mat2_4(((Material_t193706927 *)CastclassClass((RuntimeObject*)L_3, Material_t193706927_il2cpp_TypeInfo_var)));
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		Renderer_t257310565 * L_5 = GameObject_GetComponent_TisRenderer_t257310565_m1312615893(L_4, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m1312615893_RuntimeMethod_var);
		Material_t193706927 * L_6 = __this->get_mat1_3();
		Renderer_set_material_m3158565337(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ToggleTrigger::enter(System.Boolean,Character)
extern "C"  void ToggleTrigger_enter_m1707427839 (ToggleTrigger_t2585261492 * __this, bool ___isDestination0, Character_t3726027591 * ___character1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ToggleTrigger_enter_m1707427839_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Renderer_t257310565 * G_B2_0 = NULL;
	Renderer_t257310565 * G_B1_0 = NULL;
	Material_t193706927 * G_B3_0 = NULL;
	Renderer_t257310565 * G_B3_1 = NULL;
	{
		bool L_0 = __this->get_active_2();
		__this->set_active_2((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		Renderer_t257310565 * L_2 = GameObject_GetComponent_TisRenderer_t257310565_m1312615893(L_1, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m1312615893_RuntimeMethod_var);
		bool L_3 = __this->get_active_2();
		G_B1_0 = L_2;
		if (!L_3)
		{
			G_B2_0 = L_2;
			goto IL_0030;
		}
	}
	{
		Material_t193706927 * L_4 = __this->get_mat2_4();
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		goto IL_0036;
	}

IL_0030:
	{
		Material_t193706927 * L_5 = __this->get_mat1_3();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
	}

IL_0036:
	{
		Renderer_set_material_m3158565337(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrapTrigger::.ctor()
extern "C"  void TrapTrigger__ctor_m3101709300 (TrapTrigger_t598219061 * __this, const RuntimeMethod* method)
{
	{
		Trigger__ctor_m1356284031(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrapTrigger::enter(System.Boolean,Character)
extern "C"  void TrapTrigger_enter_m3384976926 (TrapTrigger_t598219061 * __this, bool ___isDestination0, Character_t3726027591 * ___character1, const RuntimeMethod* method)
{
	Scene_t1684909666  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Character_t3726027591 * L_0 = ___character1;
		bool L_1 = Character_get_isPlayer_m2500959794(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Scene_t1684909666  L_2 = SceneManager_GetActiveScene_m1722284185(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = Scene_get_name_m3371331170((&V_0), /*hidden argument*/NULL);
		SceneManager_LoadScene_m2357316016(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void Trigger::.ctor()
extern "C"  void Trigger__ctor_m1356284031 (Trigger_t3844394560 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Trigger::Start()
extern "C"  void Trigger_Start_m494493211 (Trigger_t3844394560 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Trigger::Update()
extern "C"  void Trigger_Update_m3422244802 (Trigger_t3844394560 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Trigger::enter(System.Boolean,Character)
extern "C"  void Trigger_enter_m4211865653 (Trigger_t3844394560 * __this, bool ___isDestination0, Character_t3726027591 * ___character1, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Trigger::exit(System.Boolean,Character)
extern "C"  void Trigger_exit_m109656981 (Trigger_t3844394560 * __this, bool ___isDestination0, Character_t3726027591 * ___character1, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void WinTrigger::.ctor()
extern "C"  void WinTrigger__ctor_m2900559871 (WinTrigger_t872747498 * __this, const RuntimeMethod* method)
{
	{
		Trigger__ctor_m1356284031(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WinTrigger::Start()
extern "C"  void WinTrigger_Start_m2621660491 (WinTrigger_t872747498 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WinTrigger_Start_m2621660491_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral1047780110, /*hidden argument*/NULL);
		Menu_t4261767481 * L_1 = GameObject_GetComponent_TisMenu_t4261767481_m3170984502(L_0, /*hidden argument*/GameObject_GetComponent_TisMenu_t4261767481_m3170984502_RuntimeMethod_var);
		__this->set_menu_2(L_1);
		return;
	}
}
// System.Void WinTrigger::enter(System.Boolean,Character)
extern "C"  void WinTrigger_enter_m3251762553 (WinTrigger_t872747498 * __this, bool ___isDestination0, Character_t3726027591 * ___character1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WinTrigger_enter_m3251762553_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Character_t3726027591 * L_0 = ___character1;
		bool L_1 = Character_get_isPlayer_m2500959794(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Character_t3726027591 * L_2 = ___character1;
		List_1_t145766874 * L_3 = L_2->get_path_3();
		List_1_Clear_m1988879339(L_3, /*hidden argument*/List_1_Clear_m1988879339_RuntimeMethod_var);
		Menu_t4261767481 * L_4 = __this->get_menu_2();
		Menu_finishLevel_m1671178345(L_4, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
